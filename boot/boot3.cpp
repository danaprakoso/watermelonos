#define NULL 0

#include "boot3.h"

VesaInfo* vesaInfo;
Device ide_device;
Device ahci_device;
char* screen = 0;
int txt_y = 0;
int ide_bar = 0;
int ide_device_num = 0;
int ahci_bar = 0;

void stop() {
	while (1) {}
}

bool strcmpl(char* text1, char* text2, int length) {
	int i = 0;
	while (i < length) {
		if (text1[i] != text2[i]) {
			return false;
		}
		i++;
	}
	return true;
}

char* memset(void* dest, int value, int count) {
	char* dst = (char*)dest;
	for (int i=0; i<count; i++) {
		dst[i] = value;
	}
	return dest;
}

void* memcpy(void* dest, const void* source, int count) {
	char* src = (char*)source;
	char* dst = (char*)dest;
	for (int i=0; i<count; i++) {
		dst[i] = src[i];
	}
	return dest;
}

int txt_get_offset(int x, int y) {
	return (y*80+x)*2;
}

void dump(char* text, int count) {
	if (screen == 0) {
		screen = (char*)0xB8000;
	}
	int j = txt_get_offset(0, txt_y);
	int i = 0;
	while (i < count) {
		screen[j] = text[i];
		screen[j+1] = 0x0F;
		j += 2;
		i++;
	}
}

void print(char* text) {
	if (screen == 0) {
		screen = (char*)0xB8000;
	}
	int i = 0;
	int j = txt_get_offset(0, txt_y);
	while (text[i] != 0) {
		screen[j] = text[i];
		screen[j+1] = 0x0F;
		i++;
		j += 2;
	}
	txt_y++;
}

int numlen(int number) {
	if (number == 0) {
		return 1;
	}
	int total = 0;
	while (number > 0) {
		number /= 10;
		total++;
	}
	return total;
}

void printn(int number) {
	if (screen == 0) {
		screen = (char*)0xB8000;
	}
	int len = numlen(number)-1;
	int j = txt_get_offset(0, txt_y);
	j += 2*len;
	if (number == 0) {
		screen[j] = '0';
		screen[j+1] = 0x0F;
		return;
	}
	while (number > 0) {
		screen[j] = number%10+'0';
		screen[j+1] = 0x0F;
		j -= 2;
		number /= 10;
	}
	txt_y++;
}

int numlenh(unsigned int number) {
	if (number == 0) {
		return 1;
	}
	int total = 0;
	while (number > 0) {
		number /= 16;
		total++;
	}
	return total;
}

char hex[16];

void printh(unsigned int number) {
	if (screen == 0) {
		screen = (char*)0xB8000;
	}
	hex[0] = '0';
	hex[1] = '1';
	hex[2] = '2';
	hex[3] = '3';
	hex[4] = '4';
	hex[5] = '5';
	hex[6] = '6';
	hex[7] = '7';
	hex[8] = '8';
	hex[9] = '9';
	hex[10] = 'A';
	hex[11] = 'B';
	hex[12] = 'C';
	hex[13] = 'D';
	hex[14] = 'E';
	hex[15] = 'F';
	int len = numlenh(number)-1;
	int j = txt_get_offset(0, txt_y);
	j += 2*len;
	if (number == 0) {
		screen[j] = '0';
		screen[j+1] = 0x0F;
		return;
	}
	while (number > 0) {
		screen[j] = hex[number%16];
		screen[j+1] = 0x0F;
		j -= 2;
		number /= 16;
	}
	txt_y++;
}

void out8(int port, char value) {
	asm volatile("out dx, al"::"d"(port),"a"(value));
}

char in8(int port) {
	char value;
	asm volatile("in al, dx":"=a"(value):"d"(port));
	return value;
}

void out16(int port, short value) {
	asm volatile("out dx, ax"::"d"(port),"a"(value));
}

short in16(int port) {
	short value;
	asm volatile("in ax, dx":"=a"(value):"d"(port));
	return value;
}

void out32(int port, int value) {
	asm volatile("out dx, eax"::"d"(port),"a"(value));
}

int in32(int port) {
	int value;
	asm volatile("in eax, dx":"=a"(value):"d"(port));
	return value;
}

int pci_read_int(int bus, int slot, int func, int offset) {
	int address = (bus<<16)|(slot<<11)|(func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	return in32(0xCFC);
}

Device* pci_init_device(int _class, int _subclass, Device* device) {
	for (int bus=0; bus<256; bus++) {
		for (int slot=0; slot<32; slot++) {
			for (int func=0; func<8; func++) {
				int vendorID = pci_read_int(bus, slot, func, 0)&0xFFFF;
				if (vendorID == 0xFFFF) {
					continue;
				}
				int __class = pci_read_int(bus, slot, func, 8);
				int classCode = __class>>24;
				int subclassCode = __class>>16&0xFF;
				if (classCode == _class && subclassCode == _subclass) {
					int bar0 = pci_read_int(bus, slot, func, 0x10);
					int bar1 = pci_read_int(bus, slot, func, 0x14);
					int bar2 = pci_read_int(bus, slot, func, 0x18);
					int bar3 = pci_read_int(bus, slot, func, 0x1C);
					int bar4 = pci_read_int(bus, slot, func, 0x20);
					int bar5 = pci_read_int(bus, slot, func, 0x24);
					device->bar0 = bar0;
					device->bar1 = bar1;
					device->bar2 = bar2;
					device->bar3 = bar3;
					device->bar4 = bar4;
					device->bar5 = bar5;
					
					return device;
				}
			}
		}
	}
	return NULL;
}

bool is_drive_exists(int bar, int device) {
	int status = 0;
	out8(bar+3, 0x88);
	status = in8(bar+3)&0xFF;
	if (status != 0x88) {
		return false;
	}
	out8(bar+6, device<<4);
	status = in8(bar+7)&0xFF;
	if (status == 0xFF) {
		return false;
	}
	if ((status&0x40) != 0) {
		return true;
	}
	return false;
}

void ide_wait_bsy(int bar) {
	while ((in8(bar+7)&0x80)!=0) {}
}

bool is_dvd(int bar, int device) {
	out8(bar+6, device<<4);
	out8(bar+7, 0xEC);
	int status = in8(bar+7)&0xFF;
	if ((status&1) != 0) {
		return true;
	}
	ide_wait_bsy(bar);
	for (int i=0; i<256; i++) {
		in16(bar);
	}
	return false;
}

void ide_read_sector(int lba, int count, char* buffer) {
	char commands[12];
	memset(commands, 0, 12);
	commands[0] = 0x28;
	commands[2] = lba>>24;
	commands[3] = (lba>>16)&0xFF;
	commands[4] = (lba>>8)&0xFF;
	commands[5] = lba&0xFF;
	commands[7] = (count>>8)&0xFF;
	commands[8] = count&0xFF;
	out8(ide_bar+1, 0);
	out8(ide_bar+4, 2048&0xFF);
	out8(ide_bar+5, (2048>>8)&0xFF);
	out8(ide_bar+6, ide_device_num<<4);
	out8(ide_bar+7, 0xA0);
	ide_wait_bsy(ide_bar);
	int count0 = in8(ide_bar+4)&0xFF;
	int count1 = in8(ide_bar+5)&0xFF;
	int byteCount = (count1<<8)|count0;
	int j = 0;
	for (int i=0; i<6; i++) {
		int command0 = commands[j]&0xFF;
		int command1 = commands[j+1]&0xFF;
		short command = (command1<<8)|command0;
		out16(ide_bar, command);
		j += 2;
	}
	ide_wait_bsy(ide_bar);
	j = 0;
	for (int k=0; k<count; k++) {
		for (int i=0; i<byteCount/2; i++) {
			short data = in16(ide_bar);
			buffer[j] = data&0xFF;
			buffer[j+1] = (data>>8)&0xFF;
			j += 2;
		}
		if (count > 1) {
			ide_wait_bsy(ide_bar);
		}
	}
}

unsigned int charstouint(char c1, char c2, char c3, char c4) {
	int i1 = (int)((c1<0)?(c1+256):c1)*0x1000000;
	int i2 = (int)((c2<0)?(c2+256):c2)*0x10000;
	int i3 = (int)((c3<0)?(c3+256):c3)*0x100;
	int i4 = (int)((c4<0)?(c4+256):c4);
	return i1+i2+i3+i4;
}

void load_kernel() {
	ELFHeader* hdr = (ELFHeader*)0x500000;
	int shCount = (int)hdr->shnum;
	for (int i=0; i<shCount; i++) {
		ELFSectionHeader* shdr = (ELFSectionHeader*)(0x500000+hdr->shoff);
		if (shdr->addr != 0) {
			if (shdr->type == 0x08) {
				memset(shdr->addr, 0, shdr->size);
			} else {
				memcpy(shdr->addr, 0x500000+shdr->offset, shdr->size);
			}
		}
		hdr->shoff += sizeof(ELFSectionHeader);
	}
	/*int phCount = (int)hdr->phnum;
	for (int i=0; i<phCount; i++) {
		ELFProgramHeader* phdr = (ELFProgramHeader*)(0x500000+hdr->phoff);
		if (phdr->type == 0x01) {
			memcpy(phdr->vaddr, 0x500000+phdr->offset, phdr->filesz);
		}
		hdr->phoff += sizeof(ELFProgramHeader);
	}*/
	void (*go)(VesaInfo* vesaInfo);
	go = hdr->entry;
	go(vesaInfo);
	stop();
}

void load_kernel_using_ide() {
	int bar = ide_device.bar0; //primary bar
	if (bar == 0 || bar == 1) {
		bar = 0x1F0;
	}
	bool dvd_found = false;
	// Check if controller 0 device 0 exists
	if (is_drive_exists(bar, 0)) {
		if (is_dvd(bar, 0)) {
			dvd_found = true;
			ide_bar = bar;
			ide_device_num = 0;
		}
	}
	// Check if controller 0 device 1 exists
	if (is_drive_exists(bar, 1)) {
		if (is_dvd(bar, 1)) {
			dvd_found = true;
			ide_bar = bar;
			ide_device_num = 1;
		}
	}
	bar = ide_device.bar2; //secondary bar
	if (bar == 0 || bar == 1) {
		bar = 0x170;
	}
	// Check if controller 1 device 0 exists
	if (is_drive_exists(bar, 0)) {
		if (is_dvd(bar, 0)) {
			dvd_found = true;
			ide_bar = bar;
			ide_device_num = 0;
		}
	}
	// Check if controller 1 device 1 exists
	if (is_drive_exists(bar, 1)) {
		if (is_dvd(bar, 1)) {
			dvd_found = true;
			ide_bar = bar;
			ide_device_num = 1;
		}
	}
	if (dvd_found) {
		// Mencari Primary Volume Descriptor
		char buffer[2048];
		bool pvd_found = false;
		int lba = 16;
		while (true) {
			ide_read_sector(lba, 1, buffer);
			if (buffer[0] == 0x01) {
				pvd_found = true;
				break;
			}
			lba++;
		}
		if (pvd_found) {
			lba = charstouint(buffer[161], buffer[160], buffer[159], buffer[158]);
			ide_read_sector(lba, 1, buffer);
			// Mencari folder "boot"
			int j = 0;
			while (true) {
				int length = (int)buffer[j]&0xFF;
				if (strcmpl(buffer+j+33, "BOOT", 4)) {
					lba = charstouint(buffer[j+5], buffer[j+4], buffer[j+3], buffer[j+2]);
					length = charstouint(buffer[j+13], buffer[j+12], buffer[j+11], buffer[j+10]);
					int count = length/2048;
					ide_read_sector(lba, count, buffer);
					j = 0;
					while (true) {
						length = (int)buffer[j]&0xFF;
						if (strcmpl(buffer+j+33, "KERNEL.BIN;1", 12)) {
							int kernelLba = charstouint(buffer[j+5], buffer[j+4], buffer[j+3], buffer[j+2]);
							int kernelSize = charstouint(buffer[j+13], buffer[j+12], buffer[j+11], buffer[j+10]);
							count = kernelSize/2048;
							if ((kernelSize%2048) != 0) {
								count++;
							}
							ide_read_sector(kernelLba, count, 0x500000);
							load_kernel();
							return;
						}
						j += length;
					}
				}
				j += length;
			}
		}
	}
}

void stop_cmd(HBA_PORT *port)
{
	// Clear ST (bit0)
	port->cmd &= ~HBA_PxCMD_ST;
 
	// Wait until FR (bit14), CR (bit15) are cleared
	while(1)
	{
		/*if (port->cmd & HBA_PxCMD_FR)
			continue;
		if (port->cmd & HBA_PxCMD_CR)
			continue;
		break;*/
		if (port->ci == 0) {
			break;
		}
	}
 
	// Clear FRE (bit4)
	port->cmd &= ~HBA_PxCMD_FRE;
}

void start_cmd(HBA_PORT *port)
{
	// Wait until CR (bit15) is cleared
	while (port->cmd & HBA_PxCMD_CR);
 
	// Set FRE (bit4) and ST (bit0)
	port->cmd |= HBA_PxCMD_FRE;
	port->cmd |= HBA_PxCMD_ST; 
}

void port_rebase(HBA_PORT *port, int portno) {
	stop_cmd(port);	// Stop command engine
	// Command list offset: 1K*portno
	// Command list entry size = 32
	// Command list entry maxim count = 32
	// Command list maxim size = 32*32 = 1K per port
	port->clb = AHCI_BASE + (portno<<10);
	port->clbu = 0;
	memset((void*)(port->clb), 0, 1024);
 
	// FIS offset: 32K+256*portno
	// FIS entry size = 256 bytes per port
	port->fb = AHCI_BASE + (32<<10) + (portno<<8);
	port->fbu = 0;
	memset((void*)(port->fb), 0, 256);
 
	// Command table offset: 40K + 8K*portno
	// Command table size = 256*32 = 8K per port
	HBA_CMD_HEADER *cmdheader = (HBA_CMD_HEADER*)(port->clb);
	for (int i=0; i<32; i++)
	{
		cmdheader[i].prdtl = 8;	// 8 prdt entries per command table
					// 256 bytes per command table, 64+16+48+16*8
		// Command table offset: 40K + 8K*portno + cmdheader_index*256
		cmdheader[i].ctba = AHCI_BASE + (40<<10) + (portno<<13) + (i<<8);
		cmdheader[i].ctbau = 0;
		memset((void*)cmdheader[i].ctba, 0, 256);
	}
 
	start_cmd(port);	// Start command engine
}

int find_cmdslot(HBA_PORT *port)
{
	// If not set in SACT and CI, the slot is free
	uint32_t slots = (port->sact | port->ci);
	for (int i=0; i<32; i++)
	{
		if ((slots&1) == 0)
			return i;
		slots >>= 1;
	}
	print("Cannot find free command list entry\n");
	return -1;
}
 
bool ahci_read_sector(HBA_PORT *port, uint32_t startl, uint32_t starth, uint32_t count, char *buf)
{
	port->is = (uint32_t) -1;		// Clear pending interrupt bits
	int spin = 0; // Spin lock timeout counter
	int slot = find_cmdslot(port);
	if (slot == -1)
		return false;
 
	HBA_CMD_HEADER *cmdheader = (HBA_CMD_HEADER*)port->clb;
	cmdheader += slot;
	cmdheader->cfl = sizeof(FIS_REG_H2D)/sizeof(uint32_t);	// Command FIS size
	cmdheader->w = 0;		// Read from device
	cmdheader->a = 1;
	cmdheader->p = 1;
	cmdheader->prdtl = (uint16_t)((count-1)>>4) + 1;	// PRDT entries count
 
	HBA_CMD_TBL *cmdtbl = (HBA_CMD_TBL*)(cmdheader->ctba);
	memset(cmdtbl, 0, sizeof(HBA_CMD_TBL) +
 		(cmdheader->prdtl-1)*sizeof(HBA_PRDT_ENTRY));
 	cmdtbl->acmd[0] = 0x28;
 	cmdtbl->acmd[2] = (startl>>24)&0xFF;
 	cmdtbl->acmd[3] = (startl>>16)&0xFF;
 	cmdtbl->acmd[4] = (startl>>8)&0xFF;
 	cmdtbl->acmd[5] = startl&0xFF;
 	cmdtbl->acmd[7] = (count>>8)&0xFF;
 	cmdtbl->acmd[8] = count&0xFF;
 
	// 8K bytes (16 sectors) per PRDT
	// Last entry
	cmdtbl->prdt_entry[0].dba = (uint32_t) buf;
	cmdtbl->prdt_entry[0].dbc = count*2048-1;	// 512 bytes per sector
	cmdtbl->prdt_entry[0].i = 1;
 
	// Setup command
	FIS_REG_H2D *cmdfis = (FIS_REG_H2D*)(&cmdtbl->cfis);
 
	cmdfis->fis_type = FIS_TYPE_REG_H2D;
	cmdfis->c = 1;	// Command
	cmdfis->command = 0xA0;
 
	cmdfis->countl = count & 0xFF;
	cmdfis->counth = (count >> 8) & 0xFF;
 
	// The below loop waits until the port is no longer busy before issuing a new command
	while ((port->tfd & (ATA_DEV_BUSY | ATA_DEV_DRQ)) && spin < 1000000)
	{
		spin++;
	}
	if (spin == 1000000)
	{
		print("Port is hung\n");
		return FALSE;
	}
 
	port->ci = 1<<slot;	// Issue command
 
	// Wait for completion
	while (1)
	{
		// In some longer duration reads, it may be helpful to spin on the DPS bit 
		// in the PxIS port field as well (1 << 5)
		if ((port->ci & (1<<slot)) == 0) 
			break;
		if (port->is & HBA_PxIS_TFES)	// Task file error
		{
			print("Read disk error\n");
			return FALSE;
		}
	}
 
	// Check again
	if (port->is & HBA_PxIS_TFES)
	{
		print("Read disk error\n");
		return FALSE;
	}
 
	return true;
}

HBA_PORT* find_dvd_port() {
	for (int i=0; i<32; i++) {
		HBA_PORT* port = (HBA_PORT*)(ahci_bar+0x100+i*0x80);
		if (port->sig == 0xEB140101) {
			return port;
		}
	}
	return NULL;
}

void load_kernel_using_ahci() {
	ahci_bar = ahci_device.bar5;
	HBA_PORT* port = find_dvd_port();
	if (port != NULL) {
		port_rebase(port, 0);
		// Mencari Primary Volume Descriptor
		char buffer[2048];
		bool pvd_found = false;
		int lba = 16;
		while (true) {
			ahci_read_sector(port, lba, 0, 1, buffer);
			if (buffer[0] == 0x01) {
				pvd_found = true;
				break;
			}
			lba++;
		}
		if (pvd_found) {
			lba = charstouint(buffer[161], buffer[160], buffer[159], buffer[158]);
			ahci_read_sector(port, lba, 0, 1, buffer);
			// Mencari folder "boot"
			int j = 0;
			while (true) {
				int length = (int)buffer[j]&0xFF;
				if (strcmpl(buffer+j+33, "BOOT", 4)) {
					lba = charstouint(buffer[j+5], buffer[j+4], buffer[j+3], buffer[j+2]);
					length = charstouint(buffer[j+13], buffer[j+12], buffer[j+11], buffer[j+10]);
					int count = length/2048;
					ahci_read_sector(port, lba, 0, count, buffer);
					j = 0;
					while (true) {
						length = (int)buffer[j]&0xFF;
						if (strcmpl(buffer+j+33, "KERNEL.BIN;1", 12)) {
							int kernelLba = charstouint(buffer[j+5], buffer[j+4], buffer[j+3], buffer[j+2]);
							int kernelSize = charstouint(buffer[j+13], buffer[j+12], buffer[j+11], buffer[j+10]);
							count = kernelSize/2048;
							if ((kernelSize%2048) != 0) {
								count++;
							}
							ahci_read_sector(port, kernelLba, 0, count, 0x500000);
							load_kernel();
							return;
						}
						j += length;
					}
				}
				j += length;
			}
		}
	}
}

int main(VesaInfo* _vesaInfo) {
	vesaInfo = _vesaInfo;
	Device* device = pci_init_device(0x1, 0x1, &ide_device);
	if (device != NULL) {
		load_kernel_using_ide();
	} else {
		device = pci_init_device(0x1, 0x6, &ahci_device);
		if (device != NULL) {
			load_kernel_using_ahci();
		}
	}
	stop();
	return 0;
}
