[BITS 16]
ORG 0x500

start:
	mov ax, 0
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    mov sp, 0xFFFF
    
    call find_best_video_mode
	
	mov ah, 0
	mov al, 3
	;int 10h
	
    in al, 0x92
    or al, 2
    out 0x92, al
    cli
    mov eax, cr0
    or eax, 1
    mov cr0, eax
    lgdt[gdt_pointer]
    jmp 0x8:pmode

find_best_video_mode:
	mov ax, 0x4F00
	mov di, VbeInfoBlock
	int 10h
	
	mov eax, [VbeInfoBlock.VideoModePtr]
	shr eax, 16
	and eax, 0xFFFF
	mov ebx, 16
	mul ebx
	mov ebx, [VbeInfoBlock.VideoModePtr]
	and ebx, 0xFFFF
	add eax, ebx
	mov si, ax
	push si
	
	.loop1:
	pop si
	mov ax, [si]
	mov word [mode_number], ax
	add si, 2
	push si
	;call print_h16
	
	mov cx, [mode_number]
	mov ax, 0x4F01
	mov bx, es
	mov es, bx
	mov di, ModeInfoBlock
	xor bx, bx
	int 10h
	
	cmp al, 0x4F
	jne .loop1
	
	cmp ah, 3
	je .loop1
	
	cmp ah, 2
	je .loop1
	
	cmp ah, 1
	je .loop1
	
	cmp word [mode_number], 0xFFFF
	je .loop1
	
	cmp word [mode_number], 0x100
	jl .loop1
	
	mov ax, [ModeInfoBlock.Width]
	cmp ax, 800
	je .1
	jmp .loop1
	
	.1:
	mov ax, [ModeInfoBlock.Height]
	cmp ax, 600
	je .2
	jmp .loop1
	
	.2:
	mov al, [ModeInfoBlock.BitsPerPixel]
	cmp al, 24
	je .3
	cmp al, 32
	je .3
	jmp .loop1
	
	.3:
	pop si
	
	mov ax, 0x4F02
	mov bx, ds
	mov es, bx
	mov di, 0
	mov bx, [mode_number]
	or bx, 0x4000
	int 10h
	
	ret
	
numlen_n16:
	; AX = number
	pusha
	mov word [.len], 0
	.loop1:
	cmp ax, 0
	jle .finish1
	mov bx, 10
	xor dx, dx
	div bx
	add word [.len], 1
	jmp .loop1
	
	.finish1:
	popa
	mov ax, [.len]
	ret
	
	.len dw 0

print_n16:
	; AX = number
	cmp word [txt_y], 24
	je .ret
	pusha
	push ax
	mov bx, 0xB800
	mov es, bx
	mov di, 0x0
	mov ax, [txt_y]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	add di, ax
	pop ax
	push ax
	call numlen_n16
	sub ax, 1
	add di, ax
	add di, ax
	pop ax
	.loop1:
	cmp ax, 0
	jle .finish1
	mov bx, 10
	xor dx, dx
	div bx
	push ax
	mov al, dl
	add al, '0'
	mov byte [es:di], al
	mov al, 0x0F
	mov byte [es:di+1], al
	sub di, 2
	pop ax
	jmp .loop1
	
	.finish1:
	add dword [txt_y], 1
	popa
	ret
	
	.ret:
	ret

numlen_h16:
	; AX = number
	pusha
	mov word [.len], 0
	.loop1:
	cmp ax, 0
	jle .finish1
	mov bx, 16
	xor dx, dx
	div bx
	add word [.len], 1
	jmp .loop1
	
	.finish1:
	popa
	mov ax, [.len]
	ret
	
	.len dw 0

print_h16:
	; AX = number
	cmp word [txt_y], 24
	je .ret
	pusha
	push ax
	mov bx, 0xB800
	mov es, bx
	mov di, 0x0
	mov ax, [txt_y]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	add di, ax
	pop ax
	push ax
	call numlen_h16
	sub ax, 1
	add di, ax
	add di, ax
	pop ax
	.loop1:
	cmp ax, 0
	jle .finish1
	mov bx, 16
	xor dx, dx
	div bx
	push ax
	mov si, hex
	add si, dx
	mov al, [si]
	mov byte [es:di], al
	mov al, 0x0F
	mov byte [es:di+1], al
	sub di, 2
	pop ax
	jmp .loop1
	
	.finish1:
	add dword [txt_y], 1
	popa
	ret
	
	.ret:
	ret

txt_y dd 0
hex db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
mode_number dw 0

VbeInfoBlock:
	.VbeSignature db 'VBE2'
	.VbeVersion dw 0x300
	.OemStringPtr dd 0
	.Capabilities times 4 db 0
	.VideoModePtr dd 0
	.TotalMemory dw 0
	.OemSoftwareRev dw 0
	.OemVendorNamePtr dd 0
	.OemProductNamePtr dd 0
	.OemProductRevPtr dd 0
	.Reserved times 222 db 0
	.OemData times 256 db 0
	times 100 db 0

ModeInfoBlock:
	.ModeAttributes dw 0
	.WinAAttributes db 0
	.WinBAttributes db 0
	.WinGranularity dw 0
	.WinSize dw 0
	.WinASegment dw 0
	.WinBSegment dw 0
	.WinFuncPtr dd 0
	.BytesPerScanLine dw 0
	.Width dw 0
	.Height dw 0
	.XCharSize db 0
	.YCharSize db 0
	.NumberOfPlanes db 0
	.BitsPerPixel db 0
	.NumberOfBanks db 0
	.MemoryModel db 0
	.BankSize db 0
	.NumberOfImagePages db 0
	.Reserved1 db 1
	.RedMaskSize db 0
	.RedFieldPosition db 0
	.GreenMaskSize db 0
	.GreenFieldPosition db 0
	.BlueMaskSize db 0
	.BlueFieldPosition db 0
	.RsvdMaskSize db 0
	.RsvdFieldPosition db 0
	.DirectColorModeInfo db 0
	.ScreenBuffer dd 0
	.Reserved2 dd 0
	.Reserved3 dw 0
	.LinBytesPerScanLine dw 0
	.BnkNumberOfImagePages db 0
	.LinNumberOfImagePages db 0
	.LinRedMaskSize db 0
	.LinRedFieldPosition db 0
	.LinGreenMaskSize db 0
	.LinGreenFieldPosition db 0
	.LinBlueMaskSize db 0
	.LinBlueFieldPosition db 0
	.LinRsvdMaskSize db 0
	.LinRsvdFieldPosition db 0
	.MaxPixelClock dd 0
	.Reserved4 times 189 db 0
	times 100 db 0

[BITS 32]
  
pmode:
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    
    mov esi, boot3
    mov edi, ELFHeader
    mov ecx, 52
    rep movsb
    
    mov ax, [ELFHeader.shnum]
    movzx ecx, ax
    
    .loop1:
    push ecx
    
    	mov esi, boot3
    	add esi, [ELFHeader.shoff]
    	mov edi, ELFSectionHeader
    	mov ecx, 40
    	rep movsb
    	
    	add dword [ELFHeader.shoff], 40
    	
    	cmp dword [ELFSectionHeader.type], 8
    	je .1
    	
    	mov esi, boot3
    	add esi, [ELFSectionHeader.offset]
    	mov edi, [ELFSectionHeader.addr]
    	mov ecx, [ELFSectionHeader.size]
    	rep movsb
    	
    	jmp .skip1
    	
    	.1:
    	
    	mov edi, [ELFSectionHeader.addr]
    	mov al, 0
    	mov ecx, [ELFSectionHeader.size]
    	rep stosb
    	
    	.skip1:
    
    pop ecx
    loop .loop1
    
    push dword ModeInfoBlock
    call dword [ELFHeader.entry]
    
    jmp $
    
ELFHeader:
	.ident times 16 db 0
	.type dw 0
	.machine dw 0
	.version dd 0
	.entry dd 0
	.phoff dd 0
	.shoff dd 0
	.flags dd 0
	.ehsize dw 0
	.phentsize dw 0
	.phnum dw 0
	.shentsize dw 0
	.shnum dw 0
	.shstrndx dw 0

ELFProgramHeader:
	.type dd 0
	.offset dd 0
	.vaddr dd 0
	.paddr dd 0
	.filesz dd 0
	.memsz dd 0
	.flags dd 0
	.align dd 0

ELFSectionHeader:
	.name dd 0
	.type dd 0
	.flags dd 0
	.addr dd 0
	.offset dd 0
	.size dd 0
	.link dd 0
	.info dd 0
	.addralign dd 0
	.entsize dd 0

    ; Deteksi IDE
    mov eax, 1
    mov ebx, 1
    call find_pci_device
    jc .ide_found
    
    jmp $

    .ide_found:
    mov [ide_bus], eax
    mov [ide_slot], ebx
    mov [ide_func], ecx
    ; Load kernel using IDE
    
    ; Get BAR0
    mov edx, 0x10
    call pci_read_int
    cmp eax, 0
    je .0
    cmp eax, 1
    je .0
    jmp .skip0
    
    .0:
    mov dword [ide_pbar], 0x1F0
    
    .skip0:
    
    mov eax, [ide_bus]
    mov edx, 0x18
    call pci_read_int
    cmp eax, 0
    je .1
    cmp eax, 1
    je .1
    jmp .skip1
    
    .1:
    mov dword [ide_sbar], 0x170
    
    .skip1:
    
    ; Mencari drive DVD
    ; Mengecek di controller 0 device 0
    
    mov dx, [ide_pbar]
    add dx, 6
    mov al, 0
    out dx, al
    add dx, 1
    mov al, 0xEC
    out dx, al
    in al, dx
    movzx eax, al
    call print_h32
    jmp $
    
    mov dx, [ide_pbar]
    add dx, 2
    in al, dx
    movzx eax, al
    call print_h32
    sub dx, 1
    in al, dx
    movzx eax, al
    call print_h32
    add dx, 1
    in al, dx
    movzx eax, al
    call print_h32
    add dx, 1
    in al, dx
    movzx eax, al
    call print_h32
    
    jmp $
    
    mov dword [ide_controller], 0
    mov dword [ide_device], 0
    mov dx, [ide_pbar]
    mov [ide_bar], dx
    add dx, 6
    mov al, 0
    out dx, al
    add dx, 1
    mov al, 0xEC
    out dx, al
    in al, dx
    and al, 1
    cmp al, 1
    je .ide_dvd_found
    mov ax, [ide_pbar]
    call ide_wait_bsy
    
    ; Mengecek di controller 0 device 1
    mov dword [ide_controller], 0
    mov dword [ide_device], 1
    mov dx, [ide_pbar]
    mov [ide_bar], dx
    add dx, 6
    mov al, (1<<4)
    out dx, al
    add dx, 1
    mov al, 0xEC
    out dx, al
    in al, dx
    and al, 1
    cmp al, 1
    je .ide_dvd_found
    mov ax, [ide_pbar]
    call ide_wait_bsy
    
    ; Mengecek di controller 1 device 0
    mov dword [ide_controller], 1
    mov dword [ide_device], 0
    mov dx, [ide_sbar]
    mov [ide_bar], dx
    add dx, 6
    mov al, 0
    out dx, al
    add dx, 1
    mov al, 0xEC
    out dx, al
    in al, dx
    and al, 1
    cmp al, 1
    je .ide_dvd_found
    mov ax, [ide_pbar]
    call ide_wait_bsy
    
    ; Mengecek di controller 1 device 1
    mov dword [ide_controller], 1
    mov dword [ide_device], 1
    mov dx, [ide_sbar]
    mov [ide_bar], dx
    add dx, 6
    mov al, (1<<4)
    out dx, al
    add dx, 1
    mov al, 0xEC
    out dx, al
    in al, dx
    and al, 1
    cmp al, 1
    je .ide_dvd_found
    mov ax, [ide_pbar]
    call ide_wait_bsy
    
    mov eax, text2
    call print
	
	jmp $
	
	.ide_dvd_found:
    
    ; Membaca sector 16
    .loop1:
    mov eax, [current_lba]
    mov ebx, 1
    mov ecx, buffer
    call ide_read_sector
    add dword [current_lba], 1
    
    mov esi, buffer
    mov al, [esi]
    cmp al, 1
    je .pvd_found
    
    jmp .loop1
    
    .pvd_found:
    
    mov esi, buffer
    add esi, 156
    mov eax, [esi+2]
    mov [current_lba], eax
    
    mov ebx, 1
    mov ecx, buffer
    call ide_read_sector
    
    mov eax, buffer
    mov ebx, 2048
    call dump
    
    jmp $

text3 db "PVD found", 0
current_lba dd 16
buffer times 2048 db 0

boot3:
incbin "boot/boot3"

ide_read_sector:
	; EAX = sector to read
	; EBX = number of sector
	; ECX = buffer
	pusha
	mov [.lba], eax
	mov [.sectors], ebx
	mov [.buffer], ecx
	mov dx, [ide_bar]
	add dx, 1
	mov al, 0
	out dx, al
	add dx, 3
	mov al, (2048 & 0xFF)
	out dx, al
	add dx, 1
	mov al, ((2048 >> 8) & 0xFF)
	out dx, al
	add dx, 1
	mov al, [ide_device]
	shl al, 4
	out dx, al
	add dx, 1
	mov al, 0xA0
	out dx, al
	mov ax, [ide_bar]
	call ide_wait_bsy
	
	mov dx, [ide_bar]
	add dx, 4
	in al, dx
	movzx eax, al
	mov ebx, eax
	add dx, 1
	in al, dx
	movzx eax, al
	shl eax, 8
	or eax, ebx
	
	mov edi, .cmd
	mov eax, [.lba]
	mov byte [edi+5], al
	shr eax, 8
	mov byte [edi+4], al
	shr eax, 8
	mov byte [edi+3], al
	shr eax, 8
	mov byte [edi+2], al
	mov eax, [.sectors]
	mov byte [edi+8], al
	shr eax, 8
	mov byte [edi+7], al
	
	; EAX = number of bytes per sector
	mov esi, .cmd
	mov dx, [ide_bar]
	mov ecx, 6
	.loop1:
	lodsw
	out dx, ax
	loop .loop1
	
	mov ax, [ide_bar]
	call ide_wait_bsy
	
	mov eax, [.sectors]
	mov ebx, 2048
	mul ebx
	mov ebx, 2
	xor edx, edx
	div ebx
	mov ecx, eax
	mov edi, [.buffer]
	mov dx, [ide_bar]
	.loop2:
	in ax, dx
	stosw
	loop .loop2
	
	popa
	ret
	
	.cmd db 0x28, 0x0, 0, 0, 0, 16, 0, 0, 1, 0, 0, 0
	.lba dd 0
	.sectors dd 0
	.buffer dd 0

dump:
	; EAX = buffer
	; EBX = count
	pusha
	mov esi, eax
	mov ecx, ebx
	mov edi, 0xB8000
	mov eax, [txt_y]
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	.loop1:
	lodsb
	stosb
	mov al, 0x0F
	stosb
	loop .loop1
	popa
	ret

ide_buff times 512 db 0

ide_wait_bsy:
	; AX = bar
	pusha
	mov dx, ax
	add dx, 7
	.loop1:
	in al, dx
	shr al, 7
	and al, 1
	cmp al, 1
	je .loop1
	popa
	ret

pci_read_int:
	; EAX = bus
	; EBX = slot
	; ECX = func
	; EDX = register
	pusha
	mov [.bus], eax
	mov [.slot], ebx
	mov [.func], ecx
	mov [.reg], edx
	mov eax, 0
	or eax, 0x80000000
	mov ebx, [.bus]
	shl ebx, 16
	or eax, ebx
	
	mov ebx, [.slot]
	shl ebx, 11
	or eax, ebx
	
	mov ebx, [.func]
	shl ebx, 8
	or eax, ebx
	
	mov ebx, [.reg]
	and ebx, 0xFC
	or eax, ebx
	
	mov dx, 0xCF8
	out dx, eax
	
	mov dx, 0xCFC
	in eax, dx
	mov [.ret], eax
	popa
	mov eax, [.ret]
	ret
	
	.bus dd 0
	.slot dd 0
	.func dd 0
	.reg dd 0
	.ret dd 0

numlen:
	; EAX = number
	pusha
	cmp eax, 0
	je .0
	xor ecx, ecx
	.loop1:
	cmp eax, 0
	jbe .ret
	mov ebx, 16
	mov edx, 0
	div ebx
	add ecx, 1
	jmp .loop1
	.ret:
	mov [.len], ecx
	popa
	mov eax, [.len]
	ret
	
	.0:
	popa
	mov eax, 1
	ret
	
	.len dd 0

print_h32:
	; EAX = number
	pusha
	push eax
	mov edi, 0xB8000
	mov eax, [txt_y]
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	pop eax
	cmp eax, 0
	je .0
	push eax
	call numlen
	sub eax, 1
	add edi, eax
	add edi, eax
	pop eax
	mov esi, hex
	.loop1:
	cmp eax, 0
	jbe .finish1
	mov ebx, 16
	xor edx, edx
	div ebx
	mov dl, [esi+edx]
	mov byte [edi], dl
	mov byte [edi+1], 0x0F
	sub edi, 2
	jmp .loop1
	
	.finish1:
	add dword [txt_y], 1
	popa
	ret
	
	.0:
	mov al, '0'
	stosb
	mov al, 0x0F
	stosb
	popa
	ret

print:
	; EAX = string addr
	pusha
	mov esi, eax
	mov edi, 0xB8000
	mov eax, [txt_y]
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	.loop1:
	lodsb
	cmp al, 0
	je .finish1
	stosb
	mov al, 0x0F
	stosb
	jmp .loop1
	.finish1:
	add dword [txt_y], 1
	popa
	ret

find_pci_device:
	; EAX = class
	; EBX = subclass
	mov [.class], eax
	mov [.subclass], ebx
	pusha
	mov ecx, 0
	.loop1:
	cmp ecx, 256
	je .finish1
	push ecx
		mov ecx, 0
		.loop2:
		cmp ecx, 32
		je .finish2
		push ecx
			mov ecx, 0
			.loop3:
			cmp ecx, 8
			je .finish3
			push ecx
				pop ecx
				pop ebx
				pop eax
				mov [.bus], eax
				mov [.slot], ebx
				mov [.func], ecx
				push eax
				push ebx
				push ecx
				mov edx, 0
				call pci_read_int
				and eax, 0xFFFF
				cmp eax, 0xFFFF ;Vendor ID
				je .skip1
				
				mov eax, [.bus]
				mov ebx, [.slot]
				mov ecx, [.func]
				mov edx, 8
				call pci_read_int
				
				push eax
				shr eax, 16
				and eax, 0xFF
				mov ebx, eax
				
				pop eax
				shr eax, 24
				mov ecx, eax
				
				cmp ecx, [.class]
				je .1
				jmp .skip1
				
				.1:
				cmp ebx, [.subclass]
				je .2
				jmp .skip1
				
				.2:
				pop ecx
				pop ebx
				pop eax
				mov [.bus], eax
				mov [.slot], ebx
				mov [.func], ecx
				popa
				mov eax, [.bus]
				mov ebx, [.slot]
				mov ecx, [.func]
				stc
				ret
				
				.skip1:
			pop ecx
			add ecx, 1
			jmp .loop3
			.finish3:
		pop ecx
		add ecx, 1
		jmp .loop2
		.finish2:
	pop ecx
	add ecx, 1
	jmp .loop1
	.finish1:
	popa
	clc
	ret
	
	.bus dd 0
	.slot dd 0
	.func dd 0
	.class dd 0
	.subclass dd 0

text1 db "IDE DVD device found", 0
text2 db "IDE DVD not device found", 0
ide_bus dd 0
ide_slot dd 0
ide_func dd 0
ide_pbar dd 0 ;primary bar
ide_sbar dd 0 ;secondary bar
ide_bar dd 0
ide_controller dd 0
ide_device dd 0

[BITS 16]

gdt_pointer:
gdt_limit dw 8*3-1     ;batas GDT
gdt_base dd gdt      ;dasar GDT

gdt:
dq 0

dw 0xFFFF
dw 0
db 0
db 0x9A
db 0xCF
db 0

dw 0xFFFF
dw 0
db 0
db 0x92
db 0xCF
db 0
