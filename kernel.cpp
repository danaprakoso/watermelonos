#include <system.h>

void calculator_app_onclick(View* v) {
	load_calculator_app();
}

void main_task() {
	fill_rect(0, 0, 800, 600, 0xffffffff);
	set_font((char*)&monaco_font, 59872);
	draw_image(&wallpaper, 0, 0, 585811);
	ImageButton* calculatorIcon = new ImageButton(&calculator_icon, 2436, 30, 30, 64, 64, calculator_app_onclick);
	calculatorIcon->apply();
	flush();
	mainLoop();
}

int main(BootInfo* bootInfo) {
	int totalMemory = bootInfo->mem_lower+bootInfo->mem_upper;
	init_memory((int)&kernel_end, totalMemory);
	init_serial(0x3F8, 0, 0, 0);
	init_gdt();
	init_idt();
	init_isr();
	init_irq();
	init_syscalls();
	init_keyboard();
	init_mouse();
	init_pit(1000);
	asm volatile("sti");
	VesaInfo* vesaInfo = (VesaInfo*)bootInfo->vbe_mode_info;
	if (vesaInfo != NULL) {
		init_graphics(vesaInfo);
	}
	init_pci();
	enum_pci();
	main_task();
	return 0;
}
