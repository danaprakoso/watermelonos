#include <system.h>
#include <stdarg.h>

void log(char* text) {
	int length = strlen(text);
	for (int i=0; i<length; i++) {
		write_serial(text[i]);
	}
}

void logn(int number) {
	int length = numlen(number);
	char* buffer = (char*)malloc(length+1);
	for (int i=length-1; i>=0; i--) {
		buffer[i] = number%10+'0';
		number /= 10;
	}
	buffer[length] = 0;
	log(buffer);
	free(buffer);
}

void logh(int number) {
	int length = numlenhex(number);
	char* buffer = (char*)malloc(length+1);
	for (int i=length-1; i>=0; i--) {
		buffer[i] = hex_value[number%16];
		number /= 16;
	}
	buffer[length] = 0;
	log(buffer);
	free(buffer);
}

void logc(char ch) {
	write_serial(ch);
}

void logf(char* text, ...) {
	int pos = 0;
	// Get length of variable arguments
	int total_args = 0;
	while (text[pos] != 0) {
		if (text[pos] == '%') {
			total_args++;
			pos++;
		}
		pos++;
	}
	pos = 0;
	va_list args;
	va_start(args, text);
	while (text[pos] != 0) {
		if (text[pos] == '%') {
			if (text[pos+1] == 'd') {
				unsigned int number = va_arg(args, unsigned int);
				int length = numlenu(number);
				char* buffer = (char*)malloc(length+1);
				for (int i=length-1; i>=0; i--) {
					buffer[i] = number%10+'0';
					number /= 10;
				}
				buffer[length] = 0;
				log(buffer);
				free(buffer);
			} else if (text[pos+1] == 'x') {
				unsigned int number = va_arg(args, unsigned int);
				int length = numlenhex(number);
				char* buffer = (char*)malloc(length+1);
				for (int i=length-1; i>=0; i--) {
					buffer[i] = hex_value[number%16];
					number /= 16;
				}
				buffer[length] = 0;
				log(buffer);
				free(buffer);
			} else if (text[pos+1] == 's') {
				char* str = va_arg(args, char*);
				log(str);
			} else if (text[pos+1] == 'c') {
				int ch = va_arg(args, int);
				logc(ch);
			}
			pos++;
		} else {
			write_serial(text[pos]);
		}
		pos++;
	}
	va_end(args);
}
