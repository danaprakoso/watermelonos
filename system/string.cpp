#include <system.h>

int strlen(char* string) {
	int i = 0;
	while (string[i] != 0) {
		i++;
	}
	return i;
}

char* replace_string(char* original_string, char toReplace, char with) {
	char* new_string = (char*)malloc(strlen(original_string));
	int i = 0;
	int j = 0;
	int length = strlen(original_string);
	while (i < length) {
		if (original_string[i] == toReplace) {
			if (with == 0) {
				i++;
				continue;
			} else {
				new_string[j] = with;
			}
		} else {
			new_string[j] = original_string[i];
		}
		i++;
		j++;
	}
	return new_string;
}

char* substring(char* string, int from, int to) {
	int len = strlen(string);
	char* new_string = (char*)malloc(len+1);
	memset(new_string, 0, len+1);
	memcpy(new_string, string+from, to-from);
	return new_string;
}

char* concat(char* string1, char* string2) {
	int length1 = strlen(string1);
	int length2 = strlen(string2);
	int total_length = length1+length2;
	char* new_string = (char*)malloc(total_length+1);
	int i = 0;
	int j = 0;
	while (string1[i] != 0) {
		new_string[j] = string1[i];
		i++;
		j++;
	}
	i = 0;
	while (string2[i] != 0) {
		new_string[j] = string2[i];
		i++;
		j++;
	}
	new_string[total_length] = 0;
	return new_string;
}

bool ends_with(char* string, char with) {
	int length = strlen(string);
	return (string[length-1] == with);
}

int charstoint(char c1, char c2, char c3, char c4) {
	int i1 = (int)c1*0x1000000;
	int i2 = (int)c2*0x10000;
	int i3 = (int)c3*0x100;
	int i4 = (int)c4;
	return i1+i2+i3+i4;
}

unsigned int charstouint(char c1, char c2, char c3, char c4) {
	int i1 = (int)((c1<0)?(c1+256):c1)*0x1000000;
	int i2 = (int)((c2<0)?(c2+256):c2)*0x10000;
	int i3 = (int)((c3<0)?(c3+256):c3)*0x100;
	int i4 = (int)((c4<0)?(c4+256):c4);
	return i1+i2+i3+i4;
}

unsigned int charstoushort(char c1, char c2) {
	int i1 = (int)((c1<0)?(c1+256):c1)*0x100;
	int i2 = (int)((c2<0)?(c2+256):c2);
	return i1+i2;
}

int charstoshort(char c1, char c2) {
	int i1 = (int)c1*0x1000000;
	int i2 = (int)c2*0x10000;
	return i1+i2;
}

unsigned int uchartouint(unsigned char ch) {
	return (unsigned int)ch;
}

unsigned int ucharstouint(unsigned char ch1, unsigned char ch2, unsigned char ch3, unsigned char ch4) {
	unsigned int u1 = (unsigned int)ch1;
	unsigned int u2 = (unsigned int)ch2;
	unsigned int u3 = (unsigned int)ch3;
	unsigned int u4 = (unsigned int)ch4;
	return u1*0x1000000+u2*0x10000+u3*0x100+u4;
}

unsigned int ucharstoushort(unsigned char ch1, unsigned char ch2) {
	unsigned int u1 = (unsigned int)ch1;
	unsigned int u2 = (unsigned int)ch2;
	return u1*0x100+u2;
}

char* get_node(char* string, char delimiter, int nodenum) {
	int i = 0;
	int j = 0;
	int k = 0;
	int current_node = 0;
	while (string[i] != 0) {
		if (string[i] == delimiter) {
			if (current_node == nodenum) {
				char* node = (char*)malloc(j+1);
				memcpy(node, string+k, j);
				node[j] = 0;
				return node;
			}
			j = 0;
			k = i+1;
			current_node++;
		} else {
			j++;
		}
		i++;
	}
	return 0;
}

char* upper_case(char* text) {
	int i = 0;
	while (text[i] != 0) {
		char ch = text[i];
		if (ch >= 'a' && ch <= 'z') {
			ch -= 32;
		}
		text[i] = ch;
		i++;
	}
	return text;
}

bool strcmpl(char* text1, char* text2, int length) {
	int i = 0;
	while (i < length) {
		if (text1[i] != text2[i]) {
			return false;
		}
		i++;
	}
	return true;
}

int index_of(char* string, char ch) {
	int i = 0;
	while (string[i] != 0) {
		if (string[i] == ch) {
			return i;
		}
		i++;
	}
	return -1;
}

int chartouchar(char ch) {
	int i = (int)((ch<0)?(ch+256):ch);
	return i;
}

int numlen(int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 10;
	}
	return length;
}

int numlenu(unsigned int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 10;
	}
	return length;
}

int numlenhex(unsigned int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 16;
	}
	return length;
}

char* strclone(char* text) {
	int length = strlen(text);
	char* clonedText = (char*)malloc(length+1);
	memcpy(clonedText, text, length);
	clonedText[length] = 0;
	return clonedText;
}

int get_last_char_index_from_string(char* text, char ch) {
	int length = strlen(text);
	int i = length-1;
	while (i >= 0) {
		if (text[i] == ch) {
			return i;
		}
		i--;
	}
	return -1;
}

int get_char_index_from_string(char* text, char ch, int from, int defaultValue) {
	int length = strlen(text);
	int i = from;
	while (i < length) {
		if (text[i] == ch) {
			return i;
		}
		i++;
	}
	return defaultValue;
}

int search_string(char* text, char* toSearch, int from, int defaultValue) {
	return defaultValue;
}

bool strcmp(char* text1, char* text2, int length) {
	int i = 0;
	while (text1[i] != 0) {
		if (text1[i] != text2[i]) {
			return false;
		}
		i++;
	}
	return true;
}

char* strcombine(char* text1, char* text2) {
	int length = strlen(text1)+strlen(text2);
	char* combinedStr = (char*)malloc(length+1);
	int i = 0;
	int j = 0;
	while (text1[i] != 0) {
		combinedStr[j] = text1[i];
		i++;
		j++;
	}
	i = 0;
	while (text2[i] != 0) {
		combinedStr[j] = text2[i];
		i++;
		j++;
	}
	combinedStr[length] = 0;
	return combinedStr;
}

int get_total_char_from_string(char* text, char ch) {
	int total = 0;
	int i = 0;
	while (text[i] != 0) {
		if (text[i] == ch) {
			total++;
		}
		i++;
	}
	return total;
}

int get_total_bits(int value, int bit) {
	int total_bits = 0;
	for (int i=0; i<32; i++) {
		int b = (value>>i)&1;
		if (b == bit) {
			total_bits++;
		}
	}
	return total_bits;
}

char* strdup(char* text, int from, int to) {
	int length = to-from+1;
	char* ret = (char*)malloc(length+1);
	memcpy(ret, text+from, length);
	ret[length] = 0;
	return ret;
}

char* get_next_token(char* text, char delimiter, int* next_token) {
	if (!ends_with(text, delimiter)) {
		text = strcombine(text, "/");
	}
	int target_token = *next_token;
	int current_token = 0;
	int i = 0;
	int j = 0;
	while (text[i] != 0) {
		if (text[i] == delimiter) {
			current_token++;
			if (target_token == 1) {
				if (current_token == target_token) {
					return strdup(text, 0, i-1);
				}
			} else {
				if (current_token == target_token-1) {
					j = i+1;
				} else if (current_token == target_token) {
					return strdup(text, j, i-1);
				}
			}
		}
		i++;
	}
	return "";
}

int pow10(int power) {
	if (power == 0) return 1;
	int number = 1;
	for (int i=0; i<power; i++) {
		number *= 10;
	}
	return number;
}

int strtoint(char* text) {
	int len = strlen(text)-1;
	int number = 0;
	int i = 0;
	while (text[i] != 0) {
		number += (((int)text[i]-'0')*pow10(len));
		i++;
		len--;
	}
	return number;
}

char* inttostr(int number) {
	if (number == 0) {
		char* str = malloc(2);
		str[0] = '0';
		str[1] = 0;
		return str;
	}
	int len = numlen(number);
	char* str = malloc(len+1);
	int i = len-1;
	while (number > 0) {
		char ch = number%10+'0';
		str[i] = ch;
		i--;
		number /= 10;
	}
	str[len] = 0;
	return str;
}
