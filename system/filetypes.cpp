#include <system.h>

int get_file_type(int* _data) {
	char* data = (char*)_data;
	if (data[0] == 'B' && data[1] == 'M') {
		return TYPE_BMP;
	} else if (data[1] == 'P' && data[2] == 'N' && data[3] == 'G') {
		return TYPE_PNG;
	}
	return TYPE_UNKNOWN;
}
