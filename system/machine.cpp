#include <system.h>

int currentActivity = 0;

void stop() {
	while (1);
}

void mainLoop() {
	while (true) {
		draw_cursor();
			if (is_left_clicked()) {
				for (int i=0; i<totalOnClickListeners; i++) {
					OnClickListenerInfo* info = (OnClickListenerInfo*)&onClickListeners[i];
					View* v = info->view;
					int mouseX = get_cursor_x();
					int mouseY = get_cursor_y();
					if (mouseX >= v->getX() && mouseX < (v->getX()+v->getWidth()) && mouseY >= v->getY() && mouseY < (v->getY()+v->getHeight())) {
						unsetClip();
						use_secondary_buffer();
						info->onClickListener(info->view);
					}
				}
			}
			for (int i=0; i<totalOnFocusListeners; i++) {
				OnFocusListenerInfo* info = (OnFocusListenerInfo*)&onFocusListeners[i];
				View* v = info->view;
				info->onFocusListener(v);
			}
			/*for (int i=0; i<totalOnHoverListeners; i++) {
				OnHoverListenerInfo* info = (OnHoverListenerInfo*)&onHoverListeners[i];
				View* v = info->view;
				int mouseX = get_cursor_x();
				int mouseY = get_cursor_y();
				if (mouseX >= v->getX() && mouseX < v->getX()+v->getWidth() && mouseY >= v->getY() && mouseY < v->getY()+v->getHeight()) {
					v->hovered = true;
					info->onHoverListener(v);
				} else {
					if (v->hovered) {
						v->hovered = false;
						info->onHoverListener(v);
					}
				}
			}*/
	}
}

void setActivity(int activity) {
	currentActivity = activity;
}

int getActivity() {
	return currentActivity;
}

void abort() {
}
