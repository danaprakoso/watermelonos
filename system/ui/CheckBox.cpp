#include <system.h>

void* checkBoxOnClickListener(View* view) {
	CheckBox* checkBox = (CheckBox*)view;
	fill_rect(checkBox->getX(), checkBox->getY(), checkBox->getWidth(), checkBox->getHeight(), 0xffffffff);
	bool checked = checkBox->isChecked();
	checked = !checked;
	checkBox->setChecked(checked);
	checkBox->apply();
	flush_at_area(checkBox->getX(), checkBox->getY(), checkBox->getWidth(), checkBox->getHeight());
}

CheckBox::CheckBox() {
	setViewType(VIEW_TYPE_CHECKBOX);
}

CheckBox::CheckBox(int x, int y, char* text, int textColor, bool checked) {
	setViewType(VIEW_TYPE_CHECKBOX);
	setX(x);
	setY(y);
	setText(text);
	setTextColor(textColor);
	setChecked(checked);
}

CheckBox::CheckBox(int x, int y, char* text, int textColor, bool checked, int id) {
	setViewType(VIEW_TYPE_CHECKBOX);
	setX(x);
	setY(y);
	setWidth(80);
	setHeight(20);
	setText(text);
	setTextColor(textColor);
	setChecked(checked);
	setId(id);
	setOnClickListener(checkBoxOnClickListener);
}

CheckBox::CheckBox(int x, int y, int width, int height, char* text, int textColor, bool checked, int id) {
	setViewType(VIEW_TYPE_CHECKBOX);
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	setText(text);
	setTextColor(textColor);
	setChecked(checked);
	setId(id);
	setOnClickListener(checkBoxOnClickListener);
}

void CheckBox::apply() {
	log("Applying CheckBox\n");
	if (checked) {
		draw_image(&checkbox_checked_image, getX(), getY(), 0);
	} else {
		draw_image(&checkbox_unchecked_image, getX(), getY(), 0);
	}
	drawText(text, getX()+20+5, getY()+20-5, 15, textColor);
}
