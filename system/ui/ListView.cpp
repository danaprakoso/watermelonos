#include <system.h>

ListView::ListView() {
	setViewType(VIEW_TYPE_LISTVIEW);
	items = (View**)malloc(sizeof(View*)*maxItems);
	columnCount = 2;
}

ListView::ListView(int x, int y, int width, int height) {
	setViewType(VIEW_TYPE_LISTVIEW);
	items = (View**)malloc(sizeof(View*)*maxItems);
	columnCount = 2;
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
}

void ListView::addItem(View* v) {
	if (totalItems >= maxItems) {
		maxItems *= 2;
		View** temp = (View**)malloc(sizeof(View*)*maxItems);
		for (int i=0; i<totalItems; i++) {
			temp[i] = items[i];
		}
		free(items);
		items = temp;
	}
	items[totalItems] = v;
	totalItems++;
}

ListView::~ListView() {
	for (int i=0; i<totalItems; i++) {
		delete items[i];
	}
	free(items);
}

void ListView::apply() {
	for (int i=0; i<totalItems; i++) {
		Container* v = (Container*)items[i];
		if (i == currentIndex) {
			v->setSelected(true);
		} else {
			v->setSelected(false);
		}
		v->apply();
	}
}
