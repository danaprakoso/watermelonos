#include <system.h>

CheckBoxGroup::CheckBoxGroup() {
	setViewType(8);
}

void CheckBoxGroup::addCheckBox(CheckBox* cb) {
	childs[totalChilds] = cb;
	totalChilds++;
}

void CheckBoxGroup::apply() {
	log("Applying CheckBoxGroup\n");
	for (int i=0; i<totalChilds; i++) {
		CheckBox* cb = childs[i];
		cb->apply();
	}
}
