#include <system.h>

RadioGroup::RadioGroup() {
	setViewType(6);
}

void RadioGroup::addRadioView(RadioView* v) {
	if (totalChilds >= maxChilds) {
		maxChilds *= 2;
		View** temp = (View**)malloc(sizeof(View*)*maxChilds);
		for (int i=0; i<totalChilds; i++) {
			temp[i] = childs[i];
		}
		free(childs);
		childs = temp;
	}
	childs[totalChilds] = v;
	totalChilds++;
	if (nextX == -1) {
		nextX = getX();
	}
	nextX += 100;
}

void RadioGroup::apply() {
	log("Applying RadioGroup\n");
	for (int i=0; i<totalChilds; i++) {
		RadioView* v = childs[i];
		v->apply();
	}
}
