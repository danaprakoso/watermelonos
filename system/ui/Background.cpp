#include <system.h>

Background::Background() {
}

SolidBackground::SolidBackground() {
	setType(BACKGROUND_TYPE_SOLID);
	color = 0xffffffff;
}

SolidBackground::SolidBackground(int color) {
	setType(BACKGROUND_TYPE_SOLID);
	this->color = color;
}
