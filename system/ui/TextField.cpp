#include <system.h>

bool textFieldCaretOn = false;
int textFieldCaretTimeout = 0;

void textFieldOnClickListener(View* view) {
	TextField* tf = (TextField*)view;
	if (tf->getParent() != NULL) {
		Container* container = (Container*)tf->getParent();
		View** parentChilds = container->getChilds();
		int parentChildCount = container->totalChilds;
		logf("Total TextField: %d\n", parentChildCount);
		for (int i=0; i<parentChildCount; i++) {
			if (parentChilds[i]->getViewType() == VIEW_TYPE_TEXTFIELD) {
				TextField* tf = (TextField*)parentChilds[i];
				tf->setFocused(false);
				fill_rect(tf->getX(), tf->getY(), tf->getWidth(), tf->getHeight(), 0xffffffff);
				drawText(tf->textBuffer, tf->getX()+tf->textX, tf->getY()+tf->getHeight()-6, 15, 0xff000000);
				tf->apply();
				flush_at_area(tf->getX(), tf->getY(), tf->getWidth(), tf->getHeight()+2);
			}
		}
	}
	tf->setFocused(true);
	fill_rect(tf->getX(), tf->getY(), tf->getWidth(), tf->getHeight(), 0xffffffff);
	tf->apply();
	flush_at_area(tf->getX(), tf->getY(), tf->getWidth(), tf->getHeight()+2);
}

void textFieldOnFocusListener(View* view) {
	TextField* tf = (TextField*)view;
	if (tf->isFocused()) {
		int caretColor;
		if (textFieldCaretOn) {
			caretColor = 0xff000000;
		} else {
			caretColor = 0xffffffff;
		}
		fill_rect(tf->getX()-10, tf->getY(), tf->totalChars*20+20, tf->getHeight(), 0xffffffff);
		draw_line(tf->getX()+tf->textFieldCaretX, tf->getY()+3, tf->getX()+tf->textFieldCaretX, tf->getY()+tf->getHeight()-3, caretColor);
		if (tf->totalChars < 1000) {
			if (isKeyPressed()) {
				char ch = getPressedKey();
				if (ch >= 32 && ch <= 126) {
					tf->apply();
					//drawChar(ch, tf->getX()+tf->textX, tf->getY()+tf->getHeight()-6, 0xff000000);
					tf->textBuffer[tf->totalChars] = ch;
					tf->totalChars++;
					int charWidth = getCharWidth(ch);
					tf->textFieldCaretX += charWidth;
					tf->totalTextWidth += charWidth;
					if (tf->textFieldCaretX >= (tf->getX()+tf->getWidth()-50)) {
						tf->textX -= 20;
						tf->textFieldCaretX -= 20;
					}
				} else if (ch == 8) {
					if (tf->totalChars > 0) {
						tf->apply();
						char lastChar = tf->textBuffer[tf->totalChars-1];
						tf->textBuffer[tf->totalChars-1] = 0;
						tf->totalChars--;
						int charWidth = getCharWidth(lastChar);
						tf->textFieldCaretX -= charWidth;
						tf->totalTextWidth -= charWidth;
						if (tf->totalTextWidth > tf->getWidth() && tf->textFieldCaretX <= (tf->getX()+300)) {
							tf->textX += 20;
							tf->textFieldCaretX += 20;
						} else if (tf->totalTextWidth < tf->getWidth()) {
							tf->textX = 3;
							tf->textFieldCaretX = tf->totalTextWidth;
						}
					}
				}
			}
		}
		setClip(tf->getX(), tf->getY(), tf->getWidth()-5, tf->getHeight());
		drawText(tf->textBuffer, tf->getX()+tf->textX, tf->getY()+tf->getHeight()-6, 15, 0xff000000);
		tf->apply();
		flush_at_area(tf->getX()-30, tf->getY(), tf->totalChars*20+60, tf->getHeight()+2);
		Container* container = (Container*)tf->getParent();
		setClip(container->getX(), container->getY(), container->getWidth(), container->getHeight());
	}
}

TextField::TextField() {
	setViewType(VIEW_TYPE_TEXTFIELD);
	setX(0);
	setY(0);
	setWidth(getScreenWidth());
	setHeight(40);
	strokeColor = 0xff000000;
	strokeWidth = 2;
	setOnClickListener(textFieldOnClickListener);
	setOnFocusListener(textFieldOnFocusListener);
	memset(textBuffer, 0, 1000);
}

void TextField::setBackground(Background* background) {
	if (this->background != NULL) free(this->background);
	this->background = background;
}

void TextField::apply() {
	log("Applying TextField\n");
	/*draw_line(getX(), getY(), getX()+getWidth(), getY(), strokeColor);
	draw_line(getX(), getY(), getX(), getY()+getHeight(), strokeColor);
	draw_line(getX()+getWidth(), getY(), getX()+getWidth(), getY()+getHeight(), strokeColor);*/
	int strokeColor = this->strokeColor;
	if (focused) {
		strokeColor = 0xff2196f3;
	} else {
		strokeColor = this->strokeColor;
	}
	draw_line(getX(), getY()+getHeight(), getX()+getWidth(), getY()+getHeight(), strokeColor);
}
