#include <system.h>

ImageButton::ImageButton(int* data, int dataSize, int x, int y, int width, int height) {
	this->data = data;
	this->dataSize = dataSize;
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
}

ImageButton::ImageButton(int* data, int dataSize, int x, int y, int width, int height, void (*onClickListener)(View* v)) {
	this->data = data;
	this->dataSize = dataSize;
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	this->onClickListener = onClickListener;
	registerOnClickListener(this, onClickListener);
}

ImageButton::ImageButton(int* data, int dataSize, int x, int y, int width, int height, int resizedWidth, int resizedHeight, void (*onClickListener)(View* v)) {
	this->data = data;
	this->dataSize = dataSize;
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	this->resizedWidth = resizedWidth;
	this->resizedHeight = resizedHeight;
	this->onClickListener = onClickListener;
	registerOnClickListener(this, onClickListener);
}

ImageButton::ImageButton(void (*onClickListener)(View* v)) {
	this->data = (int*)&close_icon;
	setX(0);
	setY(0);
	setWidth(100);
	setHeight(100);
	this->resizedWidth = 20;
	this->resizedHeight = 20;
	this->onClickListener = onClickListener;
	registerOnClickListener(this, onClickListener);
}

void ImageButton::apply() {
	draw_image(data, getX(), getY(), dataSize);
}
