#include <system.h>

Container::Container() {
	setViewType(VIEW_TYPE_CONTAINER);
	childs = (View**)malloc(sizeof(View*)*maxChilds);
	totalChilds = 0;
	maxChilds = 100;
}

Container::Container(int x, int y, int width, int height) {
	setViewType(VIEW_TYPE_CONTAINER);
	childs = (View**)malloc(sizeof(View*)*maxChilds);
	setPosition(x, y);
	setSize(width, height);
	totalChilds = 0;
	maxChilds = 100;
}

void Container::addChild(View* v) {
	if (totalChilds >= maxChilds) {
		maxChilds *= 2;
		View** temp = (View**)malloc(sizeof(View*)*maxChilds);
		for (int i=0; i<totalChilds; i++) {
			temp[i] = childs[i];
		}
		free(childs);
		childs = temp;
	}
	v->setParent(this);
	childs[totalChilds] = v;
	totalChilds++;
}

void Container::setSelected(bool b) {
	selected = b;
	for (int i=0; i<totalChilds; i++) {
		childs[i]->selected = b;
	}
}

void Container::apply() {
	int backgroundColor;
	if (isSelected()) {
		backgroundColor = selectedBackground;
	} else {
		backgroundColor = background;
	}
	fill_rect(getX(), getY(), getWidth(), getHeight(), backgroundColor);
	if (borderColor != 0) {
		draw_rect(getX(), getY(), getWidth(), getHeight(), borderColor);
	}
	if (title != NULL) {
		fill_rect(getX(), getY(), getWidth(), 30, titleBackgroundColor);
		drawText(title, getX()+50, getY()+20, 15, titleColor);
	}
	if (sideBar != NULL) {
		fill_rect(sideBar->getX(), sideBar->getY(), sideBar->getWidth(), sideBar->getHeight(), sideBar->backgroundColor);
		sideBar->apply();
	}
	if (closeButton != NULL) {
		draw_image(closeButton->data, closeButton->getX(), closeButton->getY(), 0);
	}
	for (int i=0; i<totalChilds; i++) {
		View* v = childs[i];
		int type = v->getViewType();
		switch (type) {
			case 1:
				((TextView*)v)->apply();
				break;
			case 2:
				((ImageView*)v)->apply();
				break;
			case 3:
				((Container*)v)->apply();
				break;
			case 4:
				((TextField*)v)->apply();
				break;
			case 5:
				((RadioView*)v)->apply();
				break;
			case 6:
				((RadioGroup*)v)->apply();
				break;
			case 7:
				((CheckBox*)v)->apply();
				break;
			case 8:
				((CheckBoxGroup*)v)->apply();
				break;
			case 9:
				((Button*)v)->apply();
				break;
			case VIEW_TYPE_LISTVIEW:
				((ListView*)v)->apply();
				break;
		}
	}
}

View* Container::getChildById(int id) {
	for (int i=0; i<totalChilds; i++) {
		View* child = childs[i];
		if (child->getId() == id) {
			return child;
		}
	}
	return NULL;
}

void Container::focus() {
	setClip(getX(), getY(), getWidth(), getHeight());
}

void Container::unfocus() {
	setClip(0, 0, getScreenWidth(), getScreenHeight());
}

Container::~Container() {
	/*for (int i=0; i<totalChilds; i++) {
		delete childs[i];
	}*/
	//free(childs);
	totalChilds = 0;
	maxChilds = 100;
	unregisterAllListeners();
	unfocus();
}
