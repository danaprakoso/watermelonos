#include <system.h>

void* radioViewOnClickListener(View* view) {
	RadioView* rv = (RadioView*)view;
	if (rv->getParent() != NULL) {
		Container* container = (Container*)rv->getParent();
		View** parentChilds = container->getChilds();
		int totalParentChilds = container->totalChilds;
		for (int i=0; i<totalParentChilds; i++) {
			RadioView* childRv = (RadioView*)parentChilds[i];
			childRv->setChecked(false);
			fill_rect(childRv->getX(), childRv->getY(), childRv->getWidth(), childRv->getHeight(), 0xffffffff);
			childRv->apply();
			flush_at_area(childRv->getX(), childRv->getY(), childRv->getWidth(), childRv->getHeight());
		}
	}
	rv->setChecked(true);
	fill_rect(rv->getX(), rv->getY(), rv->getWidth(), rv->getHeight(), 0xffffffff);
	rv->apply();
	flush_at_area(rv->getX(), rv->getY(), rv->getWidth(), rv->getHeight());
}

RadioView::RadioView() {
	setViewType(5);
}

RadioView::RadioView(int centerX, int centerY, char* text, int textColor, bool checked) {
	this->cx = centerX;
	this->cy = centerY;
	setViewType(5);
	setText(text);
	setTextColor(textColor);
	setChecked(checked);
}

RadioView::RadioView(int centerX, int centerY, char* text, int textColor, bool checked, int id) {
	this->cx = centerX;
	this->cy = centerY;
	setX(centerX-10);
	setY(centerY-10);
	int textWidth = getTextWidth(text, 15);
	setWidth(20+5+textWidth);
	setHeight(20);
	setViewType(5);
	setText(text);
	setTextColor(textColor);
	setChecked(checked);
	setId(id);
	setOnClickListener(radioViewOnClickListener);
}

void RadioView::apply() {
	log("Applying RadioView\n");
	if (checked) {
		draw_circle(cx, cy, 10, checkedBorderColor, 2);
		fill_circle(cx, cy, 5, checkedColor);
	} else {
		draw_circle(cx, cy, 10, uncheckedBorderColor, 2);
		fill_circle(cx, cy, 5, uncheckedColor);
	}
	TextViewInfo textViewInfo;
	textViewInfo.text = text;
	textViewInfo.x = cx+20+5;
	textViewInfo.y = cy+10-5;
	textViewInfo.color = textColor;
	call(0x13, (int)&textViewInfo, 0, 0);
}
