#include <system.h>

OnClickListenerInfo onClickListeners[100];
OnFocusListenerInfo onFocusListeners[100];
OnHoverListenerInfo onHoverListeners[100];
int maxOnClickListeners = 100;
int totalOnClickListeners = 0;
int maxOnFocusListeners = 100;
int totalOnFocusListeners = 0;
int maxOnHoverListeners = 100;
int totalOnHoverListeners = 0;

View::View() {
	buffer = (int*)malloc(sizeof(int)*getWidth()*getHeight());
	x = 0;
	y = 0;
	width = getWidth();
	height = getHeight();
}

void View::setSize(int w, int h) {
	free(buffer);
	buffer = (int*)malloc(sizeof(int)*w*h);
	width = w;
	height = h;
}

int* View::getBuffer() {
	return buffer;
}

void View::setPosition(int x, int y) {
	this->x = x;
	this->y = y;
}

int View::getX() {
	return x;
}

int View::getY() {
	return y;
}

int View::getWidth() {
	return width;
}

int View::getHeight() {
	return height;
}

void View::setBackground(int bg) {
	background = bg;
}

void View::setViewType(int type) {
	this->type = type;
}

void registerOnClickListener(View* v, void (*onClickListener)(View* v)) {
	OnClickListenerInfo* info = (OnClickListenerInfo*)&onClickListeners[totalOnClickListeners];
	info->view = v;
	info->onClickListener = onClickListener;
	totalOnClickListeners++;
}

void registerOnFocusListener(View* v, void (*onFocusListener)(View* v)) {
	OnFocusListenerInfo* info = (OnFocusListenerInfo*)&onFocusListeners[totalOnFocusListeners];
	info->view = v;
	info->onFocusListener = onFocusListener;
	totalOnFocusListeners++;
}

void registerOnHoverListener(View* v, void (*onHoverListener)(View* v)) {
	OnHoverListenerInfo* info = (OnHoverListenerInfo*)&onHoverListeners[totalOnHoverListeners];
	info->view = v;
	info->onHoverListener = onHoverListener;
	totalOnHoverListeners++;
}

void unregisterAllListeners() {
	totalOnFocusListeners = 0;
	memset(onClickListeners, 0, sizeof(OnClickListenerInfo)*100);
	totalOnFocusListeners = 0;
	memset(onFocusListeners, 0, sizeof(OnFocusListenerInfo)*100);
	totalOnHoverListeners = 0;
	memset(onHoverListeners, 0, sizeof(OnHoverListenerInfo)*100);
}
