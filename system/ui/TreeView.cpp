#include <system.h>

void treeViewOnClickListener(View* view) {
	TreeView* tv = (TreeView*)view;
	SideBar* parent = (SideBar*)tv->getParent();
	int mouse_x = get_cursor_x();
	int mouse_y = get_cursor_y();
	int selectedIndex = (mouse_y-parent->getY()-20)/40;
	tv->selectedNode = selectedIndex;
	tv->nextY = parent->getY()+20;
	parent->apply();
	flush_at_area(tv->getX(), tv->getY(), tv->getWidth(), tv->getHeight());
}

TreeView::TreeView() {
	setViewType(VIEW_TYPE_TREEVIEW);
	nodes = (TreeNode**)malloc(sizeof(TreeNode*)*maxNodes);
	setOnClickListener(treeViewOnClickListener);
}

void TreeView::addNode(ImageView* img, char* title, int textColor, int backgroundColor, int selectedTextColor, int selectedBackgroundColor) {
	if (totalNodes >= maxNodes) {
		TreeNode** temp = (TreeNode**)malloc(sizeof(TreeNode*)*maxNodes);
		for (int i=0; i<totalNodes; i++) {
			temp[i] = nodes[i];
		}
		free(nodes);
		nodes = temp;
	}
	TreeNode* node = (TreeNode*)malloc(sizeof(TreeNode));
	node->icon = img;
	node->title = title;
	node->textColor = textColor;
	node->backgroundColor = backgroundColor;
	node->selectedTextColor = selectedTextColor;
	node->selectedBackgroundColor = selectedBackgroundColor;
	nodes[totalNodes] = node;
	totalNodes++;
}
