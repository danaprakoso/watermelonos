#include <system.h>

void buttonHoverListener(View* view) {
	Button* button = (Button*)view;
	if (button->hovered) {
		fill_rect(button->getX(), button->getY(), button->getWidth(), button->getHeight(), button->hoveredBackgroundColor);
	} else {
		fill_rect(button->getX(), button->getY(), button->getWidth(), button->getHeight(), button->backgroundColor);
		button->apply();
	}
	int textWidth = getTextWidth(button->text, 15);
	drawText(button->text, button->getX()+(button->getWidth()/2)-(textWidth/2), button->getY()+8+15, 15, 0xffffffff);
	if (button->hovered) {
		draw_cursor_forcely();
	}
	flush_at_area(button->getX(), button->getY(), button->getWidth(), /*getScreenHeight()-button->getY()*/button->getHeight());
}

Button::Button() {
	setViewType(9);
}

Button::Button(int x, int y, int width, int height, char* text) {
	setViewType(9);
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	this->text = text;
}

Button::Button(int x, int y, int width, int height, char* text, void (*onClickListener)(View* v)) {
	setViewType(9);
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	this->text = text;
	this->onClickListener = onClickListener;
	registerOnClickListener(this, onClickListener);
}

Button::Button(int x, int y, int width, int height, char* text, int backgroundColor, void (*onClickListener)(View* v)) {
	setViewType(9);
	setX(x);
	setY(y);
	setWidth(width);
	setHeight(height);
	this->text = text;
	this->backgroundColor = backgroundColor;
	this->onClickListener = onClickListener;
	registerOnClickListener(this, onClickListener);
}

void Button::apply() {
	int textWidth = getTextWidth(text, 15);
	fill_rect(getX(), getY(), getWidth(), getHeight(), getBackgroundColor());
	drawText(text, getX()+(getWidth()/2)-(textWidth/2), getY()+8+15, 15, 0xffffffff);
	int mouseX = get_cursor_x();
	int mouseY = get_cursor_y();
	if (is_left_clicked()) {
		if (mouseX >= getX() && mouseX < (getX()+getWidth()) && mouseY >= getY() && mouseY < (getY()+getHeight())) {
			onClickListener(this);
		}
	}
}
