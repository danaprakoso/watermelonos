#include <system.h>

ImageView::ImageView() {
	setViewType(VIEW_TYPE_IMAGEVIEW);
}

void ImageView::setData(int* data) {
	this->data = data;
}

void ImageView::apply() {
	if (resizedWidth != 0 && resizedHeight != 0) {
		parse_image_info(data);
		int width = getDecodedWidth();
		int height = getDecodedHeight();
		if (width != resizedWidth || height != resizedHeight) {
			draw_and_resize_image(data, getX(), getY(), resizedWidth, resizedHeight);
		}
	} else {
		draw_image(data, getX(), getY(), 0);
	}
}

void ImageView::resizeTo(int w, int h) {
	resizedWidth = w;
	resizedHeight = h;
}

int ImageView::getResizedWidth() {
	return resizedWidth;
}

int ImageView::getResizedHeight() {
	return resizedHeight;
}
