#include <system.h>

TextView::TextView() {
	setViewType(VIEW_TYPE_TEXTVIEW);
	this->color = 0xff000000;
	this->textSize = DEFAULT_TEXT_SIZE;
	call(0x12, this->textSize, 0, 0);
}

TextView::TextView(char* text, int x, int y, int color) {
	setViewType(VIEW_TYPE_TEXTVIEW);
	this->text = text;
	setX(x);
	setY(y);
	this->color = color;
	this->textSize = DEFAULT_TEXT_SIZE;
	call(0x12, this->textSize, 0, 0);
}

TextView::TextView(char* text, int x, int y, int color, int selectedColor) {
	setViewType(VIEW_TYPE_TEXTVIEW);
	this->text = text;
	setX(x);
	setY(y);
	this->color = color;
	this->selectedColor = selectedColor;
	this->textSize = DEFAULT_TEXT_SIZE;
	call(0x12, this->textSize, 0, 0);
}

void TextView::setColor(int color) {
	this->color = color;
}

void TextView::setText(char* text) {
	this->text = text;
}

void TextView::setTextSize(int size) {
	textSize = size;
}

void TextView::apply() {
	int color;
	if (isSelected()) {
		color = selectedColor;
	} else {
		color = this->color;
	}
	TextViewInfo textViewInfo;
	textViewInfo.text = text;
	textViewInfo.x = getX();
	textViewInfo.y = getY();
	textViewInfo.color = color;
	call(0x13, (int)&textViewInfo, 0, 0);
}
