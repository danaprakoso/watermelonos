#include <system.h>

void SideBar::apply() {
	fill_rect(getX(), getY(), getWidth(), getHeight(), backgroundColor);
	if (content != NULL) {
		switch (content->getViewType()) {
			case VIEW_TYPE_TREEVIEW: {
				TreeView* tv = (TreeView*)content;
				SideBar* parent = (SideBar*)tv->getParent();
				for (int i=0; i<tv->totalNodes; i++) {
					TreeNode* node = tv->nodes[i];
					if (tv->nextY == -1) {
						tv->nextY = parent->getY()+20;
					}
					int textColor;
					int backgroundColor;
					if (i == tv->selectedNode) {
						textColor = node->selectedTextColor;
						backgroundColor = node->selectedBackgroundColor;
					} else {
						textColor = node->textColor;
						backgroundColor = node->backgroundColor;
					}
					fill_rect(parent->getX(), tv->nextY, parent->getWidth(), 40, backgroundColor);
					draw_image(node->icon->data, parent->getX()+10, tv->nextY+8, 0);
					drawText(node->title, parent->getX()+35, tv->nextY+40-15, 15, textColor);
					tv->nextY += 40;
				}
				break;
			}
		}
	}
}
