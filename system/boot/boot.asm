BITS 16

start:
jmp short skip_bpb
nop

db "WATERMEL"
dw 512
db 1
dw 1
db 2
dw 224
dw 2880
db 0F0h
dw 9
dw 18
dw 2
dd 0
dd 0
dw 0
db 41
dd 00000000h
db "WATERMELON "
db "FAT12   "

skip_bpb:
	mov ax, 0x07C0
	add ax, 544
	mov ss, ax
	mov sp, 4096
	mov ax, 0x07c0
	mov ds, ax
	
	mov ax, 19
	call get_chs
	mov si, buffer
	mov bx, ds
	mov es, bx
	mov bx, si
	mov ah, 2
	mov al, 14
	stc
	int 13h

	mov ax, ds
	mov es, ax
	mov di, buffer
	mov cx, 224
	mov ax, 0
.loop0:
	xchg cx, dx
	mov si, game_filename
	mov cx, 11
	rep cmpsb
	je .found
	add ax, 32
	mov di, buffer
	add di, ax
	xchg dx, cx
	loop .loop0
	jmp $

.found:
	mov ax, word [es:di+0Fh]
	mov word [cluster], ax
	
	mov ax, 1
	call get_chs
	mov di, buffer
	mov bx, di
	mov ah, 2
	mov al, 9
	stc
	int 13h

load_game:
	mov ax, word [cluster]
	add ax, 31
	call get_chs
	mov ax, 0
	mov es, ax
	mov bx, word [pointer]
	mov ah, 2
	mov al, 1
	stc
	int 13h
	
	mov ax, [cluster]
	mov dx, 0
	mov bx, 3
	mul bx
	mov bx, 2
	div bx
	mov si, buffer
	add si, ax
	mov ax, word [ds:si]
	or dx, dx
	jz .1

	shr ax, 4
	jmp short .2

.1:
	and ax, 0FFFh

.2:
	mov word [cluster], ax
	cmp ax, 0FF8h
	jae end
	add word [pointer], 512
	jmp load_game

end:
	mov dl, byte [bootdev]
	jmp 0x00:0x500

get_chs:
	push bx
	push ax
	mov bx, ax
	mov dx, 0
	div word [sectors_per_track]
	add dl, 01h
	mov cl, dl
	mov ax, bx
	mov dx, 0
	div word [sectors_per_track]
	mov dx, 0
	div word [head_count]
	mov dh, dl
	mov ch, al
	pop ax
	pop bx
	mov dl, byte [bootdev]
	ret

game_filename db "BOOT2   BIN"
bootdev	db 0
cluster	dw 0
pointer	dw 0x500
sectors_per_track dw 18
head_count dw 2

times 510-($-$$) db 0
dw 0AA55h

buffer:
