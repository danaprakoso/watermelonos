[BITS 16]
ORG 0x500

start:
	mov ax, 0 ;Set segment to 0
	mov ds, ax
	mov es, ax
	mov ss, ax ;Set stack segment to 0
	mov sp, 0x7E00 ;Set stack offset to 0x7E00 (making stack 0x0:0x7E00)
	
	mov ah, 0eh
	mov al, 3
	int 10h
	
	.loop_identify_memory:
	mov bx, ds
	mov es, bx
	mov di, ram_info_buffer
	mov eax, 0xE820
	mov edx, 0x534D4150
	mov ebx, [next_memory_offset_to_identify]
	mov ecx, 24
	int 15h
	cmp ebx, 0
	je .finish_identifying_memory
	mov eax, [ram_info_buffer+8]
	add dword [total_memory], eax
	mov [next_memory_offset_to_identify], ebx
	jmp .loop_identify_memory
	.finish_identifying_memory:
	
	mov ax, 0x4F00
	mov di, VbeInfoBlock
	int 10h
	
	mov eax, [VbeInfoBlock.video_modes_ptr]
	shr eax, 16
	mov es, ax
	mov eax, [VbeInfoBlock.video_modes_ptr]
	mov di, ax
	push es
	push di
	.find_mode:
	pop di
	pop es
	mov ax, [es:di]
	add di, 2
	push es
	push di
	cmp ax, 0
	je .find_mode
	cmp ax, 0x100
	jl .find_mode
	cmp ax, 0xFFFF
	je .no_graphics_mode_found
	mov [mode_number], ax
	mov bx, ds
	mov es, bx
	mov cx, ax
	mov ax, 0x4F01
	mov di, ModeInfoBlock
	int 10h
	
	mov ax, [ModeInfoBlock.attr]
	and ax, 1
	cmp ax, 0
	je .find_mode
	
	mov ax, [ModeInfoBlock.attr]
	shr ax, 3
	and ax, 1
	cmp ax, 0
	je .monochrome_type
	
	mov ax, [ModeInfoBlock.attr]
	shr ax, 4
	and ax, 1
	cmp ax, 0
	je .not_graphics_mode
	
	mov ax, [ModeInfoBlock.attr]
	shr ax, 5
	and ax, 1
	cmp ax, 0
	jne .not_vga_compatible
	
	mov ax, [ModeInfoBlock.attr]
	shr ax, 7
	and ax, 1
	cmp ax, 0
	je .no_linear_buffer_mode
	
	cmp word [ModeInfoBlock.width], 640
	jge .6
	jmp .find_mode
	
	.6:
	cmp word [ModeInfoBlock.width], 480
	jge .7
	jmp .find_mode
	
	.7:
	cmp byte [ModeInfoBlock.bits_per_pixel], 24
	jge .8
	jmp .find_mode
	
	.8:
	mov ax, 0x4F02
	mov bx, ds
	mov es, bx
	mov di, CRTCInfoBlock
	mov bx, [mode_number]
	or bx, 0x4000
	int 10h
	cmp al, 0x4F
	je .1
	mov ax, error1
	call print_string_16
	jmp $
	.1:
	cmp ah, 0x0
	je .2
	cmp ah, 0x01
	je .3
	cmp ah, 0x02
	je .4
	cmp ah, 0x03
	je .5
	mov ax, error5
	call print_string_16
	jmp $
	.3:
	mov ax, error2
	call print_string_16
	jmp $
	.4:
	mov ax, error3
	call print_string_16
	jmp $
	.5:
	mov ax, error4
	call print_string_16
	jmp $
	.2:
	pop di
	pop es
	
	; Change to Text Mode
	mov ah, 0
	mov al, 3
	;int 10h
	
	in al, 0x92
	or al, 2
	out 0x92, al
	cli
	lgdt [gdt_ptr]
	mov eax, cr0
	or eax, 1
	mov cr0, eax
	jmp 0x8:ProtectedMode

.monochrome_type:
	add byte [monochrome_causes], 1
	jmp .find_mode

.not_graphics_mode:
	add byte [no_graphics_mode_causes], 1
	jmp .find_mode

.not_vga_compatible:
	add byte [no_vga_compatible_causes], 1
	jmp .find_mode

.no_linear_buffer_mode:
	add byte [no_linear_buffer_mode_causes], 1
	jmp .find_mode

.no_graphics_mode_found:
	mov eax, [errorcode1]
	call print_number_16
	jmp $

ram_info_buffer times 24 db 0
next_memory_offset_to_identify dd 0

gdt:
	; NULL
	dq 0
	; Code
	dw 0xFFFF
	dw 0
	db 0
	db 0x9A
	db 0xCF
	db 0
	; Data
	dw 0xFFFF
	dw 0
	db 0
	db 0x92
	db 0xCF
	db 0
	; 16-bit Code
	dw 0xFFFF
	dw 0
	db 0
	db 0x9A
	db 0x8F
	db 0
	; 16-bit Data
	dw 0xFFFF
	dw 0
	db 0
	db 0x92
	db 0xCF
	db 0

gdt_ptr:
	dw 5*8-1
	dd gdt

error1 db "Set graphics mode: Function is not supported", 0
error2 db "Set graphics mode: Function call failed", 0
error3 db "Set graphics mode: Function is not supported in the current hardware configuration", 0
error4 db "Set graphics mode: Function call invalid in current video mode", 0
error5 db "Set graphics mode: Undefined error", 0
error6 db "No storage controller found. Make sure you plug it one.", 0
error7 db "No CD/DVD device found", 0
error8 db "CD/DVD files are corrupt. Can't be read", 0
error9 db "Error reading DVD, caused by ERR bit set in PxTFD of port 1", 0

errorcode1 dd 1 ;No graphics mode available

total_video_mode dw 0
total_memory dd 0
mode_number dw 0
monochrome_causes db 0 ;Set how many error caused by Monochrome Type
no_graphics_mode_causes db 0 ;Set how many error caused by No Graphics Mode
no_vga_compatible_causes db 0 ;Set how many error caused by Not VGA Compatible
no_linear_buffer_mode_causes db 0 ;Set how many error caused by No Linear Frame Buffer Mode
	
[BITS 32]
ProtectedMode:
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, 0x7E00
	
	call pci_search_device
	cmp eax, -1
	je .no_storage_controller_found
	cmp eax, 1
	je .ide_found
	cmp eax, 2
	je ahci_found
	jmp .no_storage_controller_found
	
	.ide_found:
	;; Load loader using IDE
	mov eax, [ide_bus]
	mov ebx, [ide_slot]
	mov ecx, [ide_func]
	mov edx, 0x10
	call pci_read_dword
	and eax, 0xFFFFFFFC
	mov [ide_primary_bar], eax
	cmp eax, 0
	je .1
	cmp eax, 1
	je .1
	jmp .skip_1
	.1:
	mov dword [ide_primary_bar], 0x1F0
	.skip_1:
	mov eax, [ide_bus]
	mov ebx, [ide_slot]
	mov ecx, [ide_func]
	mov edx, 0x18
	call pci_read_dword
	and eax, 0xFFFFFFFC
	mov [ide_secondary_bar], eax
	cmp eax, 0
	je .2
	cmp eax, 1
	je .2
	jmp .skip_2
	.2:
	mov dword [ide_secondary_bar], 0x170
	.skip_2:
	
	;; Search for ATAPI device
	mov dx, [ide_primary_bar]
	mov [ide_bar], dx
	mov byte [ide_device], 0
	add dx, 6
	mov al, 0x00
	out dx, al
	add dx, 1
	mov al, 0xA1
	out dx, al
	in al, dx
	cmp al, 0
	je .skip_3
	and al, 0x01
	cmp al, 0
	je .atapi_found
	
	.skip_3:
	mov dx, [ide_primary_bar]
	mov [ide_bar], dx
	mov byte [ide_device], 1
	add dx, 6
	mov al, (1<<4)
	out dx, al
	add dx, 1
	mov al, 0xA1
	out dx, al
	in al, dx
	cmp al, 0
	je .skip_4
	and al, 0x01
	cmp al, 0
	je .atapi_found
	
	.skip_4:
	mov dx, [ide_secondary_bar]
	mov [ide_bar], dx
	mov byte [ide_device], 0
	add dx, 6
	mov al, 0x00
	out dx, al
	add dx, 1
	mov al, 0xA1
	out dx, al
	in al, dx
	cmp al, 0
	je .skip_5
	and al, 0x01
	cmp al, 0
	je .atapi_found
	
	.skip_5:
	mov dx, [ide_secondary_bar]
	mov [ide_bar], dx
	mov byte [ide_device], 1
	add dx, 6
	mov al, (1<<4)
	out dx, al
	add dx, 1
	mov al, 0xA1
	out dx, al
	in al, dx
	cmp al, 0
	je .skip_6
	and al, 0x01
	cmp al, 0
	je .atapi_found
	.skip_6:
	mov eax, error7
	call print_string
	jmp $
	
	.atapi_found:
	mov dx, [ide_bar]
	call ide_wait
	mov ecx, 256
	.loop_read:
	in ax, dx
	loop .loop_read
	call load_loader
	ret
	
.no_storage_controller_found:
	mov eax, error6
	call print_string
	jmp $

ahci_found:
	mov eax, [ahci_bus]
	mov ebx, [ahci_slot]
	mov ecx, [ahci_func]
	mov edx, 0x24
	call pci_read_dword
	and eax, 0xFFFFE000
	mov [ahci_bar], eax
	
	;; Stopping port 0
	mov esi, [ahci_bar]
	add esi, 0x100
	mov eax, [esi+18h]
	and eax, 0xFFFFFFFE
	mov [esi+18h], eax
	.wait4:
	mov eax, [esi+38h]
	cmp eax, 0
	jne .wait4
	
	;; Setting CLB and FB
	mov dword [esi], 0x200000
	mov dword [esi+4], 0x0
	mov dword [esi+8], 0x500000
	mov dword [esi+12], 0x0
	
	;;; Setup command list
	mov edi, 0x200000
	mov al, 0
	mov ecx, 400h
	rep stosb
	mov edi, 0x200000
	mov eax, (1<<16) ;PRDTL
	mov dword [edi+8], 0x300000
	mov dword [edi+12], 0x00
	mov [edi], eax
	
	;; Start port 0
	mov eax, [esi+18h]
	.wait5:
	and eax, 0x8000
	cmp eax, 0
	jne .wait5
	mov eax, [esi+18h]
	or eax, (1<<4)
	mov [esi+18h], eax
	mov eax, [esi+18h]
	or eax, 1
	mov [esi+18h], eax
	
	;; Clear interrupts
	mov dword [esi+10h], 0xFFFFFFFF
	
	;; Setup command list (again)
	mov edi, 0x200000
	mov eax, [edi]
	or eax, 5
	mov [edi], eax
	mov eax, [edi]
	and eax, 0xFFFFFFBF
	mov [edi], eax
	mov eax, [edi]
	or eax, (1<<16)
	mov [edi], eax
	
	;; Build command table
	mov edi, 0x300000
	mov al, 0
	mov ecx, 90h
	rep stosb
	mov edi, 0x300000
	add edi, 80h
	mov dword [edi], 0x400000 ;Data BAR
	mov dword [edi+4], 0x00
	mov eax, 511
	mov [edi+12], eax
	mov edi, 0x300000
	mov byte [edi], 0x27
	mov byte [edi+1], 1
	mov byte [edi+2], 0x25
	mov byte [edi+4], 0 ;LBA0
	mov byte [edi+7], 0 ;LBA mode
	mov byte [edi+12], 1 ;Count low
	
	;;; PxCMD (offset 18h)
	;;; Set bit 24 to indicate ATAPI device
	;mov eax, [edi+18h]
	;or eax, (1<<24)
	;mov [edi+18h], eax
	
	;; Wait for the BSY and DRQ bit to zero
	mov esi, [ahci_bar]
	add esi, 0x100
	.wait6:
	mov eax, [esi+20h]
	and eax, 0x88
	cmp eax, 0
	jne .wait6
	
	;;; Command Issue slot 0
	mov eax, [esi+38h]
	or eax, 1
	mov [esi+38h], eax
	
	.wait2:
	mov eax, [esi+20h]
	and eax, 0x88
	cmp eax, 0
	jne .wait2
	
	.success:
	mov eax, 0x400000
	mov ebx, 512
	call dump
	
	jmp $
	
	.error:
	mov eax, error9
	call print_string
	jmp $

;;; Command List at 0x200000
;;; Command Table BAR at 0x300000
;;; Data Base Address at 0x400000
;;; FIS address at 0x500000

ahci_bus dd 0
ahci_slot dd 0
ahci_func dd 0
ahci_bar dd 0
num_ports dd 0
sam db 0 ;Supports AHCI-mode only

load_loader:
	;; Set transfer mode
	mov dx, [ide_bar]
	add dx, 1
	mov al, 0xC4
	out dx, al
	add dx, 5
	mov al, 0x00
	out dx, al
	add dx, 1
	mov al, 0xEF
	out dx, al
	mov dx, [ide_bar]
	call ide_wait
	
	mov dword [current_lba], 15
	.find_pvd: ;; Find Primary Volume Descriptor
	add dword [current_lba], 1
	cmp dword [current_lba], 2295104
	jge .pvd_not_found
	mov eax, [current_lba]
	mov ebx, 1
	mov ecx, 0x200000
	call read_sector
	
	mov al, [0x200000]
	cmp al, 255
	je .pvd_not_found
	cmp al, 0x01
	jne .find_pvd
	
	mov eax, [0x200000+156+2]
	mov [root_dir_lba], eax
	mov eax, [0x200000+156+10]
	mov [root_dir_size], eax
	mov ebx, 2048
	xor edx, edx
	div ebx
	cmp edx, 0
	jne .3
	jmp .skip_3
	.3:
	add eax, 1
	.skip_3:
	mov [root_dir_sector_count], eax
	
	mov eax, [root_dir_lba]
	mov ebx, [root_dir_sector_count]
	mov ecx, 0x200000
	call read_sector
	
	;; Read root directory
	;; Find loader.bin file
	mov esi, 0x200000
	xor ecx, ecx
	.loop1:
	cmp ecx, [root_dir_size]
	jge .loader_not_found
	mov eax, ecx
	push ecx
		cmp byte [esi], 0
		je .loader_not_found
		push esi
		add esi, 33
		mov edi, loader_filename
		mov ecx, 12
		rep cmpsb
		je .found
		pop esi
		pop ecx
		mov al, [esi]
		movzx eax, al
		add ecx, eax
		add esi, eax
		mov eax, ecx
		mov ebx, 2
		xor edx, edx
		div ebx
		cmp edx, 0
		jne .4
		jmp .skip_4
		.4:
		add esi, 1
		add ecx, 1
		.skip_4:
		push ecx
	pop ecx
	jmp .loop1
	.loader_not_found:
	mov eax, error8
	call print_string
	jmp $
	
	.found:
	pop esi
	pop ecx
	mov eax, [esi+2]
	mov [loader_lba], eax
	mov eax, [esi+10]
	mov [loader_size], eax
	mov ebx, 2048
	xor edx, edx
	div ebx
	cmp edx, 0
	jne .5
	jmp .skip_5
	.5:
	add eax, 1
	.skip_5:
	mov [loader_sector_count], eax
	
	mov eax, [loader_lba]
	mov ebx, [loader_sector_count]
	mov ecx, 0x2000000
	call read_sector
	
	mov esi, 0x2000000
	mov edi, ELFHeader
	mov ecx, 52
	rep movsb
	
	.load_elf_program_headers:
	mov ax, [ELFHeader.phnum]
	movzx ecx, ax
	.loop3:
	push ecx
	
	mov esi, 0x2000000
	add esi, [ELFHeader.phoff]
	mov edi, ELFProgramHeader
	mov ecx, 32
	rep movsb
	
	mov esi, 0x2000000
	add esi, [ELFProgramHeader.offset]
	mov edi, [ELFProgramHeader.vaddr]
	cmp edi, 0
	je .skip_7
	mov ecx, [ELFProgramHeader.filesz]
	cmp ecx, 0
	je .skip_7_2
	rep movsb
	.skip_7_2:
	mov edi, [ELFProgramHeader.vaddr]
	add edi, [ELFProgramHeader.filesz]
	mov al, 0
	mov ecx, [ELFProgramHeader.memsz]
	sub ecx, [ELFProgramHeader.filesz]
	cmp ecx, 0
	je .skip_7
	rep stosb
	.skip_7:
	
	pop ecx
	add dword [ELFHeader.phoff], 32
	loop .loop3
	
	jmp .jump_now
	
	.load_elf_sections:
	
	mov ax, [ELFHeader.shnum]
	movzx ecx, ax
	.loop2:
	push ecx
	
	mov esi, 0x2000000
	add esi, [ELFHeader.shoff]
	mov edi, ELFSectionHeader
	mov ecx, 40
	rep movsb
	
	cmp dword [ELFSectionHeader.type], 8
	je .6
	mov esi, 0x2000000
	add esi, [ELFSectionHeader.offset]
	mov edi, [ELFSectionHeader.addr]
	cmp edi, 0
	je .skip_6
	mov ecx, [ELFSectionHeader.size]
	rep movsb
	jmp .skip_6
	.6:
	mov edi, [ELFSectionHeader.addr]
	mov ecx, [ELFSectionHeader.size]
	mov al, 0
	rep stosb
	.skip_6:
	
	pop ecx
	add dword [ELFHeader.shoff], 40
	loop .loop2
	
	.jump_now:
	
	mov dword [vesa_info.vbeInfoBlock], VbeInfoBlock
	mov dword [vesa_info.modeInfoBlock], ModeInfoBlock
	
	mov eax, vesa_info
	mov ebx, [total_memory]
	mov ecx, [ide_bus]
	mov edx, [ide_slot]
	mov esi, [ide_func]
	mov edi, 1		;Controller type: IDE (1) or AHCI (2)
	jmp dword [ELFHeader.entry]

.pvd_not_found:
	mov eax, error8
	call print_string
	jmp $

vesa_info:
	.vbeInfoBlock dd 0
	.modeInfoBlock dd 0

loader_filename db "KERNEL.BIN;1"
current_lba dd 16
root_dir_lba dd 0
root_dir_size dd 0
root_dir_sector_count dd 0
loader_lba dd 0
loader_size dd 0
loader_sector_count dd 0

ELFHeader:
	.ident times 16 db 0
	.type dw 0
	.machine dw 0
	.version dd 0
	.entry dd 0
	.phoff dd 0
	.shoff dd 0
	.flags dd 0
	.ehsize dw 0
	.phentsize dw 0
	.phnum dw 0
	.shentsize dw 0
	.shnum dw 0
	.shstrndx dw 0

ELFProgramHeader:
	.type dd 0
	.offset dd 0
	.vaddr dd 0
	.paddr dd 0
	.filesz dd 0
	.memsz dd 0
	.flags dd 0
	.align dd 0

ELFSectionHeader:
	.name dd 0
	.type dd 0
	.flags dd 0
	.addr dd 0
	.offset dd 0
	.size dd 0
	.link dd 0
	.info dd 0
	.addralign dd 0
	.entsize dd 0

phnum dd 0

read_sector:
	cmp dword [controller_mode], 1
	je .ahci
	call ide_read_sector
	ret
	
	.ahci:
	call ahci_read_sector
	ret

ahci_read_sector:
	; EAX = lba
	; EBX = count
	; ECX = buffer
	pusha
	mov [.lba], eax
	mov [.count], ebx
	mov [.buffer], ecx
	
	
	.lba dd 0
	.count dd 0
	.buffer dd 0

ide_read_sector:
	; EAX = lba
	; EBX = count
	; ECX = buffer
	pusha
	mov [.lba], eax
	mov [.count], ebx
	mov [.buffer], ecx
	mov dword [.attempts], -1
	.read_again:
	add dword [.attempts], 1
	cmp dword [.attempts], 65536
	jge .error1
	mov dx, [ide_bar]
	add dx, 1
	mov al, 0x00
	out dx, al
	add dx, 3
	mov al, 2048&0xFF
	out dx, al
	add dx, 1
	mov al, (2048>>8)&0xFF
	out dx, al
	add dx, 1
	mov al, [ide_device]
	shl al, 4
	add dx, 1
	mov al, 0xA0
	out dx, al
	in al, dx
	and al, 0x01
	cmp al, 0x00
	jne .read_again
	mov dx, [ide_bar]
	call ide_wait
	mov edi, packet_command
	mov al, 0
	mov ecx, 12
	rep stosb
	mov byte [packet_command], 0x28
	mov eax, [.lba]
	mov byte [packet_command+5], al
	mov byte [packet_command+4], ah
	shr eax, 16
	mov byte [packet_command+3], al
	mov byte [packet_command+2], ah
	mov eax, [.count]
	mov byte [packet_command+8], al
	mov byte [packet_command+7], ah
	mov esi, packet_command
	mov ecx, 6
	mov dx, [ide_bar]
	.loop_write_command:
	lodsw
	out dx, ax
	loop .loop_write_command
	add dx, 4
	in al, dx
	movzx ebx, al
	add dx, 1
	in al, dx
	movzx eax, al
	shl eax, 8
	or eax, ebx
	push eax
	mov dx, [ide_bar]
	call ide_wait
	pop eax
	mov ebx, 2
	xor edx, edx
	div ebx
	mov [.word_count], eax
	mov edi, [.buffer]
	xor ecx, ecx
	.loop1:
	cmp ecx, [.count]
	jge .finish1
	push ecx
		mov ecx, [.word_count]
		mov dx, [ide_bar]
		.loop_read_data:
		in ax, dx
		stosw
		loop .loop_read_data
		call ide_wait
	pop ecx
	add ecx, 1
	jmp .loop1
	.finish1:
	popa
	mov eax, 0
	ret
	
	.error1:
	mov eax, error8
	call print_string
	popa
	mov eax, -1
	ret
	
	.lba dd 0
	.count dd 0
	.buffer dd 0
	.attempts dd 0
	.word_count dd 0

packet_command times 12 db 0

ide_wait:
	; DX = base address
	add dx, 7
	.wait1:
	in al, dx
	and al, 0x80
	cmp al, 0x00
	jne .wait1
	ret

ide_wait_drq:
	; DX = base address
	add dx, 7
	.wait1:
	in al, dx
	shr al, 3
	and al, 0x01
	cmp al, 0x00
	je .wait1
	ret

ide_primary_bar dd 0
ide_secondary_bar dd 0
ide_bar dd 0
ide_device dd 0 ;0 for Master, 1 for Slave
ide_bus dd 0
ide_slot dd 0
ide_func dd 0

success1 db "IDE found", 0
success2 db "AHCI found", 0
success3 db "ATAPI found", 0

dump:
	; EAX = data to dump
	; EBX = count
	pusha
	mov esi, eax
	mov edi, 0xB8000
	mov ecx, ebx
	.loop1:
	lodsb
	stosb
	mov al, 0x0F
	stosb
	loop .loop1
	popa
	ret

print_string:
	; EAX = string to print
	pusha
	mov esi, eax
	mov edi, 0xB8000
	mov ax, [y_position]
	movzx eax, ax
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	.loop1:
	lodsb
	cmp al, 0
	je .finish1
	stosb
	mov al, 0x0F
	stosb
	jmp .loop1
	.finish1:
	popa
	add dword [current_text_offset], 80
	add dword [current_text_offset], 80
	add dword [y_position], 1
	ret
	
pci_search_device:
	xor ecx, ecx
	.loop1:
	cmp ecx, 256
	je .finish1
	push ecx
		xor ecx, ecx
		.loop2:
		cmp ecx, 32
		je .finish2
		push ecx
			xor ecx, ecx
			.loop3:
			cmp ecx, 8
			je .finish3
			push ecx
				pop ecx
				pop ebx
				pop eax
				mov [.bus], eax
				mov [.slot], ebx
				mov [.func], ecx
				push eax
				push ebx
				push ecx
				mov edx, 0
				call pci_read_dword
				and eax, 0xFFFF
				cmp eax, 0xFFFF
				je .continue
				mov eax, [.bus]
				mov ebx, [.slot]
				mov ecx, [.func]
				mov edx, 8
				call pci_read_dword
				shr eax, 16
				mov ebx, eax
				and eax, 0xFF
				cmp eax, 0x01
				je .1
				jmp .skip_1
				.1:
				mov eax, ebx
				shr eax, 8
				cmp eax, 0x01
				je .ide_found
				.skip_1:
				mov eax, ebx
				and eax, 0xFF
				cmp eax, 0x06
				je .2
				jmp .continue
				.2:
				mov eax, ebx
				shr eax, 8
				cmp eax, 0x01
				je .ahci_found
				.continue:
			pop ecx
			add ecx, 1
			jmp .loop3
			.finish3:
		pop ecx
		add ecx, 1
		jmp .loop2
		.finish2:
	pop ecx
	add ecx, 1
	jmp .loop1
	.finish1:
	mov eax, -1
	ret
	
	.bus dd 0
	.slot dd 0
	.func dd 0
	.text1 db "Device found", 0
	
	.ide_found:
	pop ecx
	pop ebx
	pop eax
	mov [ide_bus], eax
	mov [ide_slot], ebx
	mov [ide_func], ecx
	mov eax, 1
	mov dword [controller_mode], 0
	ret
	
	.ahci_found:
	pop ecx
	pop ebx
	pop eax
	mov [ahci_bus], eax
	mov [ahci_slot], ebx
	mov [ahci_func], ecx
	mov eax, 2
	mov dword [controller_mode], 1
	ret

controller_mode dd 0; 0 = IDE, 1 = AHCI
	
pci_read_dword:
	; EAX = bus
	; EBX = slot
	; ECX = func
	; EDX = register
	mov [.offset], edx
	shl eax, 16
	shl ebx, 11
	or eax, ebx
	shl ecx, 8
	or eax, ecx
	and edx, 0xFC
	or eax, edx
	or eax, 0x80000000
	mov dx, 0xCF8
	out dx, eax
	mov dx, 0xCFC
	in eax, dx
	ret
	
	.offset dd 0

numlen:
	; EAX = number
	mov dword [.len], 0
	cmp eax, 0
	je .0
	pusha
	.loop1:
	cmp eax, 0
	jbe .finish1
	mov ebx, 10
	xor edx, edx
	div ebx
	add dword [.len], 1
	jmp .loop1
	.finish1:
	popa
	mov eax, [.len]
	ret
	
	.0:
	mov eax, 1
	ret
	
	.len dd 0

numlenhex:
	; EAX = number
	mov dword [.len], 0
	cmp eax, 0
	je .0
	pusha
	.loop1:
	cmp eax, 0
	jbe .finish1
	mov ebx, 16
	xor edx, edx
	div ebx
	add dword [.len], 1
	jmp .loop1
	.finish1:
	popa
	mov eax, [.len]
	ret
	
	.0:
	mov eax, 1
	ret
	
	.len dd 0

print_number:
	; EAX = number
	pusha
	push eax
	mov edi, 0xB8000
	mov eax, [y_position]
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	pop eax
	cmp eax, 0
	je .0
	push eax
	call numlen
	sub eax, 1
	mov ebx, 2
	mul ebx
	add edi, eax
	pop eax
	.loop1:
	cmp eax, 0
	jbe .finish1
	mov ebx, 10
	xor edx, edx
	div ebx
	add dl, '0'
	mov byte [edi], dl
	mov byte [edi+1], 0x0F
	sub edi, 2
	jmp .loop1
	.finish1:
	popa
	add dword [y_position], 1
	ret
	
	.0:
	mov byte [edi], '0'
	mov byte [edi+1], 0x0F
	popa
	add dword [y_position], 1
	ret

print_hex:
	; EAX = number
	pusha
	push eax
	mov edi, 0xB8000
	mov eax, [y_position]
	mov ebx, 80
	mul ebx
	mov ebx, 2
	mul ebx
	add edi, eax
	pop eax
	cmp eax, 0
	je .0
	push eax
	call numlenhex
	sub eax, 1
	mov ebx, 2
	mul ebx
	add edi, eax
	pop eax
	.loop1:
	cmp eax, 0
	jbe .finish1
	mov ebx, 16
	xor edx, edx
	div ebx
	movzx edx, dl
	mov esi, hex
	add esi, edx
	mov dl, [esi]
	mov byte [edi], dl
	mov byte [edi+1], 0x0F
	sub edi, 2
	jmp .loop1
	.finish1:
	popa
	add dword [y_position], 1
	ret
	
	.0:
	mov byte [edi], '0'
	mov byte [edi+1], 0x0F
	popa
	add dword [y_position], 1
	ret

print_char_32:
	; AL = character to print
	pusha
	mov edi, 0xB8000
	add edi, [current_text_offset]
	mov byte [edi], al
	mov byte [edi+1], 0x0F
	add word [current_text_offset], 2
	popa
	ret

[BITS 16]

back_to_text_mode:
	pusha
	mov ah, 0
	mov al, 3
	int 10h
	popa
	ret

dump_16:
	; AX = buffer segment to dump
	; BX = buffer offset to dump
	; CX = count
	pusha
	mov [.buffer_segment], ax
	mov [.buffer_offset], bx
	mov word [.screen_position], 0
	.loop1:
	mov bx, [.buffer_segment]
	mov es, bx
	mov di, [.buffer_offset]
	mov al, [es:di]
	add word [.buffer_offset], 1
	mov bx, 0xB800
	mov es, bx
	mov di, [.screen_position]
	mov byte [es:di], al
	mov byte [es:di+1], 0x0F
	add word [.screen_position], 2
	loop .loop1
	popa
	ret
	
	.screen_position dw 0
	.buffer_segment dw 0
	.buffer_offset dw 0

numlen_16:
	; AX = number
	mov word [.len], 0
	cmp ax, 0
	je .0
	pusha
	.loop1:
	cmp ax, 0
	jbe .finish1
	mov bx, 10
	xor dx, dx
	div bx
	add word [.len], 1
	jmp .loop1
	.finish1:
	popa
	mov ax, [.len]
	ret
	
	.0:
	mov ax, 1
	ret
	
	.len dw 0

numlenhex_16:
	; AX = number
	mov word [.len], 0
	cmp ax, 0
	je .0
	pusha
	.loop1:
	cmp ax, 0
	jbe .finish1
	mov bx, 16
	xor dx, dx
	div bx
	add word [.len], 1
	jmp .loop1
	.finish1:
	popa
	mov ax, [.len]
	ret
	
	.0:
	mov ax, 1
	ret
	
	.len dw 0

print_number_16:
	; AX = number
	pusha
	push ax
	mov bx, 0xB800
	mov es, bx
	mov di, 0x0
	mov ax, [y_position]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	add di, ax
	add di, [current_text_offset]
	pop ax
	cmp ax, 0
	je .0
	push ax
	call numlen_16
	add [current_text_offset], ax
	add [current_text_offset], ax
	sub ax, 1
	mov bx, 2
	mul bx
	add di, ax
	pop ax
	.loop1:
	cmp ax, 0
	jbe .finish1
	mov bx, 10
	xor dx, dx
	div bx
	add dl, '0'
	mov byte [es:di], dl
	mov byte [es:di+1], 0x0F
	sub di, 2
	jmp .loop1
	.finish1:
	popa
	ret
	
	.0:
	mov byte [es:di], '0'
	mov byte [es:di+1], 0x0F
	popa
	ret

print_hex_16:
	; AX = number
	pusha
	push ax
	mov bx, 0xB800
	mov es, bx
	mov di, 0x0
	mov ax, [y_position]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	add di, ax
	add di, [current_text_offset]
	pop ax
	cmp ax, 0
	je .0
	push ax
	call numlenhex_16
	add [current_text_offset], ax
	add [current_text_offset], ax
	sub ax, 1
	mov bx, 2
	mul bx
	add di, ax
	pop ax
	.loop1:
	cmp ax, 0
	jbe .finish1
	mov bx, 16
	xor dx, dx
	div bx
	movzx dx, dl
	mov si, hex
	add si, dx
	mov dl, [si]
	mov byte [es:di], dl
	mov byte [es:di+1], 0x0F
	sub di, 2
	jmp .loop1
	.finish1:
	popa
	ret
	
	.0:
	mov byte [es:di], '0'
	mov byte [es:di+1], 0x0F
	popa
	ret

hex db '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'

print_char_16:
	; AL = character to print
	pusha
	push ax
	mov bx, 0xB800
	mov es, bx
	mov di, 0x0
	mov ax, [y_position]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	add di, ax
	add di, [current_text_offset]
	pop ax
	mov byte [es:di], al
	mov byte [es:di+1], 0x0F
	add word [current_text_offset], 2
	popa
	ret
	
current_text_offset dd 0

print_string_16:
	; AX = string ptr
	pusha
	mov si, ax
	mov ax, [y_position]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	mov bx, 0xB800
	mov es, bx
	mov di, 0
	.loop:
	lodsb
	cmp al, 0
	je .finish
	stosb
	mov al, 0x0F
	stosb
	jmp .loop
	.finish:
	popa
	add word [y_position], 1
	ret

print_string_16_segment_offset:
	; AX = segment of string
	; BX = offset to string
	pusha
	mov [.string_segment], ax
	mov [.string_offset], bx
	mov word [.string_pos], 0
	mov ax, [y_position]
	mov bx, 80
	mul bx
	mov bx, 2
	mul bx
	mov [.screen_pos], ax
	.loop:
	mov bx, [.string_segment]
	mov es, bx
	mov di, [.string_offset]
	add di, [.string_pos]
	mov al, [es:di]
	add word [.string_pos], 1
	cmp al, 0
	je .finish
	mov bx, 0xB800
	mov es, bx
	mov di, [.screen_pos]
	mov [es:di], al
	mov al, 0x0F
	mov [es:di+1], al
	add word [.screen_pos], 2
	jmp .loop
	.finish:
	popa
	add word [y_position], 1
	ret
	
	.string_pos dw 0
	.string_segment dw 0
	.string_offset dw 0
	.screen_pos dw 0

y_position dd 0

VbeInfoBlock:
	.signature db "VBE2" ;VBE signature
	.version dw 0300h
	.oem_string_ptr dd 0 ;String pointer to OEM name
	.cap times 4 db 0 ;Capabilities of graphics controller
	.video_modes_ptr dd 0
	.total_memory dw 0 ;Total of 64K memory blocks
	.oem_soft_rev dw 0 ;OEM software revision number
	.oem_vendor_name_ptr dd 0 ;String pointer to OEM vendor name
	.oem_product_name_ptr dd 0 ;String pointer to OEM product name
	.oem_product_rev_ptr dd 0 ;String pointer to OEM product revision string
	.reserved_1 times 222 db 0
	.oem_data times 256 db 0 ;Data area of OEM string

ModeInfoBlock:
	.attr dw 0
	.win_a_attr db 0
	.win_b_attr db 0
	.win_gran dw 0
	.win_size dw 0
	.win_a_seg dw 0
	.win_b_seg dw 0
	.win_func_ptr dd 0
	.bytes_per_scan_line dw 0
	.width dw 0
	.height dw 0
	.x_char_size db 0
	.y_char_size db 0
	.planes db 0
	.bits_per_pixel db 0
	.num_banks db 0
	.memory_model db 0
	.bank_size db 0
	.num_img_pages db 0
	.reserved_1 db 1
	.red_mask_size db 0
	.red_field_pos db 0
	.green_mask_size db 0
	.green_field_pos db 0
	.blue_mask_size db 0
	.blue_field_pos db 0
	.reserved_mask_size db 0
	.reserved_field_pos db 0
	.direct_color_mode_info db 0
	.buff dd 0
	.reserved_2 dd 0
	.reserved_3 dw 0
	.lin_bytes_per_scan_line dw 0
	.lin_bank_num_img_pages db 0
	.lin_num_img_pages db 0
	.lin_red_mask_size db 0
	.lin_red_field_pos db 0
	.lin_green_mask_size db 0
	.lin_green_field_pos db 0
	.lin_blue_mask_size db 0
	.lin_blue_field_pos db 0
	.lin_rsv_mask_size db 0
	.lin_rsv_field_pos db 0
	.max_pixel_clock dd 0
	.reserved_4 times 189 db 0

CRTCInfoBlock:
	.horizontal_start dw 0
	.horizontal_sync_start dw 0
	.horizontal_sync_end dw 0
	.vertical_total dw 0
	.vertical_sync_start dw 0
	.vertical_sync_end dw 0
	.flags dw 0
	.pixel_clock dw 0
	.refresh_rate dw 0
	.reserved times 40 db 0
