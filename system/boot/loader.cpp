#define IDE 1
#define AHCI 2

typedef struct {
	int addr;
	short byteCount;
	short reserved:15;
	short eot:1;
} PRD;

PRD prd1 __attribute__((aligned(4)));

char* screen = (char*)0xB8000;
int y_position = 0;
char hex_value[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

int type;
int bus, slot, func;
int bar;
int ide_primary_bar;
int ide_secondary_bar;
int ide_bar;
int ide_device_num;
short identify_data[256];

void stop() {
	while (1);
}

void out8(int port, char value) {
	asm volatile("out dx, al"::"d"(port),"a"(value));
}

char mmio_read_8(int bar) {
	char* array = (char*)bar;
	return array[0];
}

void mmio_write_8(int bar, int value) {
	char* array = (char*)bar;
	array[0] = value;
}

short mmio_read_16(int bar) {
	short* array = (short*)bar;
	return array[0];
}

void mmio_write_16(int bar, int value) {
	short* array = (short*)bar;
	array[0] = value;
}

int mmio_read_32(int bar) {
	int* array = (int*)bar;
	return array[0];
}

void mmio_write_32(int bar, int value) {
	int* array = (int*)bar;
	array[0] = value;
}

char in8(int port) {
	char value;
	asm volatile("in al, dx":"=a"(value):"d"(port));
	return value;
}

void out16(int port, short value) {
	asm volatile("out dx, ax"::"d"(port),"a"(value));
}

short in16(int port) {
	short value;
	asm volatile("in ax, dx":"=a"(value):"d"(port));
	return value;
}

void out32(int port, int value) {
	asm volatile("out dx, eax"::"d"(port),"a"(value));
}

int in32(int port) {
	int value;
	asm volatile("in eax, dx":"=a"(value):"d"(port));
	return value;
}

int txt_get_offset(int x, int y) {
	return (y*80+x)*2;
}

void dump(char* data, int size) {
	int offset = txt_get_offset(0, y_position);
	for (int i=0; i<size; i++) {
		screen[offset] = data[i];
		screen[offset+1] = 0x0F;
		offset += 2;
	}
	y_position += ((size/80)+((size%80)!=0)?1:0);
}

void print(char* text) {
	if (y_position > 25) {
		return;
	}
	int i = 0;
	int j = txt_get_offset(0, y_position);
	while (text[i] != 0) {
		screen[j] = text[i];
		screen[j+1] = 0x0F;
		i++;
		j += 2;
	}
	y_position++;
}

int numlen(int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 10;
	}
	return length;
}

int numlenu(unsigned int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 10;
	}
	return length;
}

int numlenhex(int number) {
	int length = 0;
	while (number > 0) {
		length++;
		number /= 16;
	}
	return length;
}

int abs(int number) {
	if (number < 0) {
		return -number;
	}
	return number;
}

void print_number(int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int originalNumber = number;
	number = abs(number);
	int length = numlen(number);
	if (originalNumber < 0) {
		screen[offset] = '-';
		screen[offset+1] = 0x0F;
		offset += 2;
	}
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = number%10+'0';
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 10;
	}
	y_position++;
}

void print_number_unsigned(unsigned int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int length = numlenu(number);
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = number%10+'0';
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 10;
	}
	y_position++;
}

int print_hex(int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int length = numlenhex(number);
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = hex_value[number%16];
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 16;
	}
	y_position++;
}

int pci_read_int(int bus, int slot, int func, int offset) {
	int address = (bus<<16)|(slot<<11)|(func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	return in32(0xCFC);
}

void load_using_ahci() {
}

void initialize_ahci() {
}

void load_using_ide() {
	while ((in8(ide_bar+7)&0x80)!=0);
	for (int i=0; i<256; i++) {
		short data = in16(ide_bar);
		identify_data[i] = data;
	}
	bar = pci_read_int(bus, slot, func, 0x20)&0xFFFFFFFC;
	bar = 0x300000;
	print_hex(bar);
	prd1.addr = 0x200000;
	prd1.byteCount = 2048;
	prd1.eot = 1;
	mmio_write_8(bar, (1<<3));
	mmio_write_32(bar+4, &prd1);
	mmio_write_8(bar, 0);
	print_number_unsigned(&prd1);
	print_number_unsigned(mmio_read_32(bar+4));
}

void initialize_ide() {
	ide_primary_bar = pci_read_int(bus, slot, func, 0x10);
	ide_primary_bar &= 0xFFFFFFFC;
	if (ide_primary_bar == 0 || ide_primary_bar == 1) {
		ide_primary_bar = 0x1F0;
	}
	ide_secondary_bar = pci_read_int(bus, slot, func, 0x18);
	ide_secondary_bar &= 0xFFFFFFFC;
	if (ide_secondary_bar == 0 || ide_secondary_bar == 1) {
		ide_secondary_bar = 0x170;
	}
	
	// Detect IDE drive on Primary Master
	ide_bar = ide_primary_bar;
	ide_device_num = 0;
	out8(ide_bar+6, 0);
	out8(ide_bar+7, 0xEC);
	load_using_ide();
	if (in8(ide_bar+7) != 0) {
		if ((in8(ide_bar+7)&1)==0) {
			load_using_ide();
		}
	}
	// Detect IDE drive on Primary Slave
	ide_bar = ide_primary_bar;
	ide_device_num = 1;
	out8(ide_bar+6, (1<<4));
	out8(ide_bar+7, 0xA1);
	if (in8(ide_bar+7) != 0) {
		if ((in8(ide_bar+7)&1)==0) {
			load_using_ide();
		}
	}
	// Detect IDE drive on Secondary Master
	ide_bar = ide_secondary_bar;
	ide_device_num = 0;
	out8(ide_bar+6, 0);
	out8(ide_bar+7, 0xA1);
	if (in8(ide_bar+7) != 0) {
		if ((in8(ide_bar+7)&1)==0) {
			load_using_ide();
		}
	}
	// Detect IDE drive on Secondary Slave
	ide_bar = ide_secondary_bar;
	ide_device_num = 1;
	out8(ide_bar+6, (1<<4));
	out8(ide_bar+7, 0xA1);
	if (in8(ide_bar+7) != 0) {
		if ((in8(ide_bar+7)&1)==0) {
			load_using_ide();
		}
	}
}

int main(int _bus, int _slot, int _func, int _type) {
	bus = _bus;
	slot = _slot;
	func = _func;
	type = _type;
	if (type == IDE) {
		initialize_ide();
	} else if (type == AHCI) {
		initialize_ahci();
	}
}
