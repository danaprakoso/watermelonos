#include <system.h>

uint32_t pagedir[1024] __attribute__((aligned(4096)));
uint32_t pagetab1[1024] __attribute__((aligned(4096)));
uint32_t pagetab2[1024] __attribute__((aligned(4096)));
uint32_t pagetab3[1024] __attribute__((aligned(4096)));
uint32_t pagetab4[1024] __attribute__((aligned(4096)));

void init_paging() {
	memset(pagedir, 0, 4096);
	memset(pagetab1, 0, 4096);
	memset(pagetab2, 0, 4096);
	memset(pagetab3, 0, 4096);
	memset(pagetab4, 0, 4096);
	for (int i=0; i<1024; i++) {
		pagedir[i] = 0x2;
	}
	int addr = 0;
	for (int i=0; i<1024; i++) {
		pagetab1[i] = addr|3;
		addr += 4096;
	}
	for (int i=0; i<1024; i++) {
		pagetab2[i] = addr|3;
		addr += 4096;
	}
	for (int i=0; i<1024; i++) {
		pagetab3[i] = addr|3;
		addr += 4096;
	}
	for (int i=0; i<1024; i++) {
		pagetab4[i] = addr|3;
		addr += 4096;
	}
	pagedir[0] = (uint32_t)pagetab1|3;
	pagedir[1] = (uint32_t)pagetab2|3;
	pagedir[2] = (uint32_t)pagetab3|3;
	pagedir[3] = (uint32_t)pagetab4|3;
	set_pagedir((uint32_t)pagedir);
	enable_paging();
	// Test
	memset(16777215, 'A', 1);
}
