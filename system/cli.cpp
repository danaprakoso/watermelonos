#include <system.h>
#include <stdarg.h>

static char* screen = (char*)0xB8000;
int x_position = 0;
int y_position = 0;
char hex_value[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void print_number(int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int originalNumber = number;
	number = myAbs(number);
	int length = numlen(number);
	if (originalNumber < 0) {
		screen[offset] = '-';
		screen[offset+1] = 0x0F;
		offset += 2;
	}
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = number%10+'0';
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 10;
	}
	y_position++;
}

void print_number_unsigned(unsigned int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int length = numlenu(number);
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = number%10+'0';
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 10;
	}
	y_position++;
}

int print_hex(unsigned int number) {
	if (y_position > 25) {
		return;
	}
	int offset = txt_get_offset(0, y_position);
	if (number == 0) {
		screen[offset] = '0';
		screen[offset+1] = 0x0F;
		y_position++;
		return;
	}
	int length = numlenhex(number);
	offset += (length-1)*2;
	while (number > 0) {
		screen[offset] = hex_value[number%16];
		screen[offset+1] = 0x0F;
		offset -= 2;
		number /= 16;
	}
	y_position++;
}

void print(char* text) {
	if (y_position > 25) {
		return;
	}
	int i = 0;
	int j = txt_get_offset(0, y_position);
	while (text[i] != 0) {
		screen[j] = text[i];
		screen[j+1] = 0x0F;
		i++;
		j += 2;
	}
	y_position++;
}

int txt_get_offset(int x, int y) {
	return (y*80+x)*2;
}

void dump(char* data, int size) {
	int offset = txt_get_offset(0, y_position);
	for (int i=0; i<size; i++) {
		screen[offset] = data[i];
		screen[offset+1] = 0x0F;
		offset += 2;
	}
	y_position += ((size/80)+((size%80)!=0)?1:0);
}

void dn_printf(char* text, ...) {
	int offset = txt_get_offset(x_position, y_position);
	int i = 0;
	va_list args;
	va_start(args, text);
	while (text[i] != 0) {
		if (text[i] == '%') {
			if (text[i+1] == 'd') {
				int number = va_arg(args, int);
				if (number == 0) {
					screen[offset] = '0';
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
				} else {
					int length = numlen(number)-1;
					offset += (length*2);
					int j = offset;
					while (number > 0) {
						screen[j] = number%10+'0';
						screen[j+1] = 0x0F;
						j -= 2;
						number /= 10;
						x_position++;
					}
				}
			} else if (text[i+1] == 'x') {
				unsigned int number = va_arg(args, unsigned int);
				if (number == 0) {
					screen[offset] = '0';
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
				} else {
					int length = 2;
					offset += (length*2);
					int j = offset;
					while (number > 0) {
						screen[j] = hex_value[number%16];
						screen[j+1] = 0x0F;
						j -= 2;
						number /= 16;
						x_position++;
					}
					offset += 2;
				}
			} else if (text[i+1] == 'c') {
				char ch = va_arg(args, int);
				screen[offset] = ch;
				screen[offset+1] = 0x0F;
				offset += 2;
				x_position++;
			} else if (text[i+1] == 's') {
				char* str = (char*)va_arg(args, void*);
				int j = 0;
				while (str[j] != 0) {
					screen[offset] = str[j];
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
					j++;
				}
			}
			i++;
		}/* else if (text[i] == 10 || text[i] == 13) {
			y_position++;
			x_position = 0;
			offset = txt_get_offset(x_position, y_position);
		}*/ else {
			screen[offset] = text[i];
			screen[offset+1] = 0x0F;
			offset += 2;
			x_position++;
		}
		i++;
	}
	va_end(args);
}

void printf(char* text, ...) {
	int offset = txt_get_offset(x_position, y_position);
	int i = 0;
	va_list args;
	va_start(args, text);
	while (text[i] != 0) {
		if (text[i] == '%') {
			if (text[i+1] == 'd') {
				int number = va_arg(args, int);
				if (number == 0) {
					screen[offset] = '0';
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
				} else {
					int length = numlen(number)-1;
					offset += (length*2);
					int j = offset;
					while (number > 0) {
						screen[j] = number%10+'0';
						screen[j+1] = 0x0F;
						j -= 2;
						number /= 10;
						x_position++;
					}
					offset += 2;
				}
			} else if (text[i+1] == 'x') {
				unsigned int number = va_arg(args, unsigned int);
				if (number == 0) {
					screen[offset] = '0';
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
				} else {
					int length = numlenhex(number)-1;
					offset += (length*2);
					int j = offset;
					while (number > 0) {
						screen[j] = hex_value[number%16];
						screen[j+1] = 0x0F;
						j -= 2;
						number /= 16;
						x_position++;
					}
					offset += 2;
				}
			} else if (text[i+1] == 'c') {
				char ch = va_arg(args, int);
				screen[offset] = ch;
				screen[offset+1] = 0x0F;
				offset += 2;
				x_position++;
			} else if (text[i+1] == 's') {
				char* str = (char*)va_arg(args, void*);
				int j = 0;
				while (str[j] != 0) {
					screen[offset] = str[j];
					screen[offset+1] = 0x0F;
					offset += 2;
					x_position++;
					j++;
				}
			}
			i++;
		} else if (text[i] == 10 || text[i] == 13) {
			y_position++;
			x_position = 0;
			offset = txt_get_offset(x_position, y_position);
		} else {
			screen[offset] = text[i];
			screen[offset+1] = 0x0F;
			offset += 2;
			x_position++;
		}
		i++;
	}
	va_end(args);
}
