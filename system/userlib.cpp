#include <system.h>

void (*put_color_ptr)(int x, int y, int color);
int (*get_color_ptr)(int x, int y);

void set_userlib_put_color_func(void (*_put_color)(int x, int y, int color)) {
	put_color_ptr = _put_color;
}

void set_userlib_get_color_func(void (*_get_color)(int x, int y)) {
	get_color_ptr = _get_color;
}

void init_user_libc() {
	//load_elf((char*)&userlibinit);
}
