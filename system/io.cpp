#include <system.h>

void out8(int port, char value) {
	asm volatile("out dx, al"::"d"(port),"a"(value));
}

char in8(int port) {
	char value;
	asm volatile("in al, dx":"=a"(value):"d"(port));
	return value;
}

void out16(int port, short value) {
	asm volatile("out dx, ax"::"d"(port),"a"(value));
}

short in16(int port) {
	short value;
	asm volatile("in ax, dx":"=a"(value):"d"(port));
	return value;
}

void out32(int port, int value) {
	asm volatile("out dx, eax"::"d"(port),"a"(value));
}

int in32(int port) {
	int value;
	asm volatile("in eax, dx":"=a"(value):"d"(port));
	return value;
}

void mmio_write_8(int addr, char value) {
	char* address = (char*)addr;
	address[0] = value;
}

char mmio_read_8(int addr) {
	char* address = (char*)addr;
	return address[0];
}

void mmio_write_16(int addr, short value) {
	short* address = (short*)addr;
	address[0] = value;
}

short mmio_read_16(int addr) {
	short* address = (short*)addr;
	return address[0];
}

void mmio_write_32(int addr, int value) {
	int* address = (int*)addr;
	address[0] = value;
}

int mmio_read_32(int addr) {
	int* address = (int*)addr;
	return address[0];
}
