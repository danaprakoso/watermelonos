#include <system.h>

int* crcTable = NULL;

bool checkCrc() {
	if (crcTable == NULL) {
		crcTable = (int*)malloc(sizeof(int)*256);
	    for(int n=0;n<=255;n++){
    		int c = n;
    		for (int k=0;k<=7;k++) {
    	    	if((c & 1) == 1) {
    	        	c = 0xEDB88320^((c>>1)&0x7FFFFFFF);
    	        } else {
    	            c = ((c>>1)&0x7FFFFFFF);
    	        }
    	    }
    		crcTable[n] = c;
    	}
    }
    
}

//CRC (hex) = 0x104C11DB7
//CRC (binary) = 100000100110000010001110110110111
void parse_png(char* data, int dataSize) {
	if ((chartouchar(data[0])&0xFF) != 137
		|| (chartouchar(data[1])&0xFF) != 80
		|| (chartouchar(data[2])&0xFF) != 78
		|| (chartouchar(data[3])&0xFF) != 71
		|| (chartouchar(data[4])&0xFF) != 13
		|| (chartouchar(data[5])&0xFF) != 10
		|| (chartouchar(data[6])&0xFF) != 26
		|| (chartouchar(data[7])&0xFF) != 10) {
		printf("PNG signature is invalid\n");
		return;
	}
	int i = 8;
	int imageWidth = 0;
	int imageHeight = 0;
	int imageBpp = 0;
	int colorType = 0;
	int compressionMethod = 0;
	int filterMethod = 0;
	int interlaceMethod = 0;
	while (i < dataSize) {
		int length = charstouint(data[i], data[i+1], data[i+2], data[i+3]);
		i += 4;
		if (data[i] == 'I' && data[i+1] == 'H' && data[i+2] == 'D' && data[i+3] == 'R') {
			i += 4;
			imageWidth = charstouint(data[i], data[i+1], data[i+2], data[i+3]);
			if (imageWidth == 0) {
				return;
			}
			imageHeight = charstouint(data[i+4], data[i+5], data[i+6], data[i+7]);
			if (imageHeight == 0) {
				return;
			}
			imageBpp = (int)chartouchar(data[i+8])&0xFF;
			colorType = (int)chartouchar(data[i+9])&0xFF;
			if (colorType != 0 && colorType != 2 && colorType != 3 && colorType != 4 && colorType != 6) {
				return;
			}
			compressionMethod = (int)chartouchar(data[i+10]&0xFF);
			if (compressionMethod != 0) {
				return;
			}
			filterMethod = (int)chartouchar(data[i+11])&0xFF;
			if (filterMethod != 0) {
				return;
			}
			interlaceMethod = (int)chartouchar(data[i+12])&0xFF;
			if (interlaceMethod != 0 && interlaceMethod != 1) {
				return;
			}
			i += length;
			
		} else if (data[i] == 'P' && data[i+1] == 'L' && data[i+2] == 'T' && data[i+3] == 'E') {
			i += 4;
			if (colorType == 0 || colorType == 4) {
				i += length;
				i += 4;
				continue;
			}
			if ((length%3) != 0) {
				i += length;
				i += 4;
				continue;
			}
			
		}
	}
}
