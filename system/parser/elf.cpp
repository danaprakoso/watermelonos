#include <system.h>

void load_elf(char* data) {
	ELFHeader* header = (ELFHeader*)data;
	int phCount = (int)header->phnum;
	ELFProgramHeader* programHeader = (ELFProgramHeader*)(data+header->phoff);
	for (int i=0; i<phCount; i++) {
		if (programHeader->type == 0x01) {
			memcpy(programHeader->vaddr, data+programHeader->offset, programHeader->filesz);
			if (programHeader->memsz > programHeader->filesz) {
				memset(programHeader->vaddr+programHeader->filesz, 0, programHeader->memsz-programHeader->filesz);
			}
		}
		programHeader++;
	}
	void (*go)();
	go = header->entry;
	go();
}

void load_elf_2(char* data, int argc, char** argv) {
	ELFHeader* header = (ELFHeader*)data;
	int phCount = (int)header->phnum;
	ELFProgramHeader* programHeader = (ELFProgramHeader*)(data+header->phoff);
	for (int i=0; i<phCount; i++) {
		if (programHeader->type == 0x01) {
			memcpy(programHeader->vaddr, data+programHeader->offset, programHeader->filesz);
			if (programHeader->memsz > programHeader->filesz) {
				memset(programHeader->vaddr+programHeader->filesz, 0, programHeader->memsz-programHeader->filesz);
			}
		}
		programHeader++;
	}
	void (*go)(int argc, char** argv);
	go = header->entry;
	go(argc, argv);
}

void write(int type, char* text, int length) {
	printf("%s\n", text);
	stop();
}
