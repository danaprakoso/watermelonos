#include <system.h>

void saveButtonClickListener(View* view) {
	Container* container = (Container*)view->getParent();
	container->~Container();
	//free(container);
	activity_SaveTo();
}

void activity_Main(int code) {
	fill_background(0xffffffff);
	Container* container = new Container();
	container->setBackground(0xffffffff);
	container->setBorderColor(0xff000000);
	container->setPosition(10, 10);
	container->setSize(getWidth()-20, getHeight()-20);
	container->focus();
	TextView* tv = new TextView();
	tv->setText("ID Number:");
	tv->setX(10+16);
	tv->setY(10+26);
	tv->setColor(0xff000000);
	container->addChild(tv);
	TextField* idNumberField = new TextField();
	idNumberField->setX(10+16);
	idNumberField->setY(10+26+10);
	idNumberField->setWidth(getWidth()-20-32);
	idNumberField->setHeight(30);
	container->addChild(idNumberField);
	tv = new TextView();
	tv->setText("Sex:");
	tv->setX(10+16);
	tv->setY(10+26+10+30+10+20);
	tv->setColor(0xff000000);
	container->addChild(tv);
	TextField* sexField = new TextField();
	sexField->setX(10+16);
	sexField->setY(10+26+10+30+10+20+10);
	sexField->setWidth(getWidth()-20-32);
	sexField->setHeight(30);
	container->addChild(sexField);
	tv = new TextView();
	tv->setText("Status:");
	tv->setX(10+16);
	tv->setY(10+26+10+30+10+20+10+30+10+20);
	tv->setColor(0xff000000);
	container->addChild(tv);
	Container* statusGroup = new Container();
	statusGroup->addChild(new RadioView(10+16+20, 10+26+10+30+10+20+10+30+10+20+20, "Single", 0xff000000, true, 1));
	statusGroup->addChild(new RadioView(10+16+200+20, 10+26+10+30+10+20+10+30+10+20+20, "Married", 0xff000000, false, 2));
	container->addChild(statusGroup);
	container->apply();
	container->unfocus();
	flush();
	mainLoop();
}
