#include <system.h>

static Container* container;

void closeButtonListener(View* view) {
	container->~Container();
	//free(container);
	reset_screen_buffer();
	activity_Main(2);
}

void addToList(ListView* listView, int* icon, char* text) {
	if (listView->nextY == -1) {
		listView->nextY = listView->getY();
	}
	Container* container = new Container();
	container->setX(listView->getX());
	container->setY(listView->nextY);
	container->setWidth(listView->getWidth());
	container->setHeight(40);
	container->addChild(new ImageView(&file_icon, container->getX()+20, container->getY()+5, 0, 0));
	container->addChild(new TextView(text, container->getX()+60, container->getY()+40-12, 0xff000000, 0xffffffff));
	container->setBackground(0xffffffff);
	container->setSelectedBackground(0xff2196f3);
	listView->addItem(container);
	listView->nextY += 40;
}

void activity_SaveTo() {
	unsetClip();
	set_background(&wallpaper);
	fill_rect(50, 50, getWidth()-100, getHeight()-100, 0xffffffff);
	// Gambar container warna putih
	container = new Container();
	container->setBackground(0xffffffff);
	container->setPosition(50, 50);
	container->setSize(getWidth()-100, getHeight()-100);
	container->focus();
	container->setTitle("Save As...");
	container->titleColor = 0xffffffff;
	container->titleBackgroundColor = 0xff546c78;
	SideBar* folderView = new SideBar(50, 50+30, 150, getHeight()-100-30, 0xffe1e9ec);
	TreeView* folderTree = new TreeView();
	folderTree->addNode(new ImageView(&small_folder_icon, 30, 3, 20, 20), "Harddisk", 0xff464b4f, 0x00000000, 0xffffffff, 0xff26aae9);
	folderTree->addNode(new ImageView(&small_usb_icon, 30, 3, 20, 20), "Flash Disk", 0xff464b4f, 0x00000000, 0xffffffff, 0xff26aae9);
	folderView->setContent(folderTree);
	ListView* fileList = new ListView();
	fileList->setX(50+150);
	fileList->setY(50+30);
	fileList->setWidth(getWidth()-100-100);
	fileList->setHeight(getHeight()-100-30);
	addToList(fileList, &file_icon, "File 1");
	addToList(fileList, &file_icon, "File 2");
	addToList(fileList, &file_icon, "File 3");
	addToList(fileList, &file_icon, "File 4");
	addToList(fileList, &file_icon, "File 5");
	addToList(fileList, &file_icon, "File 6");
	addToList(fileList, &file_icon, "File 7");
	addToList(fileList, &file_icon, "File 8");
	addToList(fileList, &file_icon, "File 9");
	addToList(fileList, &file_icon, "File 10");
	addToList(fileList, &file_icon, "File 11");
	addToList(fileList, &file_icon, "File 12");
	addToList(fileList, &file_icon, "File 13");
	container->addChild(fileList);
	container->setCloseButton(new ImageButton(closeButtonListener));
	container->setSideBar(folderView);
	container->apply();
	container->unfocus();
	flush();
	mainLoop();
}
