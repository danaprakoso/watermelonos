[BITS 32]

global cursor_image
cursor_image:
	incbin "resources/cursor.bmp"

global checkbox_checked_image
checkbox_checked_image:
	incbin "resources/checkbox_checked.bmp"

global checkbox_unchecked_image
checkbox_unchecked_image:
	incbin "resources/checkbox_unchecked.bmp"
	
global instagram_icon
instagram_icon:
	incbin "resources/instagram.png"

global pngreader
pngreader:
	incbin "resources/pngreader"

global allocator
allocator:
	incbin "resources/allocator"

global textdrawer
textdrawer:
	incbin "resources/textdrawer"

global textwidth
textwidth:
	incbin "resources/textwidth"

global wallpaper
wallpaper:
	incbin "resources/wallpaper.png"
	
global monaco_font
monaco_font:
	incbin "resources/Monaco.ttf"

global apps_icon
apps_icon:
	incbin "resources/apps_icon.png"

global calculator_icon
calculator_icon:
	incbin "resources/calculator_icon.png"

global close_icon
close_icon:
	incbin "resources/close_icon.png"
