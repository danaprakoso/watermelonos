#include <system.h>

int serial_port = 0;

void init_serial(int port0, int port1, int port2, int port3) {
	if (port0 == 0) {
		if (port1 == 0) {
			if (port2 == 0) {
				if (port3 == 0) {
					return;
				}
				serial_port = port3;
			} else {
				serial_port = port2;
			}
		} else {
			serial_port = port1;
		}
	} else {
		serial_port = port0;
	}
	out8(serial_port+1, 0x00);
	out8(serial_port+3, 0x80);
	out8(serial_port+0, 0x03);
	out8(serial_port+1, 0x00);
	out8(serial_port+3, 0x03);
	out8(serial_port+2, 0xC7);
	out8(serial_port+4, 0x0B);
}

int serial_received() {
	return in8(serial_port + 5) & 1;
}
 
char read_serial() {
	while (serial_received() == 0);
	return in8(serial_port);
}

int is_transmit_empty() {
	return in8(serial_port + 5) & 0x20;
}

void write_serial(char a) {
	while (is_transmit_empty() == 0);
	out8(serial_port, a);
}
