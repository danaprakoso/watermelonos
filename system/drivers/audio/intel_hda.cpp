#include <system.h>

static PCIDevice* device;
static uint32_t bar;
static uint32_t corbAddr;
static int rirb_rp = 0;
static int corbszcap;
static int rirbszcap;
static int corb_bar = 0;
static int rirb_bar = 0;

static void intel_hda_handler(struct regs_t* r) {
	printf("Interrupt is firing\n");
	/*while (true) {
		int rirb_wp = mmio_read_16(bar+0x58)&0xFF;
		if (rirb_wp == 1) {
			int value1 = mmio_read_32(rirb_bar+8*rirb_wp);
			int value2 = mmio_read_32(rirb_bar+8*rirb_wp+4);
			printf("Supported PCMs: %x\n", value1);
			printf("Supported PCMs: %x\n", value2);
			rirb_rp++;
			break;
		}
	}
	stop();*/
}

typedef struct {
	unsigned int address:4;
	unsigned int index:8;
	unsigned int command:12;
	unsigned int data:8;
} Command;

void init_intel_hda() {
	device = get_audio_device();
	bar = device->bar0&0xFFFFFFF8;
	// Aktifkan Memory Space dan Bus Master
	int value = pci_read_int_2(device, 4);
	value |= (1<<1);
	value |= (1<<2);
	pci_write_int_2(device, 4, value);
	int interrupt = pci_read_int_2(device, 0x3C)&0xFF;
	irq_set_handler(interrupt, intel_hda_handler);
	// Mereset device
	value = mmio_read_32(bar+8);
	value &= 0xFFFFFFFE;
	mmio_write_32(bar+8, value);
	// Menunggu device untuk siap mereset
	while (true) {
		value = mmio_read_32(bar+8);
		if ((value&1)==0) {
			break;
		}
		sleep(1000);
	}
	sleep(1000);
	// Ketika device pertama kali dibawa, device sedang dalam state RESET.
	// Semua register tidak bisa ditulis (jika dipaksa ditulis maka tidak akan berefek apapun).
	// Satu-satunya register yg bisa ditulis adalah register CRST (Offset 8, bit 0),
	// jika bit ini ditulis sebagai 1, maka mesin akan keluar dari state RESET.
	// Mengambil mesin keluar dari state RESET
	value = mmio_read_32(bar+8);
	value |= 0x01;
	mmio_write_32(bar+8, value);
	// Menunggu hingga bit CRST dibaca sebagai 1
	while (true) {
		value = mmio_read_32(bar+8);
		if ((value&1)==1) {
			break;
		}
		sleep(1000);
	}
	sleep(1000);
	// Mengaktifkan interrupt
	value = mmio_read_32(bar+0x20);
	value |= (1<<31);
	value |= (1<<30);
	mmio_write_32(bar+0x20, value);
	// Stop CORB
	value = mmio_read_8(bar+0x4C);
	value &= ~(1<<1);
	mmio_write_8(bar+0x4C, value);
	while (true) {
		value = mmio_read_8(bar+0x4C);
		if (((value>>1)&1)==0) {
			break;
		}
	}
	printf("CORB stop complete\n");
	// Menghentikan RIRB
	value = mmio_read_8(bar+0x5C);
	value &= ~(1<<1);
	mmio_write_8(bar+0x5C, value);
	while (true) {
		value = mmio_read_8(bar+0x5C);
		if (((value>>1)&1)==0) {
			break;
		}
	}
	printf("Stopping RIRB success\n");
	// Tunggu sampai semua codec terdaftar oleh controller
	sleep(1);
	// Mendeteksi alamat-alamat codec yang melekat pada Link
	value = mmio_read_16(bar+0x0E);
	printf("Value: %d\n", value);
	value = mmio_read_32(bar+8);
	if (value == 0) {
		printf("Controller belum siap\n");
		stop();
	}
	mmio_write_8(bar+0x4E, 0x02);
	mmio_write_8(bar+0x5E, 0x02);
	// Mengkonstruksikan CORB
	corb_bar = 0x2000000;
	memset(corb_bar, 0, 1024);
	// Tes mengirim command
	// Command: GetParameter (0xA = Supported PCM rates)
	/*Command* cmd = (Command*)malloc(sizeof(Command));
	memset(cmd, 0, sizeof(Command));
	cmd->address = 0;
	cmd->index = 0x0A;
	cmd->command = 0xF00;
	memcpy(corb_bar+4, cmd, sizeof(Command));*/
	memset32(corb_bar+4, 0xF0000, 1);
	// Mengatur Base Address CORB
	mmio_write_32(bar+0x44, 0x0);
	mmio_write_32(bar+0x40, corb_bar);
	// Mengatur ukuran CORB
	corbszcap = (mmio_read_8(bar+0x4E)>>4)&0b1111;
	int total_sizes = get_total_bits(corbszcap, 1);
	/*if (total_sizes > 1) {
		int corbsize = 0;
		if (corbszcap == 1) {
			corbsize = 0;
		} else if (corbszcap == 2) {
			corbsize = 1;
		} else if (corbszcap == 4) {
			corbsize = 2;
		} else if (corbszcap == 8) {
			corbsize = 3;
		}
		value = mmio_read_8(bar+0x4E);
		value |= corbsize;
		mmio_write_8(bar+0x4E, value);
	}*/
	// Mereset CORB
	value = mmio_read_16(bar+0x4A);
	value |= (1<<15);
	mmio_write_16(bar+0x4A, value);
	while (true) {
		value = mmio_read_16(bar+0x4A);
		if (((value>>15)&1)==1) {
			break;
		}
	}
	printf("CORB reset complete\n");
	// Menghapus WP dengan menulis 0 ke registernya
	mmio_write_16(bar+0x48, 0);
	// Menkonstruksikan RIRB
	// Mengaktifkan interrupt ketika RIRB mengirim response
	/*value = mmio_read_8(bar+0x5C);
	value |= 0x01;
	mmio_write_8(bar+0x5C, value);*/
	// Mengatur base address RIRB
	rirb_bar = 0x3000000;
	memset(rirb_bar, 0, 2048);
	mmio_write_32(bar+0x54, 0x0);
	mmio_write_32(bar+0x50, rirb_bar);
	// Mengatur ukuran RIRB
	rirbszcap = (mmio_read_8(bar+0x5E)>>4)&0b1111;
	total_sizes = get_total_bits(rirbszcap, 1);
	/*if (total_sizes > 1) {
		int rirbsize = 0;
		if (rirbszcap == 1) {
			rirbsize = 0;
		} else if (rirbszcap == 2) {
			rirbsize = 1;
		} else if (rirbszcap == 4) {
			rirbsize = 2;
		} else if (rirbszcap == 8) {
			rirbsize = 3;
		}
		value = mmio_read_8(bar+0x5E);
		value |= rirbsize;
		mmio_write_8(bar+0x5E, value);
	}*/
	// Mengaktifkan CORBRUN, untuk mulai melakukan operasi
	value = mmio_read_8(bar+0x4C);
	value |= (1<<1);
	mmio_write_8(bar+0x4C, value);
	while (true) {
		value = mmio_read_8(bar+0x4C);
		if (((value>>1)&1)==1) {
			break;
		}
	}
	printf("CORBRUN set complete\n");
	value = mmio_read_8(bar+0x5C);
	value |= (1<<1);
	mmio_write_8(bar+0x5C, value);
	while (true) {
		value = mmio_read_8(bar+0x5C);
		if (((value>>1)&1)==1) {
			break;
		}
	}
	printf("RIRBBRUN set complete\n");
	// Update WP ke 1
	mmio_write_16(bar+0x48, 1);
	/*// Menunggu hingga command selesai diproses
	while (true) {
		value = mmio_read_16(bar+0x4A)&0xFF;
		if (value == 1) {
			break;
		}
	}
	printf("Sending command success\n");
	// Membaca response dari RIRB
	while (true) {
		int rirb_wp = mmio_read_16(bar+0x58)&0xFF;
		if (rirb_wp == 1) {
			sleep(5000);
			int value1 = mmio_read_32(rirb_bar+8*rirb_wp);
			int value2 = mmio_read_32(rirb_bar+8*rirb_wp+4);
			printf("Supported PCMs: %x\n", value1);
			printf("Supported PCMs: %x\n", value2);
			rirb_rp++;
			break;
		}
	}*/
}
