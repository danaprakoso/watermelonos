#include <system.h>

PCIDevice* skylakeDevice;
static int bar2d; //Base Address used for 2D rendering
static int gttStart; //Start of Graphics Translation Table (GTT) = BAR+8MB
static int bar3d; //Base Address used for 3D rendering
static int ioBar; //Base Address used for I/O
static int gen; //Generation

void intel_skylake_change_mode() {
	dn_printf("Changing Intel Skylake display mode\n");
	dn_printf("Disable display\n");
	//out8(0x3C4, 0x01);
	//out8(0x3C5, in8(0x3C5)|(1<<5));
	sleep(1);
	int reg;
	if (gen >= INTEL_GEN_5) {
		reg = INTEL_VGACNTRL;
	} else {
		return;
	}
	// Disable VGA
	//mmio_write_32(bar2d+reg, (1<<31));
	// Disable display A
	int value = mmio_read_32(bar2d+INTEL_DSPACNTR);
	value &= ~(1<<31);
	//mmio_write_32(bar2d+INTEL_DSPACNTR, value);
	// Set Pipe A screen resolution
	int maximumWidth = mmio_read_32(bar2d+INTEL_HTOTAL_A);
	dn_printf("Maximum width: %x\n", maximumWidth);
	int maximumHeight = mmio_read_32(bar2d+INTEL_VTOTAL_A);
	dn_printf("Maximum width: %x\n", maximumHeight);
}

void init_intel_skylake() {
	// Reset Intel Skylake by writing 1 to GDRST register (0x941c)
	skylakeDevice = get_pci_device(0x03, 0x00);
	if (skylakeDevice == 0) {
		print("Intel Skylake device not found.");
		return;
	} else {
		print("Intel Skylake device found.");
		bar2d = skylakeDevice->bar0&0xFFFFFFF0;
		bar3d = skylakeDevice->bar2&0xFFFFFFF0;
		ioBar = skylakeDevice->bar4&0xFFFFFFF0;
		gttStart = bar2d+0x800000;
	}
	dn_printf("Resetting Intel Skylake graphics...\n");
	mmio_write_32(bar2d+INTEL_SKYLAKE_GDRST, 1);
	while ((mmio_read_32(bar2d+INTEL_SKYLAKE_GDRST)&1)!=0);
	dn_printf("Skylake reset complete\n");
	gen = INTEL_GEN_6;
	intel_skylake_change_mode();
}
