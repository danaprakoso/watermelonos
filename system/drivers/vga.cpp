#include <system.h>

void (*put)(unsigned x, unsigned y, unsigned color);

static void write_pixel8(unsigned x, unsigned y, unsigned c) {
	unsigned wd_in_bytes;
	unsigned off;
	wd_in_bytes = 320;
	off = wd_in_bytes * y + x;
}

unsigned char g_320x200x256[] = {
/* MISC */
	0x63,
/* SEQ */
	0x03, 0x01, 0x0F, 0x00, 0x0E,
/* CRTC */
	0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
	0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x9C, 0x0E, 0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3,
	0xFF,
/* GC */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
	0xFF,
/* AC */
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
	0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
	0x41, 0x00, 0x0F, 0x00,	0x00
};

static void use_vga_mode(unsigned char *regs)
{
	unsigned i;

/* write MISCELLANEOUS reg */
	out8(VGA_MISC_WRITE, *regs);
	regs++;
/* write SEQUENCER regs */
	for(i = 0; i < VGA_NUM_SEQ_REGS; i++)
	{
		out8(VGA_SEQ_INDEX, i);
		out8(VGA_SEQ_DATA, *regs);
		regs++;
	}
/* unlock CRTC registers */
	out8(VGA_CRTC_INDEX, 0x03);
	out8(VGA_CRTC_DATA, in8(VGA_CRTC_DATA) | 0x80);
	out8(VGA_CRTC_INDEX, 0x11);
	out8(VGA_CRTC_DATA, in8(VGA_CRTC_DATA) & ~0x80);
/* make sure they remain unlocked */
	regs[0x03] |= 0x80;
	regs[0x11] &= ~0x80;
/* write CRTC regs */
	for(i = 0; i < VGA_NUM_CRTC_REGS; i++)
	{
		out8(VGA_CRTC_INDEX, i);
		out8(VGA_CRTC_DATA, *regs);
		regs++;
	}
/* write GRAPHICS CONTROLLER regs */
	for(i = 0; i < VGA_NUM_GC_REGS; i++)
	{
		out8(VGA_GC_INDEX, i);
		out8(VGA_GC_DATA, *regs);
		regs++;
	}
/* write ATTRIBUTE CONTROLLER regs */
	for(i = 0; i < VGA_NUM_AC_REGS; i++)
	{
		(void)in8(VGA_INSTAT_READ);
		out8(VGA_AC_INDEX, i);
		out8(VGA_AC_WRITE, *regs);
		regs++;
	}
/* lock 16-color palette and unblank display */
	(void)in8(VGA_INSTAT_READ);
	out8(VGA_AC_INDEX, 0x20);
}

void vga_set_mode(int width, int height, int colors) {
	unsigned char* regs = NULL;
	if (width == 320 && height == 200 && colors == 256) {
		regs = g_320x200x256;
	}
	if (regs != NULL) {
		use_vga_mode(regs);
		put = write_pixel8;
		for (int i=0; i<100; i++) {
			memset(0xA0000+i, 0xFF, 1);
		}
	} else {
		printf("Display mode not found\n");
	}
}
