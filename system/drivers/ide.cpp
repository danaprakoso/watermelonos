#include <system.h>

PCIDevice* ide_device;
int ide_bar;
int ide_device_num;
char ide_identify_data[512];
static int primary_bar;
static int secondary_bar;
static int supported_filesystems[] = {
	0x0B, 0x0C, 0x1B, 0x1C
};

char* error1 = "Cannot read IDE sector while sending command PACKET";
char* error2 = "Cannot find ATAPI device on IDE controller";

static void get_identify_data() {
	while ((in8(ide_bar+7)&0x80)!=0);
	int j = 0;
	for (int i=0; i<256; i++) {
		short value = in16(ide_bar);
		char value1 = value&0xFF;
		char value2 = (value>>8)&0xFF;
		ide_identify_data[j] = value2;
		ide_identify_data[j+1] = value1;
		j += 2;
	}
}

void ide_wait() {
	while ((in8(ide_bar+7)&0x80)!=0);
}

void ide_wait_drq() {
	while ((in8(ide_bar+7)&0x8)==0);
}

void ide_read_sector_dvd(Storage* storage, int lba, int count, char* buffer) {
	int total_retry = -1;
	retry:;
	total_retry++;
	if (total_retry >= 65536) {
		print(error1);
		return;
	}
	ide_bar = storage->getBaseAddress(storage);
	out8(storage->getBaseAddress(storage)+1, 0);
	out8(storage->getBaseAddress(storage)+4, 2048&0xFF);
	out8(storage->getBaseAddress(storage)+5, (2048>>8)&0xFF);
	out8(storage->getBaseAddress(storage)+6, (ide_device_num<<4));
	out8(storage->getBaseAddress(storage)+7, 0xA0);
	if ((in8(storage->getBaseAddress(storage)+7)&1)!=0) {
		goto retry;
	}
	ide_wait();
	int byte_count = (int)in8(storage->getBaseAddress(storage)+4)|((int)in8(storage->getBaseAddress(storage)+5)<<8);
	int word_count = byte_count/2;
	char packet_command[12];
	memset(packet_command, 0, 12);
	packet_command[0] = 0x28;
	packet_command[5] = lba&0xFF;
	packet_command[4] = lba>>8&0xFF;
	packet_command[3] = lba>>16&0xFF;
	packet_command[2] = lba>>24;
	packet_command[8] = count&0xFF;
	packet_command[7] = (count>>8)&0xFF;
	int j = 0;
	for (int i=0; i<6; i++) {
		short command = ((short)packet_command[j])|((short)packet_command[j+1]<<8);
		out16(storage->getBaseAddress(storage), command);
		j += 2;
	}
	ide_wait();
	int k = 0;
	for (int i=0; i<count; i++) {
		for (j=0; j<word_count; j++) {
			short value = in16(storage->getBaseAddress(storage));
			buffer[k] = value&0xFF;
			buffer[k+1] = (value>>8)&0xFF;
			k += 2;
		}
		ide_wait();
	}
}

void ide_read_sector_harddisk(Storage* storage, int lba, int count, char* buffer) {
	int total_retry = -1;
	retry:;
	total_retry++;
	if (total_retry >= 65536) {
		print(error1);
		return;
	}
	ide_bar = storage->getBaseAddress(storage);
	out8(storage->getBaseAddress(storage)+2, count&0xFF);
	out8(storage->getBaseAddress(storage)+3, (lba&0xFF));
	out8(storage->getBaseAddress(storage)+4, (lba>>8)&0xFF);
	out8(storage->getBaseAddress(storage)+5, (lba>>16)&0xFF);
	out8(storage->getBaseAddress(storage)+6, (storage->getDeviceNum(storage)<<4)|(1<<6));
	out8(storage->getBaseAddress(storage)+7, 0x20);
	if ((in8(storage->getBaseAddress(storage)+7)&1)!=0) {
		goto retry;
	}
	ide_wait();
	int j = 0;
	for (int k=0; k<count; k++) {
		for (int i=0; i<256; i++) {
			short data = in16(storage->getBaseAddress(storage));
			buffer[j+1] = (data>>8)&0xFF;
			buffer[j] = data&0xFF;
			j += 2;
		}
		if (count != 1) {
			ide_wait();
		}
	}
}

void ide_read_sector(Storage* storage, int lba, int count, char* buffer) {
	if (storage->type == HARDDISK) {
		ide_read_sector_harddisk(storage, lba, count, buffer);
	} else if (storage->type == CDROM) {
		//ide_read_sector_dvd(storage, lba, count, buffer);
	}
}

void ide_write_sector(Storage* storage, int lba, int count, char* buffer) {
}

void init_ide_device() {
	primary_bar = ide_device->bar0&0xFFFFFFFC;
	if (primary_bar == 0 || primary_bar == 1) {
		primary_bar = 0x1F0;
	}
	secondary_bar = ide_device->bar2&0xFFFFFFFC;
	if (secondary_bar == 0 || secondary_bar == 1) {
		secondary_bar = 0x170;
	}
	
	// Find ATAPI device
	bool atapi_found = false;
	int bar = primary_bar;
	ide_bar = bar;
	ide_device_num = 0;
	out8(bar+6, 0);
	out8(bar+7, 0xA1);
	if ((in8(bar+7)&1)==0) {
		atapi_found = true;
		get_identify_data();
		return;
	}
	
	ide_bar = bar;
	ide_device_num = 1;
	out8(bar+6, (1<<4));
	out8(bar+7, 0xA1);
	if ((in8(bar+7)&1)==0) {
		atapi_found = true;
		get_identify_data();
		return;
	}
	
	bar = secondary_bar;
	ide_bar = bar;
	ide_device_num = 0;
	out8(bar+6, 0);
	out8(bar+7, 0xA1);
	if ((in8(bar+7)&1)==0) {
		atapi_found = true;
		get_identify_data();
		return;
	}
	
	ide_bar = bar;
	ide_device_num = 1;
	out8(bar+6, (1<<4));
	out8(bar+7, 0xA1);
	if ((in8(bar+7)&1)==0) {
		atapi_found = true;
		get_identify_data();
		return;
	}
	
	print(error2);
}

void ide_wait_bsy(int bar) {
	while ((in8(bar+7)&0x80)!=0) {}
}

bool is_fs_supported(int systemId) {
	for (int i=0; i<sizeof(supported_filesystems)/sizeof(int); i++) {
		if (systemId == supported_filesystems[i]) {
			return true;
		}
	}
	return false;
}

void ide_collect_ext_partitions(Storage* storage, int relativeLba) {
	char vbr[512];
	ide_read_sector(storage, relativeLba, 1, vbr);
	int offset = 446;
	for (int i=0; i<4; i++) {
		int lba = charstouint(vbr[offset+11], vbr[offset+10], vbr[offset+9], vbr[offset+8])+relativeLba;
		int systemId = chartouchar(vbr[offset+4]);
		if (systemId == 0) {
			continue;
		}
		if (systemId == 0x05 || systemId == 0x0F) {
			ide_collect_ext_partitions(storage, lba);
		} else {
			if (is_fs_supported(systemId)) {
				char bpb[512];
				ide_read_sector(storage, lba, 1, bpb);
				Partition* p = (Partition*)malloc(sizeof(Partition));
				p->label = get_next_fs_label();
				p->startingSector = lba;
				p->fs = systemId;
				p->bytesPerSector = charstoushort(bpb[12], bpb[11]);
				p->totalSectors = charstoushort(bpb[20], bpb[19]);
				if (p->totalSectors == 0) {
					p->totalSectors = charstouint(bpb[35], bpb[34], bpb[33], bpb[32]);
				}
				add_partition(storage, p);
			}
		}
		offset += 16;
	}
}

void ide_collect_partitions(Storage* storage) {
	// Read MBR
	char mbr[512];
	ide_read_sector(storage, 0, 1, mbr);
	int offset = 446;
	for (int i=0; i<4; i++) {
		int lba = charstouint(mbr[offset+11], mbr[offset+10], mbr[offset+9], mbr[offset+8]);
		int systemId = chartouchar(mbr[offset+4]);
		if (systemId == 0) {
			continue;
		}
		if (systemId == 0x05 || systemId == 0x0F) {
			ide_collect_ext_partitions(storage, lba);
		} else {
			if (is_fs_supported(systemId)) {
				char bpb[512];
				ide_read_sector(storage, lba, 1, bpb);
				Partition* p = (Partition*)malloc(sizeof(Partition));
				p->label = get_next_fs_label();
				p->startingSector = lba;
				p->fs = systemId;
				p->bytesPerSector = charstoushort(bpb[12], bpb[11]);
				p->totalSectors = charstoushort(bpb[20], bpb[19]);
				if (p->totalSectors == 0) {
					p->totalSectors = charstouint(bpb[35], bpb[34], bpb[33], bpb[32]);
				}
				add_partition(storage, p);
			}
		}
		offset += 16;
	}
}

bool is_drive_exists(int bar, int device) {
	int status = 0;
	out8(bar+3, 0x88);
	status = in8(bar+3)&0xFF;
	if (status != 0x88) {
		return false;
	}
	out8(bar+6, device<<4);
	status = in8(bar+7)&0xFF;
	if (status == 0xFF) {
		return false;
	}
	if ((status&0x40) != 0) {
		return true;
	}
	return false;
}

bool is_dvd(int bar, int device) {
	out8(bar+6, device<<4);
	out8(bar+7, 0xEC);
	int status = in8(bar+7)&0xFF;
	if ((status&1) != 0) {
		return true;
	}
	ide_wait_bsy(bar);
	for (int i=0; i<256; i++) {
		in16(bar);
	}
	return false;
}

void init_ide_device(int bar, int deviceNum) {
	Storage* storage = create_storage();
	if (is_dvd(bar, deviceNum)) {
		storage->setStorageType(storage, CDROM);
	} else {
		storage->setStorageType(storage, HARDDISK);
	}
	storage->setBaseAddress(storage, ide_bar);
	storage->setDeviceNum(storage, deviceNum);
	storage->interface = IDE;
	add_storage(storage);
	ide_collect_partitions(storage);
}

void ide_collect_storages() {
	int bar = ide_device->bar0; //primary bar
	if (bar == 0 || bar == 1) {
		bar = 0x1F0;
	}
	// Check if controller 0 device 0 exists
	if (is_drive_exists(bar, 0)) {
		init_ide_device(bar, 0);
	}
	// Check if controller 0 device 1 exists
	if (is_drive_exists(bar, 1)) {
		init_ide_device(bar, 1);
	}
	bar = ide_device->bar2; //secondary bar
	if (bar == 0 || bar == 1) {
		bar = 0x170;
	}
	// Check if controller 1 device 0 exists
	if (is_drive_exists(bar, 0)) {
		init_ide_device(bar, 0);
	}
	// Check if controller 1 device 1 exists
	if (is_drive_exists(bar, 1)) {
		init_ide_device(bar, 1);
	}
}

