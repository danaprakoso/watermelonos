#include <system.h>

static AudioDriver* audioDriver;
static PCIDevice* audioDevice;
bool audioDriverInitialized = false;
static int deviceId;

void init_audio_driver() {
	audioDriver = (AudioDriver*)malloc(sizeof(AudioDriver));
	audioDevice = get_pci_device(0x4, 0x3);
	if (audioDevice == NULL) {
		printf("There is no such device");
		return;
	}
	if (audioDevice->vendorID == 0x8086 && audioDevice->deviceID == 0x2668) {
		audioDriver->type = INTEL_HD_AUDIO;
		audioDriver->init = init_intel_hda;
	}
	if (audioDriver->init != 0) {
		audioDriver->init();
	}
}

void audio_driver_set_init_method(void (*init)()) {
	audioDriver->init = init;
}

void audio_driver_set_device_id(int _deviceId) {
	deviceId = _deviceId;
}

void enable_audio_driver() {
	int status = audioDriver->init();
	if (status == DRV_INIT_OK) {
		//Driver initialized successfully
		audioDriverInitialized = true;
	}
}

PCIDevice* get_audio_device() {
	return audioDevice;
}

AudioDriver* get_audio_driver() {
	return audioDriver;
}
