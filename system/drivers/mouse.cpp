#include <system.h>

char mouse_cycle = 0;
char mouse_byte[3];
int mouse_x = 0;
int mouse_y = 0;
int mouse_timeout = 0;
char left_click;
char right_click;
char middle_click;
static char* saved_screen_area = NULL;
static int saved_screen_area_x = 0;
static int saved_screen_area_y = 0;
int last_cursor_x_position = -1;
int last_cursor_y_position = -1;

void mouse_handler(Register* regs) {
	//char status = in8(0x64);
	/*if ((status&1)==0) {
		return;
	}
	if ((status&0x20)==0) {
		return;
	}*/
	if (mouse_cycle == 0) {
        mouse_byte[0]=in8(0x60);
      	left_click = mouse_byte[0]&0x1;
      	right_click = mouse_byte[0]&0x2;
      	middle_click = mouse_byte[0]&0x4;
      	mouse_cycle = 1;
    } else if (mouse_cycle == 1) {
        mouse_byte[1]=in8(0x60);
      	mouse_cycle = 2;
    } else if (mouse_cycle == 2) {
      	mouse_byte[2]=in8(0x60);
      	mouse_x+=mouse_byte[1];
      	mouse_y-=mouse_byte[2];
        mouse_cycle=0;
  	}
}

void mouse_wait(char a_type) {
  	unsigned int _time_out=100000;
  	if(a_type==0) {
    	while(_time_out--) {
      		if((in8(0x64) & 1)==1) {
        		return;
      		}
    	}
    	return;
  	} else {
    	while(_time_out--) {
      		if ((in8(0x64) & 2)==0) {
        		return;
      		}
    	}
    	return;
  	}
}

void mouse_write(char a_write) {
  	mouse_wait(1);
  	out8(0x64, 0xD4);
  	mouse_wait(1);
  	out8(0x60, a_write);
}

bool is_left_clicked() {
	int mouse_x = get_cursor_x();
	int mouse_y = get_cursor_y();
	if (mouse_x != last_cursor_x_position || mouse_y != last_cursor_y_position) {
		mouse_timeout = 0;
	}
	if (mouse_timeout == 0) {
		mouse_timeout = 100;
		if (left_click) {
			return true;
		}
	}
	return false;
}

bool is_right_clicked() {
	if (right_click) {
		right_click = 0;
		return true;
	}
	return false;
}

bool is_middle_clicked() {
	if (middle_click) {
		middle_click = 0;
		return true;
	}
	return false;
}

char mouse_read() {
  	mouse_wait(0);
  	return in8(0x60);
}

void init_mouse() {
  	char _status;
  	mouse_wait(1);
  	out8(0x64, 0xA8);
  	mouse_wait(1);
  	out8(0x64, 0x20);
  	mouse_wait(0);
  	_status=(in8(0x60) | 2);
  	mouse_wait(1);
  	out8(0x64, 0x60);
  	mouse_wait(1);
  	out8(0x60, _status);
  	mouse_write(0xF6);
  	mouse_read();
  	mouse_write(0xF4);
  	mouse_read();
  	irq_set_handler(12, mouse_handler);
}

int get_cursor_x() {
	if (mouse_x < 0) {
		mouse_x = 0;
	}
	if (mouse_x >= getWidth()) {
		mouse_x = getWidth()-1;
	}
	return mouse_x;
}

int get_cursor_y() {
	if (mouse_y < 0) {
		mouse_y = 0;
	}
	if (mouse_y >= getHeight()) {
		mouse_y = getHeight()-1;
	}
	return mouse_y;
}

void draw_cursor_only() {
	int x = get_cursor_x();
	int y = get_cursor_y();
	draw_image(&cursor_image, x, y, 0);
}

void draw_cursor() {
	int x = get_cursor_x();
	int y = get_cursor_y();
	if (last_cursor_x_position == -1) {
		// First time drawing mouse
		last_cursor_x_position = 0;
		last_cursor_y_position = 0;
		saved_screen_area = save_screen_area(saved_screen_area_x, saved_screen_area_y, 14, 20);
		unsetClip();
		use_primary_buffer();
		draw_image(&cursor_image, x, y, 0);
		use_secondary_buffer();
		resetClip();
	} else {
		if (last_cursor_x_position != x || last_cursor_y_position != y) {
			if (saved_screen_area != NULL) {
				restore_screen_area(saved_screen_area_x, saved_screen_area_y, 14, 20, saved_screen_area);
			}
			saved_screen_area_x = x;
			saved_screen_area_y = y;
			saved_screen_area = save_screen_area(saved_screen_area_x, saved_screen_area_y, 14, 20);
			last_cursor_x_position = x;
			last_cursor_y_position = y;
			unsetClip();
			use_primary_buffer();
			draw_image(&cursor_image, x, y, 0);
			use_secondary_buffer();
			resetClip();
		}
	}
}

void draw_cursor_forcely() {
	int x = get_cursor_x();
	int y = get_cursor_y();
	unsetClip();
	char* prevBuffer = getCurrentScreen();
	use_primary_buffer();
	draw_image(&cursor_image, x, y, 0);
	use_secondary_buffer();
	resetClip();
}

/* position = 0 for left, 1 for right, 2 for middle */
bool is_clicked(int position, int x, int y, int width, int height) {
	int mouse_x = get_cursor_x();
	int mouse_y = get_cursor_y();
	if (position == 0) {
		if (is_left_clicked()) {
			if (mouse_x >= x && mouse_x <= x+width && mouse_y >= y && mouse_y <= y+height) {
				return true;
			}
		}
		return false;
	} else if (position == 1) {
		if (is_right_clicked()) {
			if (mouse_x >= x && mouse_x <= x+width && mouse_y >= y && mouse_y <= y+height) {
				return true;
			}
		}
		return false;
	} else if (position == 2) {
		if (is_middle_clicked()) {
			if (mouse_x >= x && mouse_x <= x+width && mouse_y >= y && mouse_y <= y+height) {
				return true;
			}
		}
		return false;
	}
	return false;
}
