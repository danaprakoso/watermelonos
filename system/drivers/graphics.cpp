#include <system.h>

static int clipX, clipY, clipWidth, clipHeight;
static bool clipSet = false;
static int screenWidth = 0;
static int screenHeight = 0;
static int Bpp = 0;
static int bpp = 0;
static int decoded_width, decoded_height, decoded_Bpp, decoded_bpp;
static int original_Bpp = 0;
static int original_bpp = 0;
static int bytesPerRow = 0;
static int originalBytesPerRow = 0;
static char* screen = 0;
static char* offscreen = 0;
static char* currentScreen = 0;
static bool allow_draw_over_screen = false;
static int chosenGraphicsHardware;
static char* currentFontData;
static int currentFontDataSize;
VesaInfo* vesaInfo;

void init_graphics(VesaInfo* _vesaInfo) {
	vesaInfo = _vesaInfo;
	screenWidth = (int)vesaInfo->x_res;
	screenHeight = (int)vesaInfo->y_res;
	bpp = (int)vesaInfo->bits_per_pixel;
	Bpp = bpp/8;
	original_Bpp = Bpp;
	original_bpp = bpp;
	screen = vesaInfo->videoBuffer;
	offscreen = (char*)malloc(screenWidth*screenHeight*Bpp+32768);
	memset(offscreen, 0, screenWidth*screenHeight*Bpp+32768);
	currentScreen = offscreen;
	bytesPerRow = (int)vesaInfo->lin_bytes_per_scanline;
	originalBytesPerRow = bytesPerRow;
}

char* get_screen() {
	return offscreen;
}

int getWidth() {
	return screenWidth;
}

int getHeight() {
	return screenHeight;
}

int get_offset(int x, int y) {
	return (y*bytesPerRow)+(x*Bpp);
}

int get_color(int x, int y) {
	if (clipSet) {
		if (x < clipX || x > (clipX+clipWidth) || y < clipY || y > (clipY+clipHeight)) {
			return 0;
		}
	}
	if (x < 0 || x > screenWidth || y < 0 || y > screenHeight) {
		return 0;
	}
	int offset = get_offset(x, y);
	switch (bpp) {
		case 24: {
			int red = (int)currentScreen[offset+2]&0xFF;
			int green = (int)currentScreen[offset+1]&0xFF;
			int blue = (int)currentScreen[offset]&0xFF;
			return 0xFF000000|(red<<16)|(green<<8)|blue;
		}
		case 32: {
			int red = (int)currentScreen[offset+2]&0xFF;
			int green = (int)currentScreen[offset+1]&0xFF;
			int blue = (int)currentScreen[offset]&0xFF;
			return 0xFF000000|(red<<16)|(green<<8)|blue;
		}
	}
	return 0;
}

void put_color(int x, int y, int color) {
	if (clipSet) {
		if (x < clipX || x > (clipX+clipWidth) || y < clipY || y > (clipY+clipHeight)) {
			return;
		}
	}
	if (!allow_draw_over_screen) {
		if (x < 0 || x > screenWidth || y < 0 || y > screenHeight) {
			return;
		}
	}
	int offset = get_offset(x, y);
	switch (bpp) {
		case 24: {
			int alphaInt = (color>>24)&0xFF;
			if (alphaInt == 0xFF) {
				currentScreen[offset] = color&0xFF;
				currentScreen[offset+1] = (color>>8)&0xFF;
				currentScreen[offset+2] = (color>>16)&0xFF;
			} else if (alphaInt > 0) {
				float alpha = ((float)alphaInt)/255;
				int foregroundRed = (color>>16)&0xFF;
				int foregroundGreen = (color>>8)&0xFF;
				int foregroundBlue = color&0xFF;
				int backgroundColor = get_color(x, y);
				int backgroundRed = (backgroundColor>>16)&0xFF;
				int backgroundGreen = (backgroundColor>>8)&0xFF;
				int backgroundBlue = backgroundColor&0xFF;
				int outputRed = (foregroundRed*alpha)+(backgroundRed*(1.0-alpha));
				int outputGreen = (foregroundGreen*alpha)+(backgroundGreen*(1.0-alpha));
				int outputBlue = (foregroundBlue*alpha)+(backgroundBlue*(1.0-alpha));
				currentScreen[offset] = outputBlue;
				currentScreen[offset+1] = outputGreen;
				currentScreen[offset+2] = outputRed;
			}
			break;
		}
		case 32: {
			int alphaInt = (color>>24)&0xFF;
			if (alphaInt == 0xFF) {
				currentScreen[offset] = color&0xFF;
				currentScreen[offset+1] = (color>>8)&0xFF;
				currentScreen[offset+2] = (color>>16)&0xFF;
			} else if (alphaInt > 0) {
				float alpha = ((float)alphaInt)/255;
				int foregroundRed = (color>>16)&0xFF;
				int foregroundGreen = (color>>8)&0xFF;
				int foregroundBlue = color&0xFF;
				int backgroundColor = get_color(x, y);
				int backgroundRed = (backgroundColor>>16)&0xFF;
				int backgroundGreen = (backgroundColor>>8)&0xFF;
				int backgroundBlue = backgroundColor&0xFF;
				int outputRed = (foregroundRed*alpha)+(backgroundRed*(1.0-alpha));
				int outputGreen = (foregroundGreen*alpha)+(backgroundGreen*(1.0-alpha));
				int outputBlue = (foregroundBlue*alpha)+(backgroundBlue*(1.0-alpha));
				currentScreen[offset] = outputBlue;
				currentScreen[offset+1] = outputGreen;
				currentScreen[offset+2] = outputRed;
			}
			break;
		}
	}
}

extern "C" void draw_png_image(char* data, int x, int y, int imageSize) {
	char* argv[4];
	argv[0] = (char*)data;
	argv[1] = x;
	argv[2] = y;
	argv[3] = imageSize;
	load_elf_2((char*)&pngreader, 4, (char**)argv);
}

int get_user_pixel(int x, int y, int Bpp, int bpp, int width, int bytesPerRow, char* buffer) {
	int offset = (y*bytesPerRow)+(x*Bpp);
	switch (bpp) {
		case 24: {
			int red = buffer[offset+2];
			int green = buffer[offset+1];
			int blue = buffer[offset];
			return 0xFF000000|(red<<16)|(green<<8)|blue;
		}
		case 32: {
			int red = buffer[offset+2];
			int green = buffer[offset+1];
			int blue = buffer[offset];
			return 0xFF000000|(red<<16)|(green<<8)|blue;
		}
	}
	return 0;
}

void put_user_pixel(int x, int y, int color, int Bpp, int bpp, int width, int bytesPerRow, char* buffer) {
	int offset = (y*width+x)*Bpp;
				buffer[offset] = color&0xFF;
				buffer[offset+1] = (color>>8)&0xFF;
				buffer[offset+2] = (color>>16)&0xFF;
}

void flush() {
	int copyLength = getWidth()*getHeight()*Bpp;
	for (int i=0; i<copyLength; i++) {
		screen[i] = currentScreen[i];
	}
}

char* getCurrentScreen() {
	return currentScreen;
}

void draw_line(int x0, int y0, int x1, int y1, int color) {
	int dx = myAbs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = myAbs(y1-y0), sy = y0<y1 ? 1 : -1; 
	int err = (dx>dy ? dx : -dy)/2, e2;
	for(;;){
		put_color(x0, y0, color);
		if (x0==x1 && y0==y1) break;
		e2 = err;
		if (e2 >-dx) { err -= dy; x0 += sx; }
		if (e2 < dy) { err += dx; y0 += sy; }
	}
}

void fill_rect(int x, int y, int width, int height, int color) {
	for (int i=0; i<height; i++) {
		draw_line(x, y+i, x+width, y+i, color);
	}
}

void draw_rect(int x, int y, int width, int height, int color) {
	draw_line(x, y, x+width, y, color);
	draw_line(x, y, x, y+width, color);
	draw_line(x+width, y, x+width, y+height, color);
	draw_line(x, y+height, x+width, y+height, color);
}

static void parse_bmp_image_info(int* _bmp_data) {
	char* bmp_data = (char*)_bmp_data;
	int width = charstouint(bmp_data[21], bmp_data[20], bmp_data[19], bmp_data[18]);
	int height = charstouint(bmp_data[25], bmp_data[24], bmp_data[23], bmp_data[22]);
	decoded_width = width;
	decoded_height = height;
}

static void draw_bmp_image(char* bmp_data, int x, int y) {
	int width = charstouint(bmp_data[21], bmp_data[20], bmp_data[19], bmp_data[18]);
	int height = charstouint(bmp_data[25], bmp_data[24], bmp_data[23], bmp_data[22]);
	int bmp_bpp = charstoushort(bmp_data[29], bmp_data[28]);
	int offset = charstouint(bmp_data[13], bmp_data[12], bmp_data[11], bmp_data[10]);
	int imageSize = charstouint(bmp_data[37], bmp_data[36], bmp_data[35], bmp_data[34]);
	offset += (imageSize-(bmp_bpp/8));
	if (bmp_bpp == 24) {
		for (int j=0; j<height; j++) {
			int rowBytes = 0;
			for (int i=width-1; i>=0; i--) {
				int blue = chartouchar(bmp_data[offset])&0xFF;
				int green = chartouchar(bmp_data[offset+1])&0xFF;
				int red = chartouchar(bmp_data[offset+2])&0xFF;
				int color = (red<<16)|(green<<8)|blue;
				put_color(x+i, y+j, 0xff000000|color);
				offset -= 3;
				rowBytes += 3;
			}
			if ((rowBytes%4)!=0) {
				offset = (offset/4+1)*4;
			}
		}
	} else if (bmp_bpp == 32) {
		for (int j=0; j<height; j++) {
			int rowBytes = 0;
			for (int i=width-1; i>=0; i--) {
				int alpha = chartouchar(bmp_data[offset])&0xFF;
				int blue = chartouchar(bmp_data[offset+1])&0xFF;
				int green = chartouchar(bmp_data[offset+2])&0xFF;
				int red = chartouchar(bmp_data[offset+3])&0xFF;
				int color = (alpha<<24)|(red<<16)|(green<<8)|blue;
				put_color(x+i, y+j, color);
				offset -= 4;
				rowBytes += 4;
			}
			/*if ((rowBytes%4)!=0) {
				offset = (offset/4-1)*4;
			}*/
		}
	}
}

void draw_image(int* _imageData, int x, int y, int imageSize) {
	char* imageData = (char*)_imageData;
	int fileType = get_file_type(_imageData);
	if (fileType == TYPE_BMP) {
		draw_bmp_image(imageData, x, y);
	} else if (fileType == TYPE_PNG) {
		draw_png_image(imageData, x, y, imageSize);
	}
}

static void draw_and_resize_bmp_image(int* _data, int x, int y, int targetWidth, int targetHeight) {
	char* data = (char*)_data;
	char* bmpBuffer = decode_bmp_image(_data);
	int* intBuffer = char_pixels_to_int_pixels(bmpBuffer, getDecodedBpp(), getDecodedbpp(), getDecodedWidth()*getDecodedHeight());
	free(bmpBuffer);
	int* resizedPixels = resize_pixels(intBuffer, getDecodedWidth(), getDecodedHeight(), targetWidth, targetHeight);
	free(intBuffer);
	bmpBuffer = int_pixels_to_char_pixels(resizedPixels, getDecodedBpp(), getDecodedbpp(), targetWidth*targetHeight);
	free(resizedPixels);
	blit_to_screen(bmpBuffer, x, y, targetWidth, targetHeight, getDecodedBpp(), getDecodedbpp());
	free(bmpBuffer);
}

void draw_and_resize_image(int* _data, int x, int y, int targetWidth, int targetHeight) {
	int fileType = get_file_type(_data);
	if (fileType == TYPE_BMP) {
		draw_and_resize_bmp_image(_data, x, y, targetWidth, targetHeight);
	}
}

void blit_to_screen(char* data, int x, int y, int width, int height, int dataBpp, int databpp) {
	for (int j=0; j<height; j++) {
		for (int i=0; i<width; i++) {
			if (databpp == 24) {
				int offset = (j*width+i)*dataBpp;
				int blue = (int)data[offset]&0xFF;
				int green = (int)data[offset+1]&0xFF;
				int red = (int)data[offset+2]&0xFF;
				int color = (red<<16)|(green<<8)|blue|0xff000000;
				put_color(x+i, y+j, color);
			} else if (databpp == 32) {
				int offset = (j*width+i)*dataBpp;
				int alpha = (int)data[offset]&0xFF;
				int blue = (int)data[offset+1]&0xFF;
				int green = (int)data[offset+2]&0xFF;
				int red = (int)data[offset+3]&0xFF;
				int color = (red<<16)|(green<<8)|blue|(alpha<<24);
				put_color(x+i, y+j, color);
			}
		}
	}
}

char* decode_bmp_image(int* _bmp_data) {
	char* bmp_data = (char*)_bmp_data;
	int width = charstouint(bmp_data[21], bmp_data[20], bmp_data[19], bmp_data[18]);
	int height = charstouint(bmp_data[25], bmp_data[24], bmp_data[23], bmp_data[22]);
	int bmp_bpp = charstoushort(bmp_data[29], bmp_data[28]);
	logf("width: %d, height: %d, bpp: %d\n", width, height, bmp_bpp);
	char* buffer = (char*)malloc(width*height*bmp_bpp/8);
	memset(buffer, 0, width*height*bmp_bpp/8);
	int offset = charstouint(bmp_data[13], bmp_data[12], bmp_data[11], bmp_data[10]);
	int imageSize = charstouint(bmp_data[37], bmp_data[36], bmp_data[35], bmp_data[34]);
	offset += (imageSize-bpp/8);
	if (bmp_bpp == 24) {
		for (int j=0; j<height; j++) {
			int rowBytes = 0;
			for (int i=width-1; i>=0; i--) {
				int blue = chartouchar(bmp_data[offset])&0xFF;
				int green = chartouchar(bmp_data[offset+1])&0xFF;
				int red = chartouchar(bmp_data[offset+2])&0xFF;
				int color = 0xff000000|(red<<16)|(green<<8)|blue;
				put_user_pixel(i, j, color, bmp_bpp/8, bmp_bpp, width, width*3, buffer);
				offset -= 3;
				rowBytes += 3;
			}
			if ((rowBytes%4)!=0) {
				offset = (offset/4+1)*4;
			}
		}
	} else if (bmp_bpp == 32) {
		for (int j=0; j<height; j++) {
			int rowBytes = 0;
			for (int i=width-1; i>=0; i--) {
				int alpha = chartouchar(bmp_data[offset])&0xFF;
				int blue = chartouchar(bmp_data[offset+1])&0xFF;
				int green = chartouchar(bmp_data[offset+2])&0xFF;
				int red = chartouchar(bmp_data[offset+3])&0xFF;
				int color = (alpha<<24)|(red<<16)|(green<<8)|blue;
				put_user_pixel(i, j, color, bmp_bpp/8, bmp_bpp, width, width*bmp_bpp/8, buffer);
				offset -= 4;
				rowBytes += 4;
			}
			if ((rowBytes%4)!=0) {
				offset = (offset/4+1)*4;
			}
		}
	}
	decoded_width = width;
	decoded_height = height;
	decoded_Bpp = bmp_bpp/8;
	decoded_bpp = bmp_bpp;
	return buffer;
}

int* char_pixels_to_int_pixels(char* data, int Bpp, int bpp, int length) {
	int* intPixels = (int*)malloc(sizeof(int)*Bpp*length);
	int offset = 0;
	for (int i=0; i<length; i++) {
		if (bpp == 24) {
			int blue = (int)data[offset]&0xFF;
			int green = (int)data[offset+1]&0xFF;
			int red = (int)data[offset+2]&0xFF;
			int color = (red<<16)|(green<<8)|blue|0xff000000;
			intPixels[i] = color;
			offset += 3;
		} else if (bpp == 32) {
			int alpha = (int)data[offset]&0xFF;
			int blue = (int)data[offset+1]&0xFF;
			int green = (int)data[offset+2]&0xFF;
			int red = (int)data[offset+3]&0xFF;
			int color = (red<<16)|(green<<8)|blue|(alpha<<24);
			intPixels[i] = color;
			offset += 4;
		}
	}
	return intPixels;
}

char* int_pixels_to_char_pixels(int* data, int Bpp, int bpp, int length) {
	char* charPixels = (char*)malloc(sizeof(char)*Bpp*length);
	int offset = 0;
	for (int i=0; i<length; i++) {
		if (bpp == 24) {
			int color = data[i];
			int blue = color&0xFF;
			int green = (color>>8)&0xFF;
			int red = (color>>16)&0xFF;
			charPixels[offset] = blue;
			charPixels[offset+1] = green;
			charPixels[offset+2] = red;
			offset += 3;
		} else if (bpp == 32) {
			int color = data[i];
			int alpha = (color>>24)&0xFF;
			int blue = color&0xFF;
			int green = (color>>8)&0xFF;
			int red = (color>>16)&0xFF;
			charPixels[offset] = alpha;
			charPixels[offset+1] = blue;
			charPixels[offset+2] = green;
			charPixels[offset+3] = red;
			offset += 4;
		}
	}
	return charPixels;
}

int* resize_pixels(int* pixels, int w1, int h1, int w2, int h2) {
    int* temp = (int*)malloc(sizeof(int)*w2*h2);
    int x_ratio = (int)((w1<<16)/w2)+1;
    int y_ratio = (int)((h1<<16)/h2)+1;
    int x2, y2;
    for (int i=0;i<h2;i++) {
        for (int j=0;j<w2;j++) {
            x2 = ((j*x_ratio)>>16) ;
            y2 = ((i*y_ratio)>>16) ;
            temp[(i*w2)+j] = pixels[(y2*w1)+x2] ;
        }                
    }                
    return temp;
}

char* save_canvas() {
	int copyLength = getWidth()*getHeight()*Bpp;
	char* offscreen = (char*)malloc(copyLength);
	memcpy(offscreen, currentScreen, copyLength);
	return offscreen;
}

void set_font(char* data, int size) {
	currentFontData = data;
	currentFontDataSize = size;
}

int getTextWidth(char* text, int size) {
	int width;
	char* argv[5];
	argv[0] = text;
	argv[1] = currentFontData;
	argv[2] = currentFontDataSize;
	argv[3] = size;
	argv[4] = (int)&width;
	load_elf_2((char*)&textwidth, 5, argv);
	return width;
}

extern "C" void drawText(char* text, int x, int y, int size, int color) {
	// x = left of the text
	// y = from bottom of text
	char* argv[9];
	argv[0] = text;
	argv[1] = currentFontData;
	argv[2] = currentFontDataSize;
	argv[3] = screenWidth;
	argv[4] = screenHeight;
	argv[5] = x;
	argv[6] = y;
	argv[7] = size;
	argv[8] = color;
	load_elf_2((char*)&textdrawer, 9, argv);
}

extern "C" void draw_text(char* text, int x, int y, int size, int color) {
	drawText(text, x, y, size, color);
}

void draw_number(int number, int x, int y, int size, int color) {
	drawText(inttostr(number), x, y, size, color);
}

extern "C" int drawChar(char ch, int x, int y, int color) {
	// x = left of the character
	// y = from bottom of character
	
}

extern "C" int getCharWidth(char ch) {
	// x = left of the character
	// y = from bottom of character
	
}

void restore_canvas(char* offscreen) {
	int copyLength = getWidth()*getHeight()*Bpp;
	for (int i=0; i<copyLength; i++) {
		screen[i] = offscreen[i];
	}
}

char* getPrimaryBuffer() {
	return screen;
}

char* getSecondaryBuffer() {
	return currentScreen;
}

char* save_screen_area(int x, int y, int width, int height) {
	char* buffer = (char*)malloc(width*height*Bpp);
	int bufferOffset = 0;
	for (int j=0; j<height; j++) {
		for (int i=0; i<width; i++) {
			int screenOffset = ((y+j)*(int)vesaInfo->lin_bytes_per_scanline)+((x+i)*Bpp);
			for (int k=0; k<Bpp; k++) {
				buffer[bufferOffset] = currentScreen[screenOffset];
				screenOffset++;
				bufferOffset++;
			}
		}
	}
	return buffer;
}

void restore_screen_area(int x, int y, int width, int height, char* buffer) {
	int screenOffset = (y*(int)vesaInfo->lin_bytes_per_scanline)+(x*Bpp);
	int bufferOffset = 0;
	for (int j=0; j<height; j++) {
		for (int i=0; i<width; i++) {
			int screenOffset = ((y+j)*(int)vesaInfo->lin_bytes_per_scanline)+((x+i)*Bpp);
			for (int k=0; k<Bpp; k++) {
				screen[screenOffset] = buffer[bufferOffset];
				screenOffset++;
				bufferOffset++;
			}
		}
	}
	free(buffer);
}

void use_secondary_buffer() {
	currentScreen = offscreen;
}

void use_primary_buffer() {
	currentScreen = screen;
}

void use_user_buffer(char* buffer) {
	currentScreen = buffer;
}

void fill_background(int color) {
	fill_rect(0, 0, getWidth(), getHeight(), 0xff000000|color);
}

void allowDrawOverScreen(bool b) {
	allow_draw_over_screen = b;
}

int get_default_Bpp() {
	return original_Bpp;
}

int get_default_bpp() {
	return original_bpp;
}

int set_user_Bpp(int _Bpp) {
	Bpp = _Bpp;
}

int set_user_bpp(int _bpp) {
	bpp = _bpp;
}

void use_default_Bpp() {
	Bpp = original_Bpp;
}

void use_default_bpp() {
	bpp = original_bpp;
}

void use_default_bytes_per_row() {
	bytesPerRow = originalBytesPerRow;
}

void set_user_bytes_per_row(int bytes) {
	bytesPerRow = bytes;
}

int get_default_bytes_per_row() {
	return originalBytesPerRow;
}

int getDecodedWidth() {
	return decoded_width;
}

int getDecodedHeight() {
	return decoded_height;
}

int getDecodedBpp() {
	return decoded_Bpp;
}

int getDecodedbpp() {
	return decoded_bpp;
}

void parse_image_info(int* _imageData) {
	char* imageData = (char*)_imageData;
	int fileType = get_file_type(_imageData);
	if (fileType == TYPE_BMP) {
		parse_bmp_image_info(_imageData);
	}
}

void set_background(int* imageData) {
	/*parse_image_info(imageData);
	int width = getDecodedWidth();
	int height = getDecodedHeight();
	if (width == getWidth() && height == getHeight()) {
		// Image does not need to be resized
		draw_image(&wallpaper, 0, 0);
	} else {
		draw_and_resize_image(&wallpaper, 0, 0, getWidth(), getHeight());
	}*/
}

int getScreenWidth() {
	return screenWidth;
}

int getScreenHeight() {
	return screenHeight;
}

static void _draw_circle(int cx, int cy, int radius, int color) {
	int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    put_color(cx, cy + radius, color);
    put_color(cx, cy - radius, color);
    put_color(cx + radius, cy, color);
    put_color(cx - radius, cy, color);
    while(x < y)  {
        if(f >= 0)  {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;    
        put_color(cx + x, cy + y, color);
        put_color(cx - x, cy + y, color);
        put_color(cx + x, cy - y, color);
        put_color(cx - x, cy - y, color);
        put_color(cx + y, cy + x, color);
        put_color(cx - y, cy + x, color);
        put_color(cx + y, cy - x, color);
        put_color(cx - y, cy - x, color);
    }
}

void draw_circle(int cx, int cy, int radius, int color, int strokeWidth) {
	for (int i=0; i<strokeWidth; i++) {
		_draw_circle(cx, cy, radius, color);
		radius++;
	}
}

void fill_circle(int cx, int cy, int radius, int color) {
	int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;
    draw_line(cx, cy+radius, cx, cy-radius, color);
    draw_line(cx-radius, cy, cx+radius, cy, color);
    while(x < y)  {
        if(f >= 0)  {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;
        draw_line(cx-x, cy+y, cx+x, cy+y, color);
        draw_line(cx-x, cy-y, cx+x, cy-y, color);
        draw_line(cx-y, cy+x, cx+y, cy+x, color);
        draw_line(cx-y, cy-x, cx+y, cy-x, color);
    }
}

void setClip(int x, int y, int w, int h) {
	clipX = x;
	clipY = y;
	clipWidth = w;
	clipHeight = h;
	clipSet = true;
}

void unsetClip() {
	clipSet = false;
}

void resetClip() {
	clipSet = true;
}

void flush_at_area(int x, int y, int width, int height) {
	for (int j=y; j<y+height; j++) {
		for (int i=x; i<x+width; i++) {
			int offset = get_offset(i, j);
			for (int i=0; i<Bpp; i++) {
				screen[offset] = currentScreen[offset];
				offset++;
			}
		}
	}
}

void reset_screen_buffer() {
	int copyLength = getWidth()*getHeight()*Bpp;
	memset(offscreen, 0, copyLength);
}

void init_hardware_graphics() {
	chosenGraphicsHardware = INTEL_SKYLAKE;
	if (chosenGraphicsHardware == INTEL_SKYLAKE) {
		init_intel_skylake();
	}
}
