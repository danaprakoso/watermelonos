#include <system.h>

PCIDevice** devices = 0;
int num_devices = 0;
int max_devices = 64;

void add_device(PCIDevice* device) {
	if (num_devices >= max_devices) {
		max_devices *= 2;
		PCIDevice** new_devices = (PCIDevice**)malloc(sizeof(PCIDevice)*max_devices);
		for (int i=0; i<num_devices; i++) {
			new_devices[i] = devices[i];
		}
		devices = new_devices;
	}
	devices[num_devices] = device;
	num_devices++;
}

int pci_read_int(int bus, int slot, int func, int offset) {
	int address = (bus<<16)|(slot<<11)|(func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	return in32(0xCFC);
}

int pci_read_int_2(PCIDevice* device, int offset) {
	int address = (device->bus<<16)|(device->slot<<11)|(device->func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	return in32(0xCFC);
}

void pci_write_int(int bus, int slot, int func, int offset, int value) {
	int address = (bus<<16)|(slot<<11)|(func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	out32(0xCFC, value);
}

void pci_write_int_2(PCIDevice* device, int offset, int value) {
	int address = (device->bus<<16)|(device->slot<<11)|(device->func<<7)|(offset&0xFC)|0x80000000;
	out32(0xCF8, address);
	out32(0xCFC, value);
}

void enum_pci() {
	for (int bus=0; bus<256; bus++) {
		for (int slot=0; slot<32; slot++) {
			for (int func=0; func<8; func++) {
				int vendorID = pci_read_int(bus, slot, func, 0)&0xFFFF;
				if (vendorID == 0xFFFF) {
					continue;
				}
				int deviceID = pci_read_int(bus, slot, func, 0)>>16;
				PCIDevice* device = (PCIDevice*)malloc(sizeof(PCIDevice));
				device->vendorID = vendorID;
				device->deviceID = deviceID;
				device->bus = bus;
				device->slot = slot;
				device->func = func;
				device->bar0 = pci_read_int(bus, slot, func, 0x10);
				device->bar1 = pci_read_int(bus, slot, func, 0x14);
				device->bar2 = pci_read_int(bus, slot, func, 0x18);
				device->bar3 = pci_read_int(bus, slot, func, 0x1C);
				device->bar4 = pci_read_int(bus, slot, func, 0x20);
				device->bar5 = pci_read_int(bus, slot, func, 0x24);
				int _class = pci_read_int(bus, slot, func, 0x08);
				_class >>= 16;
				device->_subclass = _class&0xFF;
				_class >>= 8;
				device->_class = _class;
				add_device(device);
			}
		}
	}
}

PCIDevice* get_pci_device(int _class, int _subclass) {
	for (int i=0; i<num_devices; i++) {
		PCIDevice* device = devices[i];
		if (device->_class == _class && device->_subclass == _subclass) {
			return device;
		}
	}
	return 0;
}

PCIDevice* get_pci_device_2(int vendorID, int deviceID) {
	for (int i=0; i<num_devices; i++) {
		PCIDevice* device = devices[i];
		if (device->vendorID == vendorID && device->deviceID == deviceID) {
			return device;
		}
	}
	return 0;
}

char* get_device_name(PCIDevice* device) {
	return "";
}

void init_pci() {
	devices = (PCIDevice**)malloc(sizeof(PCIDevice*)*max_devices);
}

void dump_pci_regs(int _class, int _subclass) {
	PCIDevice* device = get_pci_device(_class, _subclass);
	if (device == NULL) {
		printf("Device with class ID %d and subclass ID %d not found", _class, _subclass);
	} else {
		printf("BAR0: %x\n", device->bar0);
		printf("BAR1: %x\n", device->bar1);
		printf("BAR2: %x\n", device->bar2);
		printf("BAR3: %x\n", device->bar3);
		printf("BAR4: %x\n", device->bar4);
		printf("BAR5: %x\n", device->bar5);
	}
}

void dump_pci_regs_2(int vendorID, int deviceID) {
	PCIDevice* device = get_pci_device_2(vendorID, deviceID);
	if (device == NULL) {
		printf("Device with vendor ID %x and device ID %x not found", vendorID, deviceID);
	} else {
		printf("BAR0: %x\n", device->bar0);
		printf("BAR1: %x\n", device->bar1);
		printf("BAR2: %x\n", device->bar2);
		printf("BAR3: %x\n", device->bar3);
		printf("BAR4: %x\n", device->bar4);
		printf("BAR5: %x\n", device->bar5);
	}
}
