#include <system.h>

void read_sector(Storage* storage, int lba, int count, char* buffer) {
	if (ide_device != 0) {
		ide_read_sector(storage, lba, count, buffer);
	} else if (ahci_device != 0) {
		ahci_read_sector(lba, count, buffer);
	}
}

Storage* findStorageByDriveLetter(char ch) {
	for (int i=0; i<totalStorage; i++) {
		Storage* storage = storages[i];
		for (int j=0; j<storage->totalPartition; j++) {
			Partition* p = storage->partitions[i];
			if (p->getDriveLetter() == ch) {
				return storage;
			}
		}
	}
	return NULL;
}

void init_diskio() {
	storages = (Storage**)malloc(sizeof(Storage*)*64);
	ide_device = get_pci_device(0x1, 0x1);
	if (ide_device != 0) {
		init_ide_device();
	} else {
		ahci_device = get_pci_device(0x1, 0x6);
	}
	// Collects storages
	if (ide_device != 0) {
		ide_collect_storages();
	}
}

void addStorage(Storage* storage) {
	storages[totalStorage] = storage;
	totalStorage++;
}
