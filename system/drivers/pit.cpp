#include <system.h>

static bool waiting = false;
static int time = 0;

void pit_handler(Register* r) {
	if (mouse_timeout > 0) {
		mouse_timeout--;
	}
	/*if (textFieldCaretTimeout > 0) {
		textFieldCaretTimeout--;
	} else {
		textFieldCaretOn = !textFieldCaretOn;
		textFieldCaretTimeout = 300;
	}*/
	if (waiting) {
		time++;
	}
}

void sleep(long wait_time) {
	waiting = true;
	while (time < wait_time);
	time = 0;
}

void init_pit(int millis_per_second) {
	int a = 1193180/millis_per_second;
	out8(0x43, 0x36);
	out8(0x40, a&0xFF);
	out8(0x40, a>>8&0xFF);
	irq_set_handler(0, pit_handler);
}
