[BITS 32]

global enable_paging
enable_paging:
	push ebp
	mov ebp, esp
	mov eax, cr0
	or eax, (1<<31)
	mov cr0, eax
	mov esp, ebp
	pop ebp
	ret

global set_pagedir
set_pagedir:
	push ebp
	mov ebp, esp
	mov eax, [esp+8]
	mov cr3, eax
	mov esp, ebp
	pop ebp
	ret
