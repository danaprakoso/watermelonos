#include <system.h>

int myAbs(int number) {
	if (number < 0) {
		return -number;
	}
	return number;
}
