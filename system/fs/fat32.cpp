#include <system.h>

void fat32_read_sector(int lba, int count, char* buffer) {
}

int fat32_delete_file(char* path) {
    return 0;
}

int fat32_write_file(char* path, char* content, int size) {
    return 0;
}

int fat32_update_file(char* path, char* content, int size) {
    return 0;
}

File* fat32_open_file(char* path) {
    char label = path[0];
    Storage* storage = getStorage(label);
    path = substring(path, 3, strlen(path));
    if (!ends_with(path, '/')) {
        path = strcombine(path, "/");
    }
    Partition* p = NULL;
    for (int i = 0; i < storage->totalPartition; i++) {
        p = storage->partitions[i];
        if (p->label == label) {
            break;
        }
    }
    int next_token = 1;
    int total_token = get_total_char_from_string(path, '/');
    if (p != NULL) {
        char bpb[512];
        if (storage->interface == IDE) {
            ide_read_sector(storage, p->startingSector, 1, bpb);
        }
        int sectors_per_cluster = chartouchar(bpb[13]);
        int bytes_per_sector = charstoushort(bpb[12], bpb[11]);
        int bytes_per_cluster = bytes_per_sector*sectors_per_cluster;
        int reserved_sectors = charstoushort(bpb[15], bpb[14]);
        int fat_size /* in sectors */ = charstouint(bpb[39], bpb[38], bpb[37], bpb[36]);
        int fat_count = chartouchar(bpb[16]);
        int first_data_sector = p->startingSector + reserved_sectors + (fat_count * fat_size);
        int lba = first_data_sector;
        while (next_token - 1 < total_token) {
            char* dirEntry = (char*) malloc(sectors_per_cluster * bytes_per_sector);
            ide_read_sector(storage, lba, sectors_per_cluster, dirEntry);
            char* filename = get_next_token(path, '/', &next_token);
            next_token++;
            char* newfilename = get_fat_file_name(filename);
            int c = 0;
            bool file_found = false;
            while (c < bytes_per_cluster) {
                if (strcmpl(newfilename, dirEntry + c, 11)) {
                    file_found = true;
                    break;
                }
                c += 16;
            }
            if (!file_found) {
                return NULL;
            }
            int file_cluster_high = charstoushort(dirEntry[c + 21], dirEntry[c + 20]);
            int file_cluster_low = charstoushort(dirEntry[c + 27], dirEntry[c + 26]);
            int file_cluster = (file_cluster_high << 16) | file_cluster_low;
            int file_size = charstouint(dirEntry[c + 31], dirEntry[c + 30], dirEntry[c + 29], dirEntry[c + 28]);
            int data_sector = (file_cluster - 2) * sectors_per_cluster + first_data_sector;
            lba = data_sector;
            if (next_token - 1 == total_token) {
                int total_sectors = file_size / 512;
                if ((file_size % 512) != 0) {
                    total_sectors++;
                }
                char* data = (char*) malloc(total_sectors * bytes_per_sector);
                if (total_sectors <= sectors_per_cluster) {
                    ide_read_sector(storage, lba, total_sectors, data);
                    File* file = new File(data, file_size);
                    return file;
                } else {
                    int cluster_needed = file_size/bytes_per_sector;
                    if ((file_size%bytes_per_sector) != 0) {
                    	cluster_needed++;
                    }
                    for (int i=0; i<cluster_needed; i++) {
                    	
                    }
                }
            }
        }
    }
    return NULL;
}

int fat32_format(char* path, char* diskLabel, int bytesPerSector, int sectorsPerCluster) {
    return 0;
}
