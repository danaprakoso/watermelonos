#include <system.h>

void elf_run(char* data, int argc, char** argv) {
	ELFHeader* h = (ELFHeader*)data;
	int sh_count = (int)h->shnum;
	if (sh_count >= 1) {
		int sh_offs = (int)data+h->shoff;
		for (int i=0; i<sh_count; i++) {
			ELFSectionHeader* sh = (ELFSectionHeader*)sh_offs;
			if (sh->type == 0x08) {
				memset(sh->addr, 0, sh->size);
			} else {
				memcpy(sh->addr, (int)data+sh->offset, sh->size);
			}
			sh_offs += sizeof(ELFSectionHeader);
		}
		void (*go)();
		go = h->entry;
		go();
	}
}

void run_app(char* data, int argc, char** argv) {
	// Check format of the executable file
	if (data[0] == 0x7F && data[1] == 'E' && data[2] == 'L' && data[3] == 'F') {
		elf_run(data, argc, argv);
	}
}
