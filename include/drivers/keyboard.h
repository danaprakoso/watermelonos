#ifndef KEYBOARD_H
#define KEYBOARD_H

void init_keyboard();
bool isKeyPressed();
void waitForKey();
int getPressedKey();
extern unsigned char kbdus[128];

#endif
