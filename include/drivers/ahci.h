#ifndef AHCI_H
#define AHCI_H

extern PCIDevice* ahci_device;
void ahci_read_sector(int lba, int count, char* buffer);

#endif
