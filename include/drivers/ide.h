#ifndef IDE_H
#define IDE_H

#include <drivers/diskio.h>

extern PCIDevice* ide_device;
void init_ide_device();
void ide_read_sector(Storage* storage, int lba, int count, char* buffer);
void ide_read_sector_dvd(Storage* storage, int lba, int count, char* buffer);
void ide_read_sector_harddisk(int lba, int count, char* buffer);
void ide_collect_storages();
void ide_write_sector(Storage* storage, int lba, int count, char* buffer);

#endif
