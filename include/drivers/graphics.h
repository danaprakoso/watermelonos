#ifndef GRAPHICS_H
#define GRAPHICS_H

#define TYPE_UNKNOWN 0
#define TYPE_BMP 1

#define INTEL_SKYLAKE 1

typedef struct {
	char* data;
	int width;
	int height;
	int bpp;
	int Bpp;
} BufferInfo;

void init_graphics(VesaInfo* vesaInfo);
int get_color(int x, int y);
void flush();
void put_color(int x, int y, int color);
void fill_rect(int x, int y, int width, int height, int color);
void draw_line(int x1, int y1, int x2, int y2, int color);
char* getCurrentScreen();
int getWidth();
int getHeight();
char* save_canvas();
void restore_canvas(char* offscreen);
char* save_screen_area(int x, int y, int width, int height);
void restore_screen_area(int x, int y, int width, int height, char* buffer);
void use_primary_buffer();
void use_secondary_buffer();
void fill_background(int color);
void allowDrawOverScreen(bool b);
extern "C" void loadFontFromMemory(int* data, int fontDataSize);
extern "C" void setFontSize(int size);
extern "C" void drawText(char* text, int x, int y, int size, int color);
extern "C" void draw_text(char* text, int x, int y, int size, int color);
extern "C" void destroyFont();
char* decode_bmp_image(int* _bmp_data);
void use_user_buffer(char* buffer);
char* getPrimaryBuffer();
char* getSecondaryBuffer();
int set_user_bpp(int _bpp);
int set_user_Bpp(int _Bpp);
int get_default_bpp();
int get_default_Bpp();
void use_default_Bpp();
void use_default_bpp();
int get_default_bytes_per_row();
void set_user_bytes_per_row(int bytes);
void use_default_bytes_per_row();
void put_user_pixel(int x, int y, int color, int Bpp, int bpp, int width, int bytesPerRow, char* buffer);
int get_user_pixel(int x, int y, int Bpp, int bpp, int width, int bytesPerRow, char* buffer);
void blit_to_screen(char* data, int x, int y, int width, int height, int dataBpp, int databpp);
int getDecodedbpp();
int getDecodedBpp();
int getDecodedHeight();
int getDecodedWidth();
int* char_pixels_to_int_pixels(char* data, int Bpp, int bpp, int length);
char* int_pixels_to_char_pixels(int* data, int Bpp, int bpp, int length);
int* resize_pixels(int* pixels, int w1, int h1, int w2, int h2);
void draw_and_resize_image(int* _data, int x, int y, int targetWidth, int targetHeight);
void draw_image(int* _imageData, int x, int y, int imageSize);
void set_background(int* imageData);
void parse_image_info(int* _imageData);
int getScreenWidth();
int getScreenHeight();
void draw_circle(int cx, int cy, int radius, int color, int strokeWidth);
void fill_circle(int cx, int cy, int radius, int color);
void unsetClip();
void setClip(int x, int y, int w, int h);
void resetClip();
int getTextWidth(char* text, int size);
void flush_at_area(int x, int y, int width, int height);
extern "C" int drawChar(char ch, int x, int y, int color);
extern "C" int getCharWidth(char ch);
void reset_screen_buffer();
void draw_rect(int x, int y, int width, int height, int color);
void init_hardware_graphics();
extern "C" VesaInfo* vesaInfo;
char* get_screen();
void set_font(char* data, int size);
void draw_number(int number, int x, int y, int size, int color);

#endif
