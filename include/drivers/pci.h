#ifndef PCI_H
#define PCI_H

typedef struct {
	int vendorID, deviceID;
	int bus;
	int slot;
	int func;
	int bar0, bar1, bar2, bar3, bar4, bar5;
	int _class, _subclass;
} PCIDevice;

void init_pci();
void enum_pci();
PCIDevice* get_pci_device(int _class, int _subclass);
PCIDevice* get_pci_device_2(int vendorID, int deviceID);
char* get_device_name(PCIDevice* device);
int pci_read_int(int bus, int slot, int func, int offset);
int pci_read_int_2(PCIDevice* device, int offset);
void pci_write_int(int bus, int slot, int func, int offset, int value);
void pci_write_int_2(PCIDevice* device, int offset, int value);
void dump_pci_regs(int _class, int _subclass);
void dump_pci_regs_2(int vendorID, int deviceID);

#endif
