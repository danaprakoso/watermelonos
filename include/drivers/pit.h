#ifndef PIT_H
#define PIT_H

void init_pit(int millis_per_second);
void sleep(long wait_time);

#endif
