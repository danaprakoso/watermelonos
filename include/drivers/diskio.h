#ifndef DISKIO_H
#define DISKIO_H

#include <drivers/storage.h>

#define STORAGE_TYPE_HARDDISK 1
#define STORAGE_TYPE_DVD 2
#define STORAGE_TYPE_FLASHDISK 3

void init_diskio();
void read_sector(Storage* storage, int lba, int count, char* buffer);
Storage* findStorageByDriveLetter(char ch);
void addStorage(Storage* storage);
File* open_file(char* path);

#endif
