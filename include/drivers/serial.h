#ifndef SERIAL_H
#define SERIAL_H

void init_serial(int port0, int port1, int port2, int port3);
char read_serial();
void write_serial(char a);

#endif
