#ifndef SKYLAKE_H
#define SKYLAKE_H

#define INTEL_GEN_1 1
#define INTEL_GEN_2 2
#define INTEL_GEN_3 3
#define INTEL_GEN_4 4
#define INTEL_GEN_5 5
#define INTEL_GEN_6 6
#define INTEL_GEN_7 7
#define INTEL_GEN_8 8
#define INTEL_GEN_9 9

#define INTEL_SKYLAKE_GDRST 0x941c

void init_intel_skylake();

#endif
