#ifndef VIEW_H
#define VIEW_H

#define VIEW_TYPE_TEXTVIEW 1
#define VIEW_TYPE_IMAGEVIEW 2
#define VIEW_TYPE_CONTAINER 3
#define VIEW_TYPE_TEXTFIELD 4
#define VIEW_TYPE_RADIOVIEW 5
#define VIEW_TYPE_RADIOGROUP 6
#define VIEW_TYPE_CHECKBOX 7
#define VIEW_TYPE_CHECKBOXGROUP 8
#define VIEW_TYPE_BUTTON 9
#define VIEW_TYPE_TREEVIEW 10
#define VIEW_TYPE_SIDEBAR 11
#define VIEW_TYPE_LISTVIEW 12

typedef struct OnClickListenerInfo_t;

class View;

void registerOnClickListener(View* v, void (*onClickListener)(View* v));
void registerOnFocusListener(View* v, void (*onFocusListener)(View* v));
void registerOnHoverListener(View* v, void (*onHoverListener)(View* v));

class View {
private:
	int* buffer;
	int id;
	int x, y;
	int width, height;
	int type = 0;
	View* parent = NULL;
	void (*onClickListener)(View* v) = NULL;
	void (*onFocusListener)(View* v) = NULL;
	void (*onHoverListener)(View* v) = NULL;
public:
	int background = 0xffffffff;
	int selectedBackground = 0xff2196f3;
	bool selected = false;
	char* title = NULL;
	int titleColor;
	int titleBackgroundColor;
	bool hovered = false;
	View();
	void setPosition(int x, int y);
	void setSize(int w, int h);
	int* getBuffer();
	int getX();
	int getY();
	int getWidth();
	int getHeight();
	void setViewType(int type);
	void setBackground(int bg);
	void setSelectedBackground(int bg) {
		selectedBackground = bg;
	}
	void setParent(View* v) {
		parent = v;
	}
	View* getParent() { return parent; }
	int getBackground() {
		return background;
	}
	int getSelectedBackground() {
		return selectedBackground;
	}
	void setOnClickListener(void (*onClickListener)(View* v)) {
		this->onClickListener = onClickListener;
		registerOnClickListener(this, onClickListener);
	}
	void setOnFocusListener(void (*onFocusListener)(View* v)) {
		this->onFocusListener = onFocusListener;
		registerOnFocusListener(this, onFocusListener);
	}
	void setOnHoverListener(void (*onHoverListener)(View* v)) {
		this->onHoverListener = onHoverListener;
		registerOnHoverListener(this, onHoverListener);
	}
	int getViewType() {
		return type;
	}
	void setX(int x) {
		this->x = x;
	}
	void setY(int y) {
		this->y = y;
	}
	void setWidth(int w) {
		width = w;
	}
	void setHeight(int h) {
		height = h;
	}
	void setId(int id) {
		this->id = id;
	}
	int getId() {
		return id;
	}
	void setTitle(char* text) {
		title = text;
	}
	char* getTitle() { return title; }
	bool isSelected() { return selected; }
};

typedef struct OnClickListenerInfo_t {
	View* view;
	void (*onClickListener)(View* v);
} OnClickListenerInfo;

typedef struct OnFocusListenerInfo_t {
	View* view;
	void (*onFocusListener)(View* v);
} OnFocusListenerInfo;

typedef struct OnHoverListenerInfo_t {
	View* view;
	void (*onHoverListener)(View* v);
} OnHoverListenerInfo;

extern OnClickListenerInfo onClickListeners[100];
extern OnFocusListenerInfo onFocusListeners[100];
extern OnHoverListenerInfo onHoverListeners[100];
extern int totalOnClickListeners;
extern int totalOnFocusListeners;
extern int totalOnHoverListeners;
void unregisterAllListeners();

#endif
