#ifndef IMAGEBUTTON_H
#define IMAGEBUTTON_H

#include <ui/View.h>

class ImageButton:public View {
public:
	int* data;
	int dataSize;
	int resizedWidth;
	int resizedHeight;
	void (*onClickListener)(View* view);
	ImageButton(int* data, int dataSize, int x, int y, int width, int height);
	ImageButton(int* data, int dataSize, int x, int y, int width, int height, void (*onClickListener)(View* v));
	ImageButton(int* data, int dataSize, int x, int y, int width, int height, int resizedWidth, int resizedHeight, void (*onClickListener)(View* v));
	ImageButton(void (*onClickListener)(View* v));
	void apply();
};

#endif
