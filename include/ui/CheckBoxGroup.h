#ifndef CHECKBOXGROUP_H
#define CHECKBOXGROUP_H

#include <ui/CheckBox.h>

class CheckBoxGroup:public View {
private:
	CheckBox** childs;
	int totalChilds = 0;
	int maxChilds = 100;
public:
	CheckBoxGroup();
	void addCheckBox(CheckBox* cb);
	void apply();
};

#endif
