#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include <ui/View.h>

class ImageView:public View {
private:
	int resizedWidth = 0;
	int resizedHeight = 0;
public:
	int* data;
	ImageView();
	ImageView(int* data, int x, int y, int resizeWidth, int resizeHeight) {
		setViewType(VIEW_TYPE_IMAGEVIEW);
		this->data = data;
		setX(x);
		setY(y);
		this->resizedWidth = resizeWidth;
		this->resizedHeight = resizeHeight;
	}
	void setData(int* data);
	int getResizedWidth();
	int getResizedHeight();
	void resizeTo(int w, int h);
	void apply();
};

#endif
