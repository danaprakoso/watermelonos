#ifndef CHECKBOX_H
#define CHECKBOX_H

class CheckBox:public View {
private:
	bool checked = false;
	char* text;
	int textColor;
public:
	CheckBox();
	CheckBox(int x, int y, char* text, int textColor, bool checked);
	CheckBox(int x, int y, char* text, int textColor, bool checked, int id);
	CheckBox(int x, int y, int width, int height, char* text, int textColor, bool checked, int id);
	char* getText() { return text; }
	void setText(char* text) {
		this->text = text;
	}
	int getTextColor() { return textColor; }
	void setTextColor(int color) {
		this->textColor = color;
	}
	bool isChecked() { return checked; }
	void setChecked(bool b) {
		checked = b;
	}
	void apply();
};

#endif
