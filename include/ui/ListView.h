#ifndef ListVIEW_H
#define ListVIEW_H

class ListView:public View {
public:
	int columnCount = 0;
	int totalItems = 0;
	int maxItems = 100;
	int nextY = -1;
	View** items;
	int currentIndex = 0;
	ListView();
	ListView(int x, int y, int width, int height);
	void addItem(View* v);
	~ListView();
	void apply();
};

#endif
