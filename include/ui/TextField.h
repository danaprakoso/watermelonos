#ifndef TEXTFIELD_H
#define TEXTFIELD_H

#include <ui/Background.h>

class TextField:public View {
private:
	Background* background;
	bool focused = false;
public:
	char textBuffer[1000];
	int textFieldCaretX = 5;
	int textX = 3;
	int totalTextWidth = 0;
	int totalChars = 0;
	TextField();
	int strokeColor;
	int strokeWidth;
	void setBackground(Background* background);
	int getStrokeColor() {
		return strokeColor;
	}
	int getStrokeWidth() {
		return strokeWidth;
	}
	void setStrokeColor(int color) {
		strokeColor = color;
	}
	void setStrokeWidth(int width) {
		strokeWidth = width;
	}
	bool isFocused() { return focused; }
	void setFocused(bool b) { this->focused = b; }
	void apply();
};

extern bool textFieldCaretOn;
extern int textFieldCaretTimeout;

#endif
