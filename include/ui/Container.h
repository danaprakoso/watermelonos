#ifndef CONTAINER_H
#define CONTAINER_H

#include <ui/SideBar.h>
#include <ui/ImageButton.h>

class Container:public View {
private:
	View** childs;
	SideBar* sideBar;
public:
	int borderColor = 0;
	ImageButton* closeButton = NULL;
	int totalChilds = 0;
	int maxChilds = 100;
	Container();
	Container(int x, int y, int width, int height);
	void addChild(View* v);
	void apply();
	void setSideBar(SideBar* sideBar) {
		this->sideBar = sideBar;
	}
	void setBorderColor(int color) {
		borderColor = color;
	}
	void setCloseButton(ImageButton* btn) {
		closeButton = btn;
		closeButton->setParent(this);
		closeButton->setX(getX()+getWidth()-30);
		closeButton->setY(getY()+9);
		closeButton->setWidth(20);
		closeButton->setHeight(20);
	}
	View* getChildById(int id);
	void focus();
	void unfocus();
	View** getChilds() { return childs; }
	void setSelected(bool selected);
	~Container();
};

#endif
