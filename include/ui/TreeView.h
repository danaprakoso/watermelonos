#ifndef TREEVIEW_H
#define TREEVIEW_H

typedef struct {
	ImageView* icon;
	char* title;
	int textColor;
	int backgroundColor;
	int selectedTextColor;
	int selectedBackgroundColor;
} TreeNode;

class TreeView:public View {
public:
	TreeView();
	TreeNode** nodes = NULL;
	int selectedNode = 0;
	int totalNodes = 0;
	int maxNodes = 100;
	int nextY = -1;
	void addNode(ImageView* img, char* title, int textColor, int backgroundColor, int selectedTextColor, int selectedBackgroundColor);
};

#endif
