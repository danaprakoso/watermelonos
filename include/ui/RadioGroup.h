#ifndef RADIOGROUP_H
#define RADIOGROUP_H

#include <ui/RadioView.h>

class RadioGroup:public View {
private:
	RadioView** childs;
	int nextX = -1;
public:
	int totalChilds = 0;
	int maxChilds = 10;
	RadioGroup();
	void addRadioView(RadioView* v);
	void apply();
};

#endif
