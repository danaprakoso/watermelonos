#ifndef TEXTVIEW_H
#define TEXTVIEW_H

typedef struct {
	char* text;
	int x;
	int y;
	int color;
} TextViewInfo;

#define DEFAULT_TEXT_SIZE 15

class TextView:public View {
private:
public:
	int color;
	int selectedColor;
	int textSize;
	char* text;
	TextView();
	TextView(char* text, int x, int y, int color);
	TextView(char* text, int x, int y, int color, int selectedColor);
	void setColor(int color);
	void setTextSize(int textSize);
	void setText(char* text);
	void apply();
};

#endif
