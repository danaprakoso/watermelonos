#ifndef BACKGROUND_H
#define BACKGROUND_H

#define BACKGROUND_TYPE_LINEAR_GRADIENT 100001
#define BACKGROUND_TYPE_RADIAL_GRADIENT 100002
#define BACKGROUND_TYPE_SOLID 100003

class Background {
private:
	int type;
public:
	Background();
	int getType() {
		return type;
	}
	void setType(int type) {
		this->type = type;
	}
};

class SolidBackground:public Background {
private:
	int color;
public:
	SolidBackground();
	SolidBackground(int color);
	int getColor() {
		return color;
	}
	void setColor(int color) {
		this->color = color;
	}
};

#endif
