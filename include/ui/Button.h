#ifndef BUTTON_H
#define BUTTON_H

class Button:public View {
private:
public:
	char* text;
	int textColor;
	int backgroundColor = 0xff2196f3;
	int hoveredBackgroundColor = 0xff1a77c1;
	int clickedBackgroundColor = 0xff125488;
	void (*onClickListener)(View* view);
	Button();
	Button(int x, int y, int width, int height, char* text);
	Button(int x, int y, int width, int height, char* text, void (*onClickListener)(View* v));
	Button(int x, int y, int width, int height, char* text, int backgroundColor, void (*onClickListener)(View* v));
	char* getText() { return text; }
	int getTextColor() { return textColor; }
	int getBackgroundColor() { return backgroundColor; }
	void setText(char* text) { this->text = text; }
	void setTextColor(int color) { this->textColor = color; }
	void setBackgroundColor(int color) { this->backgroundColor = color; }
	void apply();
};

#endif
