#ifndef GRIDVIEW_H
#define GRIDVIEW_H

class GridView:public View {
public:
	int columnCount = 0;
	int totalItems = 0;
	int maxItems = 0;
	View** items;
	GridView();
	void addItem(View* v);
	~GridView();
};

#endif
