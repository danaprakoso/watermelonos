#ifndef SIDEBAR_H
#define SIDEBAR_H

#include <ui/View.h>

class SideBar:public View {
public:
	SideBar() {
		setViewType(VIEW_TYPE_SIDEBAR);
	}
	SideBar(int x, int y, int width, int height, int backgroundColor) {
		setViewType(VIEW_TYPE_SIDEBAR);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
		this->backgroundColor = backgroundColor;
	}
	View* content;
	int backgroundColor = 0xffffffff;
	void setContent(View* content) {
		this->content = content;
		content->setParent(this);
		content->setX(getX());
		content->setY(getY());
		content->setWidth(getWidth());
		content->setHeight(getHeight());
	}
	void setBackgroundColor(int color) {
		backgroundColor = color;
	}
	void apply();
};

#endif
