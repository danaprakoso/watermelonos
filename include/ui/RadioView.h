#ifndef RADIOVIEW_H
#define RADIOVIEW_H

class RadioView:public View {
private:
	int checkedColor = 0xff009688;
	int checkedBorderColor = 0xff009688;
	int uncheckedColor = 0xffffffff;
	int uncheckedBorderColor = 0xff6d6d6d;
	int cx = 0;
	int cy = 0;
	bool checked = false;
	char* text = NULL;
	int textColor;
public:
	RadioView();
	RadioView(int centerX, int centerY, char* text, int textColor, bool checked);
	RadioView(int centerX, int centerY, char* text, int textColor, bool checked, int id);
	int getCheckedColor() { return checkedColor; }
	int getCheckedBorderColor() { return checkedBorderColor; }
	int getUncheckedColor() { return uncheckedColor; }
	int getUncheckedBorderColor() { return uncheckedBorderColor; }
	int getCenterX() {
		return cx;
	}
	int getCenterY() {
		return cy;
	}
	void setCenterX(int cx) {
		this->cx = cx;
	}
	void setCenterY(int cy) {
		this->cy = cy;
	}
	void setChecked(bool checked) {
		this->checked = checked;
	}
	bool isChecked() {
		return checked;
	}
	char* getText() {
		return text;
	}
	void setText(char* text) {
		this->text = text;
	}
	int getTextColor() {
		return textColor;
	}
	void setTextColor(int color) {
		textColor = color;
	}
	void apply();
};

#endif
