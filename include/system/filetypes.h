#ifndef FILETYPES_H
#define FILETYPES_H

#define TYPE_BMP 1
#define TYPE_PNG 2

int get_file_type(int* _data);

#endif
