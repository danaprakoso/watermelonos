#ifndef IO_H
#define IO_H

void out8(int port, char value);
char in8(int port);
void out16(int port, short value);
short in16(int port);
void out32(int port, int value);
int in32(int port);
void mmio_write_8(int addr, char value);
char mmio_read_8(int addr);
void mmio_write_16(int addr, short value);
short mmio_read_16(int addr);
void mmio_write_32(int addr, int value);
int mmio_read_32(int addr);

#endif
