#ifndef RESOURCES_H
#define RESOURCES_H

#define MONACO_FONT_SIZE 59872
#define OPEN_SANS_LIGHT_FONT_SIZE 222412

extern "C" int cursor_image;
extern "C" int checkbox_checked_image;
extern "C" int checkbox_unchecked_image;
extern "C" int close_icon;
extern "C" int instagram_icon;
extern "C" int pngreader;
extern "C" int allocator;
extern "C" int textdrawer;
extern "C" int textwidth;
extern "C" int wallpaper;
extern "C" int monaco_font;
extern "C" int apps_icon;
extern "C" int calculator_icon;
extern "C" int close_icon;

/*extern "C" int wallpaper;
extern "C" int monaco_font;
extern "C" int open_sans_light;
extern "C" int small_folder_icon;
extern "C" int small_usb_icon;
extern "C" int file_icon;
extern "C" int pngloader;
extern "C" int userlibinit;
extern "C" int mainapp;
extern "C" int program02;*/

#endif
