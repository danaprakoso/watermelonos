#ifndef USERLIB_H
#define USERLIB_H

void set_userlib_put_color_func(void (*_put_color)(int x, int y, int color));
void set_userlib_get_color_func(void (*_get_color)(int x, int y));
void init_user_libc();
extern "C" void (*put_color_ptr)(int x, int y, int color);
extern "C" int (*get_color_ptr)(int x, int y);

#endif
