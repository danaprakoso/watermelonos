#ifndef MY_STRING_H
#define MY_STRING_H

int strlen(char* string);
char* replace_string(char* original_string, char toReplace, char with);
char* substring(char* string, int from, int to);
char* concat(char* string1, char* string2);
bool ends_with(char* string, char with);
int charstoint(char c1, char c2, char c3, char c4);
unsigned int charstouint(char c1, char c2, char c3, char c4);
int charstoshort(char c1, char c2);
unsigned int charstoushort(char c1, char c2);
char* get_node(char* string, char delimiter, int nodenum);
char* upper_case(char* text);
bool strcmpl(char* text1, char* text2, int length);
int index_of(char* string, char ch);
int chartouchar(char ch);
int numlen(int number);
int numlenu(unsigned int number);
int numlenhex(unsigned int number);
char* strclone(char* text);
int get_char_index_from_string(char* text, char ch, int from, int defaultValue);
int get_last_char_index_from_string(char* text, char ch);
unsigned int uchartouint(unsigned char ch);
unsigned int ucharstouint(unsigned char ch1, unsigned char ch2, unsigned char ch3, unsigned char ch4);
unsigned int ucharstoushort(unsigned char ch1, unsigned char ch2);
int search_string(char* text, char* toSearch, int from, int defaultValue);
bool strcmp(char* text1, char* text2, int length);
char* strcombine(char* text1, char* text2);
int get_total_char_from_string(char* text, char ch);
int get_total_bits(int value, int bit);
char* get_next_token(char* text, char delimiter, int* next_token);
int pow10(int power);
int strtoint(char* text);
char* inttostr(int number);

#endif
