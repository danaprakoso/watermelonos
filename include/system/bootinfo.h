#ifndef BOOTINFO_H
#define BOOTINFO_H

typedef struct {
	char signature[4];
	short version;
	int oemStringPtr;
	int capabilities;
	int videoModesPtr;
	short totalMemory;
	short oemSoftwareRev;
	int oemVendorNamePtr;
	int oemProductNamePtr;
	int oemProductRevPtr;
	char reserved[222];
	char oemData[256];
} VbeInfoBlock;

typedef struct {
	short mode_attrib;
	char win_a_attrib;
	char win_b_attrib;
	short win_gran;
	short win_size;
	short win_a_seg;
	short win_b_seg;
	int win_func_ptr;
	short bytes_per_scanline;
	short x_res;
	short y_res;
	char x_char_size;
	char y_char_size;
	char num_of_planes;
	char bits_per_pixel;
	char num_of_banks;
	char mem_model;
	char bank_size;
	char num_of_image_pages;
	char rsv0;
	char red_mask_size;
	char red_field_pos;
	char green_mask_size;
	char green_field_pos;
	char blue_mask_size;
	char blue_field_pos;
	char rsv_mask_size;
	char rsv_field_pos;
	char direct_color_mode_info;
	int videoBuffer;
	int rsv1;
	short rsv2;
	short lin_bytes_per_scanline;
	char bank_number_of_image_pages;
	char lin_number_of_image_pages;
	char lin_red_mask_size;
	char lin_red_field_pos;
	char lin_green_mask_size;
	char lin_green_field_pos;
	char lin_blue_mask_size;
	char lin_blue_field_pos;
	char lin_rsv_mask_size;
	char lin_rsv_field_pos;
	int max_pixel_clock;
	char rsv3[189];
} VesaInfo;

typedef struct {
	int flags;
	int mem_lower;
	int mem_upper;
	int boot_dev;
	int cmdline;
	int mods_count;
	int mods_addr;
	int syms0;
	int syms1;
	int syms2;
	int syms3;
	int mmap_len;
	int mmap_addr;
	int drives_len;
	int drives_addr;
	int config_table;
	int bootloader_name;
	int apm_table;
	int vbe_control_info;
	int vbe_mode_info;
	short vbe_mode;
	short vbe_interface_seg;
	short vbe_interface_offs;
	short vbe_interface_len;
} BootInfo;

#endif
