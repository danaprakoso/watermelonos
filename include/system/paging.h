#ifndef PAGING_H
#define PAGING_H

extern "C" void enable_paging();
extern "C" void set_pagedir(uint32_t pagedir_addr);
void init_paging();

#endif
