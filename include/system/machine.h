#ifndef MACHINE_H
#define MACHINE_H

#define ACTIVITY_MAIN 1
#define ACTIVITY_SAVETO 2

void stop();
void mainLoop();
template<typename Base, typename T>
inline bool instanceof(const T *ptr) {
    return dynamic_cast<const Base*>(ptr) != nullptr;
}
void setActivity(int activity);
int getActivity();

#endif
