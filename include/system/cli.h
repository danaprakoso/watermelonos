#ifndef CLI_H
#define CLI_H

extern char hex_value[16];
void print_number(int number);
void print_number_unsigned(unsigned int number);
int print_hex(unsigned int number);
void print(char* text);
int txt_get_offset(int x, int y);
void dump(char* data, int size);
void dn_printf(char* text, ...);
void printf(char* text, ...);

#endif
