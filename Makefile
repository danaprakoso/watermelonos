ASM_SOURCES=$(wildcard *.asm) $(wildcard system/*.asm) $(wildcard system/init/*.asm)
ASM_OBJECTS=$(ASM_SOURCES:.asm=.o)
ASM_COMPILER=/usr/bin/nasm
S_SOURCES=$(wildcard *.s)
S_OBJECTS=$(S_SOURCES:.s=.o)
S_COMPILER=/home/dana/compiler/bin/i686-elf-as
CPP_SOURCES=$(wildcard *.cpp) $(wildcard system/*.cpp) $(wildcard system/drivers/*.cpp) $(wildcard system/drivers/audio/*.cpp) $(wildcard system/drivers/graphics/intel/*.cpp) $(wildcard system/fs/*.cpp) $(wildcard system/init/*.cpp) $(wildcard system/parser/*.cpp) $(wildcard system/ui/*.cpp) $(wildcard system/apps/*.cpp)
CPP_OBJECTS=$(CPP_SOURCES:.cpp=.o)
C_COMPILER=/home/dana/compiler/bin/i686-myos-gcc
CPP_COMPILER=/home/dana/compiler/bin/i686-myos-g++
AR=/home/dana/compiler/bin/i686-myos-ar
INCLUDEDIR=-I./include -I/home/dana/compiler/usr/include/freetype2
CFLAGS=-w -fpermissive -masm=intel $(INCLUDEDIR) -fno-exceptions -fno-rtti

all: $(CPP_OBJECTS) $(ASM_OBJECTS) $(S_OBJECTS)
	#i686-elf-g++ $(CFLAGS) -c boot/boot3.cpp -o boot/boot3.o
	#i686-elf-g++ $(CFLAGS) -T boot/linker.ld -o boot/boot3 -ffreestanding -nostdlib boot/boot3.o -lgcc
	#nasm -f bin -o boot/boot.bin boot/boot.asm
	#bin/nasm -f bin -o boot/boot2.bin boot/boot2.asm
	#dd status=noxfer conv=notrunc if=boot/boot.bin of=boot.flp
	#sudo mount -o loop -t vfat boot.flp mounted
	#sudo cp boot/boot2.bin mounted
	#sudo umount mounted
	#cp boot.flp iso
	i686-elf-g++ -T linker.ld -o kernel.bin -ffreestanding -nostdlib $^ -lgcc -fno-exceptions -fno-rtti
	cp kernel.bin iso/boot
	grub-mkrescue -o DanaOS.iso iso
	#/usr/bin/mkisofs -quiet -V 'DANAOS' -input-charset iso8859-1 -o Watermelon.iso -b boot.flp iso/
	VBoxManage startvm Watermelon

%.o: %.cpp
	$(CPP_COMPILER) $(CFLAGS) -c $^ -o $@

%.o: %.asm
	$(ASM_COMPILER) -f elf32 -o $@ $^
	
%.o: %.s
	$(S_COMPILER) $^ -o $@

clean:
	/bin/rm -rf */*.o

png_reader:
	cd lib/pngreader; make

textdrawer:
	cd lib/textdrawer; make
